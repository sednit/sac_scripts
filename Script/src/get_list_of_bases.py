import os
import re
import ftplib
from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError

class GetListOfBasesScenario(BaseScenario):

  def _validate_specific_data(self):
    pass

  def _real(self):
    #path = "C:\\Users\\" + self.config["os_login"]
    #path_to_v8i = path + "\\AppData\\Roaming\\1C\\1CEStart\\ibases.v8i" 
    raise SACError(
      "ARGS_ERROR",
      "path_to_v8i",
      "hf"
    )
    debug(path_to_v8i)
    print(path_to_v8i)

    ftp = ftplib.FTP(self.config["host"])
    ftp.login(self.config["ftp_login"], self.config["ftp_password"])
    ftp.cwd("/")
    ftp.mkd('tmp')
    ftp.cwd("/tmp/")
    with open(path_to_v8i, "rb") as fobj:
      ftp.storbinary('STOR ' + self.config["file_name"], fobj)

if __name__ == "__main__":
  GetListOfBasesScenario.main()