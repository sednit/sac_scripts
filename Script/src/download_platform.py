# coding: utf-8

import re
import shutil
import os
import cgi
import uuid
import platform
from robobrowser import RoboBrowser


from lib.common import bootstrap
from lib.common.base_scenario import *
from lib.common.errors import *
from lib.common.logger import *
from lib.utils import *
from lib.utils.cmd import run_cmd
from lib.utils.fs import *
from lib.common import global_vars as gv
from lib.common.config import *
from lib.base_download_release import *


class DownloadPlatformScenario(BaseDownloadReleaseScenario):

    def _validate_additional_data(self):
        validate_data = [
            ["target-arch", int, int, [64, 32]],
            ["target-os", str, str, ["Windows", "Linux-deb", "Linux-rpm"]],
            ["version", PlatformVersion]
        ]
        self.config.validate(validate_data)
        if self.config["target-os"] == "Windows":
            allowed_distr_types = ["full", "thin-client"]
        else:
            allowed_distr_types = ["server", "client", "thin-client"]
        validate_data = [
            ["distr-type", str, str, allowed_distr_types]
        ]
        self.config.validate(validate_data)

    def _get_url(self):
        # reset browser opened page
        self.browser.open(self.portal_url+"/total")
        # get to platform versions page
        self.real_version = go_to_product_version_page(
            self.browser,
            "Технологическая платформа {}.{}".format(
                self.config["version"].version[0],
                self.config["version"].version[1]
            ),
            self.config["version"] if len(self.config["version"].version) == 4 \
            else "current"
        )
        # get to download page
        os_type_str = {
            "Windows": "Windows", "Linux-deb": "DEB", "Linux-rpm": "RPM"
        }[self.config["target-os"]]
        # select regex filter. If OS is Windows and version lower than specified
        # allow download only full version
        if self.config["target-os"] == "Windows" and \
           self.real_version < "8.3.11.2467":
            global_logger.info(message="OS type and version of platform points "
                               "on version, which is vulnerable to bug 10182776"
                               ", so as workaround would be downloaded full "
                               "distribution of platform",
                               version=self.real_version)
            regex_filter = "Технологическая платформа.*{}.*".format(os_type_str)
        else:
            regex_filter = {
                "full": "Технологическая платформа.*{}.*".format(os_type_str),
                "thin-client": "Тонкий клиент.*{}.*".format(os_type_str),
                "client": "Клиент.*{}.*".format(os_type_str),
                "server": "Сервер.*{}.*".format(os_type_str)
            }[self.config["distr-type"]]
        try:
            find_link_and_go(
                self.browser, "a", regex_filter,
                lambda x: "64" in x if self.config["target-arch"] == 64 \
                else "64" not in x
            )
        except SACError as err:
            if err.str_code == "BROWSER_ERROR":
                raise SACError(
                    "LOGIC_ERROR",
                    "Can't find product distribution with specified parameters",
                    os=self.config["target-os"],
                    distr_type=self.config["distr-type"],
                    arch=self.config["target-arch"]
                )
            else:
                raise
        # get download link
        return self.browser.find(class_="downloadDist").find("a")["href"]

    def _get_available_tests(self):
        return [
            ["try-login", self._login_to_portal, False],
            ["check-url", self._get_url, False],
        ]

    def _process_files(self, src):
        if self.config["save-accurately"]:
            save_accurately(src, self.config["download-folder"], self.tmp)
        else:
            save_platform(src, self.config["download-folder"], self.tmp,
                          self.real_version, self.config["target-os"],
                          self.config["target-arch"], self.config["distr-type"])


if __name__ == "__main__":
    DownloadPlatformScenario.main()
