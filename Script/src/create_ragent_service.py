import os


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.utils.cmd import run_cmd
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario


class CreateRagentServiceScenario(BaseScenario):

    def _after_init(self):
        # import proper service module
        if self.config["os-type"] == "Windows":
            from lib.win_utils import service
        else:
            from lib.linux_utils import service
        self.service_module = service

    def _validate_specific_data(self):
        # first piece of data
        validate_data = [
            # specific parameters
            ["setup-folder", StrPathExpanded],
            ["name", str],
            ["description", str],
            ["port", int],
            ["regport", int],
            ["range", str, str, dyn_range_checker],
            ["username", str],
            ["cluster-folder", StrPathExpanded],
            ["cluster-debug", bool],
            ["start-service-automatically", bool]
        ]
        self.config.validate(validate_data)
        # if OS is Windows, then check that password supplied
        if self.config["os-type"] == "Windows":
            validate_data = [
                ["password", str],
            ]
            self.config.validate(validate_data)
        # otherwise, we don't care
        else:
            self.config["password"] = ""

    def _check_setup_folder(self):
        # only check that ragent is there
        if self.config["os-type"] == "Windows":
            result = self.service_module.find_file_in_1c_installation(
                    "ragent.exe", self.config["setup-folder"]
            )
        else:
            result = self.service_module.find_file_in_1c_installation(
                    "ragent"
            )
        if result is None:
                raise SACError(
                    "ARGS_ERROR", "ragent not found in setup-folder"
                )

    def _check_data_not_used(self):
        # check that service with specified parameters not already exists
        if not self.service_module.can_create_1c_cluster_service(
                self.config["name"], self.config["port"],
                self.config["regport"], dyn_range_parser(self.config["range"]),
                self.config["cluster-folder"]
        ):
            raise SACError(
                "ARGS_ERROR", "ragent service with specified parameters "
                "already exists", name=self.config["name"],
                port=self.config["port"], regport=self.config["regport"],
                range=self.config["range"],
                cluster_folder=self.config["cluster-folder"]
            )

    def _test_sc_permissions(self):
        # check permissions for service creation
        if self.config["os-type"] != "Windows":
            return self.service_module.Service.test_sc_permissions()
        else:
            username = self.config["username"] if \
                       self.config["username"] != "" else None
            password = self.config["password"] if \
                       self.config["password"] != "" else None
            return self.service_module.Service.test_sc_permissions(username,
                                                                   password)

    def _get_available_tests(self):
        return [
            ("test-sc-permissions", self._test_sc_permissions, False),
            ("check-setup-folder", self._check_setup_folder, True),
            ("check-data-not-used", self._check_data_not_used, True),
        ]

    def _real(self):
        # install service
        self.service_module.install_service_1c(
            self.config["name"], self.config["setup-folder"],
            self.config["username"], self.config["password"],
            self.config["cluster-folder"], self.config["port"],
            self.config["regport"], self.config["range"],
            self.config["cluster-debug"], self.config["description"],
            self.config["start-service-automatically"]
        )
        # if cluster folder doesn't exist, create it and set ownership
        if not os.path.exists(self.config["cluster-folder"]):
            os.makedirs(self.config["cluster-folder"], exist_ok=True)
        if self.config["os-type"] == "Windows":
            run_cmd("cacls {} /E /G {}:f".format(
                self.config["cluster-folder"], self.config["username"]
            ), shell=True)
        else:
            run_cmd(["chown", "-R", str(self.config["username"]),
                     self.config["cluster-folder"]])


## Parse range of ports for ragent.
# @param str_range String with range.
# @return List of ports.
def dyn_range_parser(str_range):
    ports = []
    try:
        subranges = str_range.split(",")
        for subrange in subranges:
            parts = subrange.split(":")
            if len(parts) < 2:
                ports.append(int(parts[0]))
            else:
                ports += list(range(int(parts[0]), int(parts[1]) + 1))
    except:
        raise ValueError("Incorrect dynamic range")
    return ports


## Check that string contain valid range of ports for ragent.
# @param str_range String with range.
# @return True, if correct string, False otherwise.
def dyn_range_checker(str_range):
    try:
        dyn_range_parser(str_range)
        return True
    except:
        return False


if __name__ == "__main__":
    CreateRagentServiceScenario.main()
