from lib.common import bootstrap
from lib.common.errors import SACError
from lib.base_rac_scenario import BaseRacScenario


class KillSessionsScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["cluster-port", int],
            ["kill-designer", bool],
        ]
        self.config.validate(validate_data)
        if "infobase-name" not in self.config:
            self.config["infobase-name"] = None
        # if infobase name is in configuration, check it
        elif self.config["infobase-name"] not in [None, ""]:
            validate_data = [
                ["infobase-name", str],
            ]
        self.config.validate(validate_data)

    def _get_additional_tests(self):
        return [
             ["check-cluster-existence", self._get_cluster_id_by_port, True],
        ]

    def _real(self):
        # get cluster uuid
        options = {"cluster": self._get_cluster_id_by_port()}
        self._add_user_credentials(options)
        # if infobase specified, find it and kill sessions in specific
        # infobase
        if self.config["infobase-name"] not in [None, ""]:
            # select infobase uuid
            res = self._execute_rac("infobase", "summary list", options)
            parsed_output = self._parse_rac_output(
                bootstrap.try_decode(res.stdout)
            )
            infobase = self._get_object_by_field(parsed_output, "name",
                                                 self.config["infobase-name"])
            if infobase is None:
                raise SACError(
                    "LOGIC_ERROR",
                    "infobase with specified name doesn't exists",
                    infobase_name=self.config["infobase-name"]
                )
            else:
                infobase_uuid = infobase["infobase"]
                options["infobase"] = infobase_uuid
        else:
            infobase_uuid = None
        res = self._execute_rac("session", "list", options)
        parsed_output = self._parse_rac_output(bootstrap.try_decode(res.stdout))
        if "infobase" in options:
            del options["infobase"]
        for session in parsed_output:
            if not self.config["kill-designer"] and \
               session["app-id"].lower() == "designer":
                continue
            options["session"] = session["session"]
            # swallow all exceptions because we target situation when no session
            # alive
            try:
                self._execute_rac("session", "terminate", options)
            except:
                pass
            # delete session option from dict
            del options["session"]
        # get list of sessions again and check that it is empty
        if infobase_uuid is not None:
            options["infobase"] = infobase_uuid
        res = self._execute_rac("session", "list", options)
        parsed_output = self._parse_rac_output(bootstrap.try_decode(res.stdout))
        # check that only designer sessions left, if kill-designer set to False
        for session in parsed_output:
            if self.config["kill-designer"]:
                raise SACError("LOGIC_ERROR",
                               "Sessions still alive")
            else:
                if session["app-id"].lower() != "designer":
                    raise SACError("LOGIC_ERROR",
                                   "Sessions still alive")


if __name__ == "__main__":
    KillSessionsScenario.main()
