const path = require("path");
const node_modules_path = path.resolve('../../node/node_modules');
const libPath = path.resolve('jslib');

const fs = require('fs');

const args = require(libPath + '/argParser')(process.argv);
const fileGet = require(libPath + '/fileGet');

if (args['test-mode'] === 'True') return; 

const main = async () => {
  const version = args.version;
  const tmpDir = path.resolve();
  const ftpPath = path.resolve();
  await fileGet({
    host: '192.168.0.240',
    port: 978,
    dstPath: 'C:/projects/python/sac scripts/test',
    srcPath: '/platform/8.3/8.3.12.1616/windows64full/0x040e.ini',
    name: undefined,
  });

}

main();
