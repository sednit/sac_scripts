from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario


class CreateRasServiceScenario(BaseScenario):

    def _after_init(self):
        # import proper service module
        if self.config["os-type"] == "Windows":
            from lib.win_utils import service
        else:
            from lib.linux_utils import service
        self.service_module = service

    def _validate_specific_data(self):
        # first piece of data
        validate_data = [
            # specific parameters
            ["name", str],
            ["description", str],
            ["port", int],
            ["username", str],
            ["agent-host", str],
            ["agent-port", int],
            ["setup-folder", StrPathExpanded],
            ["start-service-automatically", bool]
        ]
        self.config.validate(validate_data)
        # if OS is Windows, then check that password supplied
        if self.config["os-type"] == "Windows":
            validate_data = [["password", str], ]
        # otherwise, we don't care
        else:
            self.config["password"] = ""
        self.config.validate(validate_data)

    def _check_setup_folder(self):
        # only check that ras is there
        if self.config["os-type"] == "Windows":
            result = self.service_module.find_file_in_1c_installation(
                    "ras.exe", self.config["setup-folder"]
            )
        else:
            result = self.service_module.find_file_in_1c_installation(
                    "ras"
            )
        if result is None:
                raise SACError(
                    "ARGS_ERROR", "ras not found in setup-folder"
                )

    def _check_data_not_used(self):
        # check that service with specified parameters not already exists
        if not self.service_module.can_create_ras_service(
                self.config["name"], self.config["port"]
        ):
            raise SACError(
                "ARGS_ERROR", "ras service with specified parameters "
                "already exists", name=self.config["name"],
                port=self.config["port"], agent_host=self.config["agent-host"],
                agent_port=self.config["agent-port"],
            )

    def _test_sc_permissions(self):
        # check permissions for service creation
        if self.config["os-type"] != "Windows":
            return self.service_module.Service.test_sc_permissions()
        else:
            username = self.config["username"] if \
                       self.config["username"] != "" else None
            password = self.config["password"] if \
                       self.config["password"] != "" else None
            return self.service_module.Service.test_sc_permissions(username,
                                                                   password)

    def _get_available_tests(self):
        return [
            ("test-sc-permissions", self._test_sc_permissions, False),
            ("check-setup-folder", self._check_setup_folder, True),
            ("check-data-not-used", self._check_data_not_used, True),
        ]

    def _real(self):
        # install service
        self.service_module.install_ras(
            self.config["name"], self.config["setup-folder"],
            self.config["username"], self.config["password"],
            self.config["port"], self.config["agent-host"],
            self.config["agent-port"], self.config["description"],
            self.config["start-service-automatically"]
        )


if __name__ == "__main__":
    CreateRasServiceScenario.main()
