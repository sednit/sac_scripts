import os


from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from lib.common.config import StrPathExpanded
from lib.utils.cmd import run_cmd


class ChangeIbPubScenario(BaseScenario):

    def _validate_specific_data(self):
        # first check most common settings
        validate_data = [
            ["action", str, str, ["publish", "delete"]],
            ["virtual-directory", str],
            ["setup-folder", StrPathExpanded],
        ]
        self.config.validate(validate_data)
        # if action is "publish", check for additional parameters
        if self.config["action"] == "publish":
            validate_data = [
                ["ib", str],
                ["srvr", str],
                ["port", int],
                ["real-directory", StrPathExpanded]
            ]
            self.config.validate(validate_data)
        # check web server
        available_web_servers = ["apache20", "apache22", "apache24"]
        if self.config["os-type"] == "Windows":
            available_web_servers.append("iis")
        validate_data = [
            ["web-server", str, str,
             lambda x: x.lower() in available_web_servers],
        ]
        self.config.validate(validate_data)
        self.config["web-server"] = self.config["web-server"].lower()
        # check config path
        if self.config["web-server"].lower() != "iis" and \
           "config-path" in self.config:
            validate_data = [
                ["config-path", StrPathExpanded]
            ]
            self.config.validate(validate_data)
        else:
            self.config["config-path"] = None

    def _after_init(self):
        if self.config["os-type"] == "Windows":
            from lib import win_utils as m
        else:
            from lib import linux_utils as m
        self.__os_utils_module = m

    ## Find webinst executable in 1C:Enterprise installation location.
    # @param self Pointer to object.
    # @return Path to webinst executable.
    def __find_webinst_executable(self):
        if self.config["os-type"] == "Windows":
            webinst_executable = self.__os_utils_module \
                                     .find_file_in_1c_installation(
                                         "webinst.exe",
                                         self.config["setup-folder"]
                                     )
        else:
            webinst_executable = self.__os_utils_module \
                                     .find_file_in_1c_installation(
                                         "webinst",
                                         self.config["setup-folder"]
                                     )
        if webinst_executable is None or not os.path.exists(webinst_executable):
            raise SACError("LOGIC_ERROR",
                                         "cannot find webinst executable")
        else:
            return webinst_executable

    def _get_available_tests(self):
        return [
            ["check-webinst-executable", self.__find_webinst_executable, True],
        ]

    def _real(self):
        webinst_executable = self.__find_webinst_executable()
        # build command line
        action = "-" + self.config["action"]
        server = "-" + self.config["web-server"]
        conn_str = "Srvr=\"{}:{}\";Ref=\"{}\"".format(self.config["srvr"],
                                                      self.config["port"],
                                                      self.config["ib"])
        args = [webinst_executable, action, server, "-wsdir",
                self.config["virtual-directory"], "-dir",
                self.config["real-directory"], "-connstr", conn_str]
        if self.config["config-path"] not in [None, StrPathExpanded("")]:
            args += ["-confPath", self.config["config-path"]]
        # execute
        res = run_cmd(args, shell=False)
        if res.returncode != 0:
            raise SACError(
                "CMD_RESULT_ERROR",
                returncode=res.returncode,
                stdout=bootstrap.try_decode(res.stdout),
                stderr=bootstrap.try_decode(res.stderr),
                args=args
            )


if __name__ == "__main__":
    ChangeIbPubScenario.main()
