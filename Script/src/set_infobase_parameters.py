import threading


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.logger import global_logger
from lib.base_rac_scenario import BaseRacScenario


class SetInfobaseParametersScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["cluster-port", int],
            ["infobase-parameters", dict],
        ]
        self.config.validate(validate_data)
        # if infobase is not None nor empty string, check it
        if self.config["infobase-name"] not in [None, ""]:
            validate_data = [
                ["infobase-name", str],
            ]
        self.config.validate(validate_data)

    def _get_additional_tests(self):
        return [
             ["check-cluster-existence", self._get_cluster_id_by_port, True],
        ]

    def __set_infobase_parameters(self, infobase_uuid, base_options,
                                  result_list, result_index):
        options = base_options.copy()
        options["infobase"] = infobase_uuid
        for key, value in self.config["infobase-parameters"].items():
            if key not in options:
                options[key] = value
        try:
            result_list[result_index] = self._execute_rac("infobase", "update",
                                                          options)
        except Exception as err:
            global_logger.error(**err.to_dict())
            result_list[result_index] = None

    def _real(self):
        # find cluster uuid
        options = {"cluster": self._get_cluster_id_by_port()}
        # set common parameters
        self._add_user_credentials(options)
        # select infobase
        res = self._execute_rac("infobase", "summary list", options)
        # set login and password for infobases
        self._add_user_credentials(options, "infobase")
        parsed_output = self._parse_rac_output(bootstrap.try_decode(res.stdout))
        infobases = {i["name"]: i["infobase"] for i in parsed_output}
        required_infobases_uuids = []
        if self.config["infobase-name"] not in [None, ""]:
            try:
                required_infobases_uuids.append(
                    infobases[self.config["infobase-name"]]
                )
            except KeyError:
                raise SACError(
                    "LOGIC_ERROR",
                    "infobase with specified name doesn't exists",
                    infobase_name=self.config["infobase-name"]
                )
        else:
            required_infobases_uuids = [v for _, v in infobases.items()]
        # start working with each infobase in separated thread
        threads = []
        results = [None for _ in range(0, len(required_infobases_uuids))]
        for i in range(0, len(required_infobases_uuids)):
            infobase_uuid = required_infobases_uuids[i]
            threads.append(threading.Thread(
                target=self.__set_infobase_parameters,
                args=(infobase_uuid, options, results, i)
            ))
            threads[-1].start()
        for thread in threads:
            thread.join()
        for i in results:
            if i is None:
                raise SACError("LOGIC_ERROR",
                               "Error occurred while working on infobases")


if __name__ == "__main__":
    SetInfobaseParametersScenario.main()
