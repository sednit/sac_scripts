import sys
import shutil
import tempfile


from lib.common import bootstrap
from lib.common.config import *
from lib.utils import *
from lib.common.base_scenario import *


class PlatformInstallScenario(BaseScenario):

    def _validate_specific_data(self):

        def check_platform_modules(data):
            try:
                detect_arch = lambda x: 64 if "64" in x.value else 32
                initial_arch = None
                for el in data:
                    if initial_arch is None:
                        initial_arch = detect_arch(el)
                        continue
                    if detect_arch(el) != initial_arch:
                        global_logger.error(
                            message="All modules should have same bitness, "
                            "ie 'server,thin' or 'server64,thin64'",
                        )
                        return False
                return True
            except Exception as e:
                return False

        validate_data = [
            ["version", PlatformVersion],
            ["distr-folder",  StrPathExpanded],
            # this folder store copied distr.
            ["platform-modules", set, PlatformModules.from_string,
             check_platform_modules],
        ]
        self.config.validate(validate_data)
        if len(self.config["platform-modules"]) < 1:
            raise SACError("ARGS_ERROR", "No module specified")
        # is OS is Windows, append presented setup-folder, otherwise, set it to
        # empty string (which will be converted to ".")
        if self.config["os-type"] == "Windows":
            validate_data = [["setup-folder", StrPathExpanded], ]
        else:
            self.config["setup-folder"] = ""
            validate_data = [["languages", str], ]
        self.config.validate(validate_data)

    def _after_init(self):
        # set fields for future usage
        self.platform_installer = None
        # getting OS-specific Platform1C class
        if self.config["os-type"] == "Windows":
            from lib.win_utils.platform_installer import Platform1CInstaller
        else:
            from lib.linux_utils.platform_installer import Platform1CInstaller
        # store Platform1CInstallerCls class. Object of this class will be
        # will be created later
        self.Platform1CInstallerCls = Platform1CInstaller

    ## Test installers.
    # @param self Pointer to object.
    def _test_installer(self):
        self.platform_installer.test_installer()

    ## Check web servers.
    # @param self Pointer to object.
    def _test_web(self):
        if PlatformModules.WEB in self.config["platform-modules"] or \
           PlatformModules.WEB64 in self.config["platform-modules"]:
            self.Platform1CInstallerCls.test_web()

    ## Try to setup Platform1CInstaller object.
    # @param self Pointer to object.
    def _setup_platform_1c_installer(self):
        self.platform_installer = self.Platform1CInstallerCls(self.config)

    def _get_available_tests(self):
        return [
            ("test-install-permissions",
             self.Platform1CInstallerCls.test_install_permissions, False),
            ("check-distr", self._setup_platform_1c_installer, True),
            ("test-update", self._test_installer, True),
        ]

    def _real(self):
        # if Platform1CInstaller object not set during tests, set it now
        if self.platform_installer is None:
                self._setup_platform_1c_installer()
        # perform installation
        self.platform_installer.install()


if __name__ == "__main__":
    PlatformInstallScenario.main()
