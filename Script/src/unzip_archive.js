const path = require("path");
const node_modules_path = path.resolve('../../node/node_modules');
const libPath = path.resolve('jslib');

const fs = require('fs');

const yauzl = require(node_modules_path + '/yauzl');
const mkdirp = require(node_modules_path + '/mkdirp');

const args = require(libPath + '/argParser')(process.argv);

if (args['test-mode'] === 'True') return; 

const normalizeArgs = () => {
  if (!args.dst_path) {
    args['dst_path'] = path.dirname(args.src_path);
  }	
  if (!args.name) {
    args.name = path.basename(args.src_path, '.zip');
  }
}

const directoryExists = async (directoryPath) => {
  return new Promise( (resolve) => {
   	fs.stat(directoryPath, function (err, stats) {
      if (err || stats === undefined) {
        return resolve(false);
      }
      if (stats.isDirectory()) {
        return resolve(true);
      }
      return resolve(false);
    });
  })
}

const mkdirpAsync = async (directoryPath) => {
  return new Promise((resolve) => {
    mkdirp(directoryPath, function(err) {
      if (err) throw err;
    })
    return resolve(true);
  })
}

const main = async () => {
  normalizeArgs();

  if (! await directoryExists(args['dst_path'])) {
    await mkdirpAsync(args['dst_path']);
  }
  yauzl.open(args.src_path, {lazyEntries: true}, (err, zipfile) => {
    if (err) throw err;
    zipfile.on("entry", function(entry) {
      if (/\/$/.test(entry.fileName)) {
      	const dirPath = path.resolve(args.dst_path, entry.fileName) + '\\';
      	mkdirp(dirPath, function(err) {
          if (err) throw err;
          zipfile.readEntry();
        });
      } else {
        zipfile.openReadStream(entry, function(err, readStream) {
          if (err) throw err;
          const filePath = path.resolve(args.dst_path, entry.fileName);
          const dirPath = path.dirname(filePath);
          mkdirp(dirPath, function(err) {
            readStream.on("end", function() {
              zipfile.readEntry();
            });
            if (err) throw err;
            readStream.pipe(fs.createWriteStream(filePath));
          });
        });
      }
    });
    zipfile.readEntry();
  })
}

main();
