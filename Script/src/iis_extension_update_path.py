# coding: utf-8
from lib.common.config import StrPathExpanded
from lib.common.errors import SACError
from lib.common.base_scenario import BaseScenario


class IISExtensionUpdatePathScenario(BaseScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["os-type", str, str, ["Windows"]],
            ["pub-name", str],
            ["setup-folder", StrPathExpanded],
        ]
        self.config.validate(validate_data)

    def _after_init(self):
        from lib.win_utils import iis
        from lib import win_utils
        self.iis_module = iis
        self.win_utils_module = win_utils

    def __get_wsisapi_path(self):
        wsisapi_path = self.win_utils_module.find_file_in_1c_installation(
            "wsisapi.dll", self.config["setup-folder"]
        )
        if wsisapi_path is None:
            raise SACError("LOGIC_ERROR", "wsisapi.dll not found")
        return wsisapi_path

    def _get_available_tests(self):
        return [
            ["check-wsisapi-dll", self.__get_wsisapi_path, True],
            ["test-web",
             lambda: self.iis_module.replace_handler_in_specific_entry(
                 "", self.config["pub-name"], True
             ), True]
        ]

    def _real(self):
        self.iis_module.replace_handler_in_specific_entry(
            self.__get_wsisapi_path(), self.config["pub-name"], False
        )


if __name__ == "__main__":
    IISExtensionUpdatePathScenario.main()
