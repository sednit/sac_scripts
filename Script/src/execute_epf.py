#!/usr/bin/python3
# coding: utf-8
import os


from lib.common import bootstrap
from lib.common import global_vars as gv
from lib.common.errors import SACError
from lib.common.logger import global_logger, escape_log_string
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario
from lib.common.const import LANGS
from lib.utils import detect_actual_os_type, try_open_file, execute_1c_client, \
    open_file


class ExecuteEpfScenario(BaseScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["test-mode", bool],
            ["os-type", str, str, ["Windows", "Linux-deb", "Linux-rpm"]],
            ["lang", str, str, LANGS],
            ["epf-path", StrPathExpanded],
            ["host", str, str, lambda x: len(x) > 0],
            ["port", int],
            ["ib", str],
            ["command", str],
            ["username", str],
            ["password", str],
            ["result-file", str],
            ["setup-folder", StrPathExpanded],
            ["check-ib-availability-time-limit", int, int,
             lambda x: x > 0]
        ]
        # if result-file is not empty, make string from it
        if self.config["result-file"] != "":
            validate_data = [
                ["result-file", StrPathExpanded],
            ]
        self.config.validate(validate_data)

    def _after_init(self):
        if detect_actual_os_type() == "Windows":
            from lib.win_utils import find_file_in_1c_installation
            _1cv8 = "1cv8.exe"
            _1cv8c = "1cv8c.exe"
        else:
            from lib.linux_utils import find_file_in_1c_installation
            _1cv8 = "1cv8"
            _1cv8c = "1cv8c"
        path = find_file_in_1c_installation(_1cv8c,
                                            self.config["setup-folder"])
        if path is None:
            path = find_file_in_1c_installation(_1cv8,
                                                self.config["setup-folder"])
            if path is None:
                raise SACError(
                    "ARGS_ERROR",
                    "Cannot find 1C:Enterprise client executable",
                )
        self.config["client-executable"] = path
        # if OS is Windows, also try to find file in bin subfolder.
        if self.config["os-type"] == "Windows":
            try:
                try_open_file(self.config["client-executable"])
            except:
                try:
                    new_client_executable = os.path.join(
                        os.path.split(self.config["client-executable"])[0],
                        "bin",
                        os.path.split(self.config["client-executable"])[1]
                    )
                    try_open_file(new_client_executable)
                    self.config["client-executable"] = new_client_executable
                except:
                    pass
        # setup result file
        self.result_file_path = None if "result-file" not in self.config \
                                or self.config["result-file"] == "" else \
                                StrPathExpanded(self.config["result-file"])

    ## Try to open necessary files.
    # @param self Pointer to object.
    def _check_files(self):
        try_open_file(self.config["epf-path"])
        try_open_file(self.config["client-executable"])

    ## Make sure that current user is allowed to enter IB and execute EPFs.
    # @param self Pointer to object.
    def _check_ib_availability(self):
        success_file = os.path.join(self.tmp, "ib_enter_success")
        if os.path.exists(success_file):
            os.remove(success_file)
        # first: check that we can enter base and execute EPFs.
        conn_str = "Srvr={}:{};Ref={};".format(self.config["host"],
                                               self.config["port"],
                                               self.config["ib"])
        cmd = [
            self.config["client-executable"], "/IBConnectionString",
            conn_str, "/Execute",
            os.path.join(os.path.abspath(os.path.dirname(__file__)),
                         "CheckIBAvailability.epf"),
            "/C", success_file, "/L" + self.config["lang"],
            "/DisableStartupDialogs", "/DisableStartupMessages"
        ]
        # if user and password set in config, add them to string
        if "username" in self.config and "password" in self.config and \
           self.config["username"] != "":
            cmd += ["/N", str(self.config["username"]), "/P",
                    str(self.config["password"])]
        # add access code if necessary
        if "access-code" in self.config and self.config["access-code"] != "":
            cmd += ["/UC", str(self.config["access-code"])]
        try:
            execute_1c_client(cmd,
                              self.config["check-ib-availability-time-limit"],
                              lambda: os.path.exists(success_file))
        except SACError as err:
            if err.num_code == SACError("TIMEOUT_ERROR").num_code:
                raise SACError(
                    "ARGS_ERROR",
                    "Cannot enter IB and execute EPF with specified parameters"
                )
            else:
                raise
        if not os.path.exists(success_file):
            raise SACError(
                "ARGS_ERROR",
                "Cannot enter IB and execute EPF with specified parameters"
            )
        else:
            os.remove(success_file)

    def _get_available_tests(self):
        return [
            ("check-files", self._check_files, True),
            ("check-ib-availability", self._check_ib_availability, False)
        ]

    ## Check that result file is exist (if result-file is specified).
    # @param self Pointer to object.
    def _check_result_file(self):
        if self.result_file_path is None:
            return
        f = open_file(self.result_file_path)
        try:
            data = f.read()
        except UnicodeError:
            raise SACError("LOGIC_ERROR",
                                         "Cannot decode result file")
        f.close()
        # workaround for "TextWriter new file 'Zero Width No-Break Space' bug"
        if ord(data[0]) == 65279:
            data = data[1:]
        # end
        if gv.ESCAPE_STRINGS:
            global_logger.info(epf=data)
        else:
            global_logger.info(epf=escape_log_string(data))

    def _real(self):
        if self.result_file_path is not None:
            if os.path.exists(self.result_file_path):
                os.remove(self.result_file_path)
        # building command line string
        conn_str = "Srvr={}:{};Ref={};".format(self.config["host"],
                                               self.config["port"],
                                               self.config["ib"])
        cmd = [
            self.config["client-executable"], "/IBConnectionString",
            conn_str, "/Execute", self.config["epf-path"],
            "/C", self.config["command"], "/L" + self.config["lang"],
            "/DisableStartupDialogs", "/DisableStartupMessages"
        ]
        # if user and password set in config, add them to string
        if "username" in self.config and "password" in self.config and \
           self.config["username"] != "":
            cmd += ["/N", str(self.config["username"]), "/P",
                    str(self.config["password"])]
        # add access code if necessary
        if "access-code" in self.config and self.config["access-code"] != "":
            cmd += ["/UC", str(self.config["access-code"])]
        # execute epf
        res = execute_1c_client(cmd)
        if res.returncode != 0:
                raise SACError("CMD_RESULT_ERROR",
                               returncode=res.returncode,
                               stdout=bootstrap.try_decode(res.stdout),
                               stderr=bootstrap.try_decode(res.stderr),
                               args=res.args)
        # check result file
        self._check_result_file()
        # show warning about 1cv8c behavior.
        global_logger.warning(
            message="Return code 0 mean only that 1cv8c returned 0. It " \
            "doesn't mean that EPF itself finished successful."
        )
        return 0

    @staticmethod
    def _handle_top_level_exception(err):
        res = 1
        if isinstance(err, SACError):
            global_logger.error(**err.to_dict())
            res = err.num_code
        else:
            err = SACError("UNKNOWN", err)
            global_logger.error(**err.to_dict())
            res = err.num_code
        # if result is 4, show additional information, which can help to
        # investigate occurred error
        if res == 4:
            global_logger.warning(
                message="EPF execution failed. Exact reason unknown." \
                "TIMEOUT_ERROR could mean that execution exceeded time as " \
                "well as that execution wasn't started due to login error, " \
                "wrong rights on IB or anything else. To see in specific " \
                "what happened, run script again and watch for the process."
            )
        return res


if __name__ == "__main__":
    ExecuteEpfScenario.main()
