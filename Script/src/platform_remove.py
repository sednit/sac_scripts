import sys
import shutil
import tempfile


from lib.common import bootstrap
from lib.common.config import *
from lib.utils import *
from lib.common.base_scenario import *


def number_of_ragent_processes_running(folder=""):
    if detect_actual_os_type() == "Windows":
        from lib import win_utils as os_utils
    else:
        from lib import linux_utils as os_utils
    ragent_path = os.path.join(folder, "ragent")
    ragent_procs = list()
    if detect_actual_os_type() == "Windows":
        from lib.win_utils import get_processes_id_by_name
        ragent_procs = get_processes_id_by_name(
            "ragent.exe",
            repr(folder)[1:-1]
        ) if folder != "" else get_processes_id_by_name(
            "ragent.exe",
        )
    else:
        from lib.linux_utils import get_processes_id_by_name
        if folder == "":
            ragent_procs = get_processes_id_by_name(ragent_path)
        else:
            ragent_procs = get_processes_id_by_name(ragent_path, folder)
    return len(ragent_procs)


class PlatformRemoveScenario(BaseScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["version", PlatformVersion],
        ]
        # is OS is Windows, append presented setup-folder, otherwise, set it to
        # empty string (which will be converted to ".")
        if self.config["os-type"] == "Windows":
            validate_data.append(["setup-folder", StrPathExpanded])
        else:
            self.config["setup-folder"] = ""
        self.config.validate(validate_data)
        # logic check
        if self.config["os-type"] == "Windows":
            if self.config["version"] == "" \
               and self.config["setup-folder"] == "":
                raise SACError(
                    "ARGS_ERROR", "Both version and setup folder not specified,"
                    " which is prohibited"
                )

    def _after_init(self):
        # select proper utils module, service module and Platform1CRemover class
        if self.config["os-type"] == "Windows":
            from lib.win_utils.platform_remover import Platform1CRemover
        else:
            from lib.linux_utils.platform_remover import Platform1CRemover
        self.platform_remover = Platform1CRemover(self.config)

    ## Check that cluster processes not started. On Windows it counts only
    #  processes, which uses executable from setup-folder.
    # @param self Pointer to object.
    def _check_cluster_started(self):
        if self.config["os-type"] == "Windows":
            folder = self.config["setup-folder"]
        else:
            # temporary fix, which is find ragent's folder in /opt/1C
            from lib.linux_utils import find_file_in_1c_installation
            f_ = find_file_in_1c_installation("ragent")
            if not f_:  # if ragent not found, assume that nothing started from
                return  # /opt/1C
            folder = os.path.dirname(f_)
        num_of_procs = number_of_ragent_processes_running(folder)
        if num_of_procs > 0:
            raise SACError("INSTALL_ERROR",
                                         "cluster processes is running")

    def _test_old_version(self):
        # under Windows, we use function which actually return installations,
        # which fit requirements, but don't use return value, only make sure
        # that function complete successful
        if self.config["os-type"] == "Windows":
            self.platform_remover._find_installations()
        # under Linux systems use special testing function
        else:
            self.platform_remover.test_old_version()

    def _get_available_tests(self):
        return [
            ("check-old-version", self._test_old_version, False),
            ("check-cluster-started", self._check_cluster_started, True),
        ]

    def _real(self):
        self.platform_remover.uninstall_old()


if __name__ == "__main__":
    PlatformRemoveScenario.main()
