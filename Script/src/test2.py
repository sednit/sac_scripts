from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError

class Test2Scenario(BaseScenario):

    def _validate_specific_data(self):
        pass

    def _real(self):
        raise SACError(
            "ARGS_ERROR",
            "Какаята очень сериёзноя ашипка",
            "hf"
        )

if __name__ == "__main__":
  Test2Scenario.main()
