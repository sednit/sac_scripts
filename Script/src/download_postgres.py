# coding: utf-8

import re
import shutil
import os
import cgi
import uuid
import platform
from robobrowser import RoboBrowser


from lib.common import bootstrap
from lib.common.base_scenario import *
from lib.common.errors import *
from lib.common.logger import *
from lib.utils import *
from lib.utils.cmd import run_cmd
from lib.utils.fs import *
from lib.common import global_vars as gv
from lib.common.config import *
from lib.base_download_release import *


class DownloadPostgresScenario(BaseDownloadReleaseScenario):

    def _validate_additional_data(self):
        validate_data = [
            ["target-arch", int, int, [64, 32]],
            ["target-os", str, str, ["Windows", "Linux-deb", "Linux-rpm"]],
            ["version", str]
        ]
        self.config.validate(validate_data)

    def _get_url(self):

        def get_selection_function(os_type, arch):

            def f(s):
                os_test = os_type in s
                arch_test = "64" in s if arch == 64 else "64" not in s
                postgres_test = "Дистрибутив СУБД PostgreSQL для" in s
                additional_modules_test = "дополнительные модули" not in s
                return os_test and arch_test and postgres_test and \
                    additional_modules_test

            return f

        # reset browser opened page
        self.browser.open(self.portal_url+"/total")
        # get to platform versions page
        self.real_version = go_to_product_version_page(
            self.browser, "PostgreSQL", self.config["version"]
        )
        # get to download page
        os_type_str = {
            "Windows": "Windows", "Linux-deb": "DEB", "Linux-rpm": "RPM"
        }[self.config["target-os"]]
        try:
            find_link_and_go(
                self.browser, "a", None,
                get_selection_function(os_type_str, self.config["arch"])
            )
        except SACError as err:
            if err.str_code == "BROWSER_ERROR":
                raise SACError(
                    "LOGIC_ERROR",
                    "Can't find product distribution with specified parameters",
                    os=self.config["target-os"],
                    arch=self.config["target-arch"]
                )
            else:
                raise
        # get download link
        return self.browser.find(class_="downloadDist").find("a")["href"]

    def _get_available_tests(self):
        return [
            ["try-login", self._login_to_portal, False],
            ["check-url", self._get_url, False],
        ]

    def _process_files(self, src):
        if self.config["save-accurately"]:
            save_accurately(src, self.config["download-folder"], self.tmp)
        else:
            save_postgres(src, self.config["download-folder"], self.tmp,
                          self.real_version, self.config["target-os"],
                          self.config["target-arch"])


if __name__ == "__main__":
    DownloadPostgresScenario.main()
