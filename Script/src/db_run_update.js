const path = require("path");
const node_modules_path = path.resolve('../../node/node_modules');
const libPath = path.resolve('jslib');

const fs = require('fs');
const cmd = require(node_modules_path + '/node-cmd');

const args = require(libPath + '/argParser')(process.argv);

if (args['test-mode'] === 'True') return; 

const main = async () => {
  const command = `"${args.platform_path}" ENTERPRISE /F "${args.db_path}" /WA- /CВыполнитьОбновлениеИЗавершитьРаботу`;
  cmd.get(command, (err, data, stderr) => {
    if (err) throw err;
    console.log('the current working dir is : ',data)
  });
}

main();
