import os
import uuid
import shutil
import time
import datetime as dt
from enum import Enum


from lib.common import bootstrap
from lib.common.logger import global_logger, LogFunc
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError


SERVICE_CONTROL_DELAY = 30


class StartServiceScenario(BaseScenario):

    ## Available object properties:
    # self.config - ScenarioConfiguration object.
    # self.tmp - Path to temporary folder.

    def _validate_specific_data(self):
        validate_data = [
            ["service-name", str]
        ]
        self.config.validate(validate_data)

    def _after_init(self):
        if self.config["os-type"] == "Windows":
            from lib.win_utils.service import Service as Service
        else:
            from lib.linux_utils.service import SystemdService as Service
        self.ServiceCls = Service
        self.service = Service(self.config["service-name"])

    def _connect_service(self):
        self.service.connect()

    def _get_available_tests(self):
        return [
            ("test-sc-permissions", self.ServiceCls.test_sc_permissions, False),
            ("check-service-existence", self._connect_service, True)
        ]

    def _real(self):
        l = LogFunc(message="starting service",
                    service_name=self.service.name)
        # 3. Start services
        self.service.start()
        # 4. Check services started after timeout
        global_logger.info(message="Give service time to start...")
        endtime = dt.datetime.now() + \
                  dt.timedelta(seconds=SERVICE_CONTROL_DELAY)
        while(True):
            if self.service.started or dt.datetime.now() >= endtime:
                break
            time.sleep(1)
        if not self.service.started:
            raise SACError("SERVICE_ERROR",
                                         "service not started",
                                         name=self.service.name)


if __name__ == "__main__":
    StartServiceScenario.main()
