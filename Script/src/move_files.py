import os
import re


from lib.common import bootstrap
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from lib.utils.fs import move_file_or_directory, remove_file_or_directory


class MoveFilesScenario(BaseScenario):

    ## Available object properties:
    # self.config - ScenarioConfiguration object.
    # self.tmp - Path to temporary folder.

    def _validate_specific_data(self):
        validate_data = [
            ["source", StrPathExpanded],
            ["destination", StrPathExpanded],
            ["with-root", bool],
            ["replace", bool],
            ["pattern", str],
            ["invert-pattern", bool]
        ]
        self.config.validate(validate_data)
        if self.config["pattern"]:
            try:
                re.compile(self.config["pattern"])
            except re.error:
                raise SACError("ARGS_ERROR", "Invalid regular expression",
                               regex=self.config["pattern"])

    ## Check destination location.
    # @param self Pointer to object.
    def __check_destination_folder(self):
        # first, if destination directory doesn't exists, try to create it
        if not os.path.exists(self.config["destination"]):
            try:
                os.makedirs(self.config["destination"])
            except:
                raise SACError(
                    "ARGS_ERROR",
                    "Cannot create directory at specified location",
                    location=self.config["destination"]
                )
        else:
            if not os.path.isdir(self.config["destination"]) and \
               not self.config["replace"]:
                raise SACError(
                    "ARGS_ERROR",
                    "Destination path already exists and replace is not allowed",
                    destination_path=self.config["destination"]
                )

    ## Check source location existence.
    # @param self Pointer to object.
    def __check_source_file(self):
        if not os.path.exists(self.config["source"]):
            raise SACError("ARGS_ERROR", "Source path not exists",
                                         location=self.config["source"])

    def _get_available_tests(self):
        return [
            ["check-destination-folder", self.__check_destination_folder,
             False],
            ["check-source", self.__check_source_file, True]
        ]

    def _real(self):
        # create destination folder
        if os.path.exists(self.config["destination"]):
            if not os.path.isdir(self.config["destination"]):
                if self.config["replace"]:
                    remove_file_or_directory(self.config["destination"])
                else:
                    raise SACError(
                        "ARGS_ERROR",
                        "Destination path exists and not a directory",
                        path=self.config["destination"]
                    )
        else:
            os.makedirs(self.config["destination"])
        # perform move
        move_file_or_directory(self.config["source"],
                               self.config["destination"],
                               self.config["with-root"],
                               self.config["pattern"],
                               self.config["invert-pattern"])


if __name__ == "__main__":
    MoveFilesScenario.main()
