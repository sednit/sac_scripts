import threading


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.logger import global_logger
from lib.base_rac_scenario import BaseRacScenario


class SetServerParametersScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["cluster-port", int],
            ["server-parameters", dict],
        ]
        self.config.validate(validate_data)
        if self.config["server-host"] not in [None, ""]:
            validate_data = [
                ["server-host", str],
            ]
        self.config.validate(validate_data)

    def _get_additional_tests(self):
        return [
             ["check-cluster-existence", self._get_cluster_id_by_port, True],
        ]

    def __set_server_parameters(self, server_uuid, base_options,
                                result_list, result_index):
        # update server data
        options = base_options.copy()
        options["server"] = server_uuid
        for key, value in self.config["server-parameters"].items():
            if key not in options:
                options[key] = value
        try:
            result_list[result_index] = self._execute_rac("server", "update",
                                                          options)
        except Exception as err:
            global_logger.error(**err.to_dict())
            result_list[result_index] = None

    def _real(self):
        # find cluster uuid
        options = {"cluster": self._get_cluster_id_by_port()}
        # select server
        self._add_user_credentials(options)
        res = self._execute_rac("server", "list", options)
        parsed_output = self._parse_rac_output(bootstrap.try_decode(res.stdout))
        servers = {i["agent-host"]: i["server"] for i in parsed_output}
        required_servers_uuids = []
        if self.config["server-host"] not in [None, ""]:
            try:
                required_servers_uuids.append(
                    servers[self.config["server-host"]]
                )
            except KeyError:
                raise SACError(
                    "LOGIC_ERROR", "server with specified hoste doesn't exists",
                    server_name=self.config["server-host"]
                )
        else:
            required_servers_uuids = [v for _, v in servers.items()]
        # start working with each server in separated thread
        threads = []
        results = [None for _ in range(0, len(required_servers_uuids))]
        for i in range(0, len(required_servers_uuids)):
            server_uuid = required_servers_uuids[i]
            threads.append(threading.Thread(
                target=self.__set_server_parameters,
                args=(server_uuid, options, results, i)
            ))
            threads[-1].start()
        for thread in threads:
            thread.join()
        for i in results:
            if i is None:
                raise SACError("LOGIC_ERROR",
                               "Error occurred while working on servers")


if __name__ == "__main__":
    SetServerParametersScenario.main()
