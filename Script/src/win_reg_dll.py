#!/usr/bin/python3
# coding: utf-8
import os


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario
from lib.utils import try_open_file
from lib.utils.cmd import run_cmd


def call_regsvr32(dll_path, uninstall_flag=False, register_server_flag=True,
                  command=""):
    args = ["regsvr32", "/s"]
    if uninstall_flag:
        args.append("/u")
    if not register_server_flag:
        args.append("/n")
    if command != "" or not register_server_flag:
        args.append("/i")
        args.append(command)
    args.append(dll_path)
    res = run_cmd(args)
    if res.returncode != 0:
        raise SACError(
            "CMD_RESULT_ERROR", args=res.args,
            returncode=res.returncode,
            stdout=bootstrap.try_decode(res.stdout),
            stderr=bootstrap.try_decode(res.stderr)
        )


class WinRegDLLScenario(BaseScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["os-type", str, str, ["Windows"]],
            ["folder", StrPathExpanded],
            ["lib-name", str],
            ["command", str],
            ["register-server", bool],
            ["uninstall", bool]
        ]
        self.config.validate(validate_data)

    def _get_available_tests(self):
        return [
            ["check-dll-path", lambda: try_open_file(os.path.join(
                self.config["folder"], self.config["lib-name"]
            )), True],
        ]

    def _real(self):
        call_regsvr32(
            os.path.join(
                self.config["folder"], self.config["lib-name"]
            ),
            self.config["uninstall"],
            self.config["register-server"],
            self.config["command"]
        )


if __name__ == "__main__":
    WinRegDLLScenario.main()
