from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.base_scenario import BaseScenario


class DeleteServiceScenario(BaseScenario):

    def _after_init(self):
        if self.config["os-type"] == "Windows":
            from lib.win_utils import service
        else:
            from lib.linux_utils import service
        self.service_module = service

    def _service_started(self):
        # check that service not started
        srvc = self.service_module.Service(self.config["name"])
        if srvc.connect(ignore_errors=True):
            if srvc.started:
                raise SACError(
                    "SERVICE_ERROR", "service running",
                    service=self.config["name"]
                )

    def _test_sc_permissions(self):
        # check permissions for working with services
        if self.config["os-type"] != "Windows":
            return self.service_module.Service.test_sc_permissions()
        else:
            return self.service_module.Service.test_sc_permissions(None, None)

    def _get_available_tests(self):
        return [
            ("test-sc-permissions", self._test_sc_permissions, False),
            ("service-started", self._service_started, True),
        ]

    def _validate_specific_data(self):
        validate_data = [
            ["name", str],
        ]
        self.config.validate(validate_data)

    def _real(self):
        self.service_module.delete_service(self.config["name"])


if __name__ == "__main__":
    DeleteServiceScenario.main()
