import threading


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.logger import global_logger
from lib.base_rac_scenario import BaseRacScenario


class SetClusterParametersScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["cluster-parameters", dict],
        ]
        self.config.validate(validate_data)
        if self.config["cluster-port"] not in [None, ""]:
            validate_data = [
                ["cluster-port", int],
            ]
        self.config.validate(validate_data)

    def _get_additional_tests(self):
        if self.config["cluster-port"] not in [None, ""]:
            return [
                ["check-cluster-existence", self._get_cluster_id_by_port, True],
            ]
        else:
            return []

    def __set_cluster_parameters(self, cluster_uuid, base_options,
                                 result_list, result_index):
        options = base_options.copy()
        options["cluster"] = cluster_uuid
        for key, value in self.config["cluster-parameters"].items():
            if key not in options:
                options[key] = value
        try:
            result_list[result_index] = self._execute_rac("cluster", "update",
                                                          options)
        except Exception as err:
            global_logger.error(**err.to_dict())
            result_list[result_index] = None

    def _real(self):
        options = {}
        res = self._execute_rac("cluster", "list", {})
        parsed_output = self._parse_rac_output(bootstrap.try_decode(res.stdout))
        clusters = {int(i["port"]): i["cluster"] for i in parsed_output}
        self._add_user_credentials(options, "agent")
        required_clusters_uuids = []
        if self.config["cluster-port"] not in [None, ""]:
            try:
                required_clusters_uuids.append(
                    clusters[self.config["cluster-port"]]
                )
            except KeyError:
                raise SACError(
                    "LOGIC_ERROR",
                    "cluster with specified port doesn't exists",
                    cluster_port=self.config["cluster-port"]
                )
        else:
            required_clusters_uuids = [v for _, v in clusters.items()]
        # start working with each cluster in separated thread
        threads = []
        results = [None for _ in range(0, len(required_clusters_uuids))]
        for i in range(0, len(required_clusters_uuids)):
            cluster_uuid = required_clusters_uuids[i]
            threads.append(threading.Thread(
                target=self.__set_cluster_parameters,
                args=(cluster_uuid, options, results, i)
            ))
            threads[-1].start()
        for thread in threads:
            thread.join()
        for i in results:
            if i is None:
                raise SACError("LOGIC_ERROR",
                               "Error occurred while working on clusters")


if __name__ == "__main__":
    SetClusterParametersScenario.main()
