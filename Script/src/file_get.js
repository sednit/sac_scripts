const path = require("path");
const node_modules_path = path.resolve('../../node/node_modules');
const libPath = path.resolve('jslib');
const WebSocket = require(node_modules_path + '/ws');

const fs = require('fs');

const args = require(libPath + '/argParser')(process.argv);
const fileGet = require(libPath + '/fileGet');

if (args['test-mode'] === 'True') return; 

const main = async () => {
  await fileGet({
    host: args.host,
    port: args.port,
    dstPath: args['dst_path'],
    srcPath: args['src_path']
    name: args['dst_name'],
  });
}

main();
