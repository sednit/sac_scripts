from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError

class PingTestScenario(BaseScenario):

    def _validate_specific_data(self):
        pass

    def _real(self):
        raise SACError(
            "ARGS_ERROR",
            "Some errrorororor",
            path=self.config["destination"]
        )

if __name__ == "__main__":
  PingTestScenario.main()
