# coding: utf-8
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario


class WebExtensionUpdateAllPathsScenario(BaseScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["setup-folder", StrPathExpanded],
        ]
        self.config.validate(validate_data)

    def _get_available_tests(self):
        return [["test-web", self.test_web, True]]

    def _after_init(self):
        if self.config["os-type"] == "Windows":
            from lib.win_utils.platform_installer import test_web, setup_web
        else:
            from lib.linux_utils.platform_installer import test_web, setup_web
        self.test_web = lambda: test_web()
        self.setup_web = lambda: setup_web(self.config["setup-folder"])

    def _real(self):
        self.setup_web()


if __name__ == "__main__":
    WebExtensionUpdateAllPathsScenario.main()
