import os


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.logger import global_logger
from lib.base_rac_scenario import BaseRacScenario


class GetSessionsScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["cluster-port", int],
        ]
        self.config.validate(validate_data)
        if "infobase-name" not in self.config:
            self.config["infobase-name"] = None
        # if infobase name is in configuration, check it
        elif self.config["infobase-name"] not in [None, ""]:
            validate_data = [
                ["infobase-name", str],
            ]
        self.config.validate(validate_data)

    def _get_additional_tests(self):
        return [
             ["check-cluster-existence", self._get_cluster_id_by_port, True],
        ]

    def _real(self):
        # get cluster uuid
        options = {"cluster": self._get_cluster_id_by_port()}
        self._add_user_credentials(options)
        # if infobase specified, find it and get list of sessions for specific
        # infobase
        if self.config["infobase-name"] not in [None, ""]:
            # select infobase uuid
            res = self._execute_rac("infobase", "summary list", options)
            parsed_output = self._parse_rac_output(
                bootstrap.try_decode(res.stdout)
            )
            infobase = self._get_object_by_field(parsed_output, "name",
                                                 self.config["infobase-name"])
            if infobase is None:
                raise SACError(
                    "LOGIC_ERROR",
                    "infobase with specified name doesn't exists",
                    infobase_name=self.config["infobase-name"]
                )
            else:
                options["infobase"] = infobase["infobase"]
        # request session list and print it in log
        res = self._execute_rac("session", "list", options)
        global_logger.info(message="RAC output",
                           rac_output=bootstrap.try_decode(res.stdout))


if __name__ == "__main__":
    GetSessionsScenario.main()
