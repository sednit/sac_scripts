#!/usr/bin/python3
# coding: utf-8
import os
from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from requests import get

class FileDownload(BaseScenario):

    def _validate_specific_data(self):
        pass

    def _real(self):
        url = 'http://192.168.0.240:96/get_file?source=' + self.config['source'] 
        file_name = self.config['destination']
        with open(file_name, "wb") as file:
            response = get(url)
            file.write(response.content)

if __name__ == "__main__":
  FileDownload.main()