# coding: utf-8

import os
import socket
import subprocess as sp
import time


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.common.config import StrPathExpanded
from lib.base_rac_scenario import BaseRacScenario
from lib.utils import detect_actual_os_type, read_yaml
from lib.utils.cmd import run_cmd


AGENT_USER = None
AGENT_PWD = None
CLUSTER_UUID = None
CLUSTER_USER = None
CLUSTER_PWD = None
ALLOW_REMOVE_UNNECESSARY = False


## Find free network port.
# @return Port number.
def find_free_port():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(("", 0))
    addr, port = sock.getsockname()
    sock.close()
    return port


## Join multiple dicts. If key appear multiple times, last value is used.
# @param *dicts Dictionaries.
# @return Dictionary.
def join_dicts(*dicts):
    r = {}
    for d in dicts:
        for k, v in d.items():
            r[k] = v
    return r


## Class which represent started RAS.
class Ras:

    ## Find RAS executable in 1C:Enterprise folder. If OS is Linux,
    #  setup_folder parameter is omitted
    # @param setup_folder Location of 1C:Enterprise installation.
    # @return Path to RAS executable.
    @staticmethod
    def __find_ras_executable(setup_folder):
        if detect_actual_os_type() == "Windows":
            from lib.win_utils import find_file_in_1c_installation
            ras_executable = find_file_in_1c_installation(
                "ras.exe", StrPathExpanded(setup_folder)
            )
        else:
            from lib.linux_utils import find_file_in_1c_installation
            ras_executable = find_file_in_1c_installation(
                "ras", StrPathExpanded(setup_folder)
            )
        if ras_executable is None or not os.path.exists(ras_executable):
            raise SACError("LOGIC_ERROR",
                                         "cannot find ras executable")
        else:
            return ras_executable

    ## Constructor.
    # @param self Pointer to object.
    # @param host Cluster host name.
    # @param port Cluster port.
    # @param setup_folder Location of 1C:Enterprise installation.
    def __init__(self, host, port, setup_folder):
        self.target_host = host
        self.target_port = int(port)
        # set listening port for RAS
        self.port = find_free_port()
        # find RAS executable
        self.ras_exe = self.__find_ras_executable(setup_folder)
        # start RAS process
        self._proc = sp.Popen(
            [self.ras_exe, "cluster", "-p", str(self.port),
             "{}:{}".format(self.target_host, self.target_port)],
            stdout=sp.PIPE, stderr=sp.PIPE
        )

    ## Destructor.
    # @param self Pointer to object.
    def __del__(self):
        self._proc.terminate()


## Class, which represents RAC connection to RAS.
class Rac:

    ## Find RAC executable.
    # @param setup_folder Location of 1C:Enterprise installation.
    # @return Path to RAC executable.
    @staticmethod
    def __find_rac_executable(setup_folder):
        if detect_actual_os_type() == "Windows":
            from lib.win_utils import find_file_in_1c_installation
            rac_executable = find_file_in_1c_installation(
                "rac.exe", StrPathExpanded(setup_folder)
            )
        else:
            from lib.linux_utils import find_file_in_1c_installation
            rac_executable = find_file_in_1c_installation(
                "rac", StrPathExpanded(setup_folder)
            )
        if rac_executable is None or not os.path.exists(rac_executable):
            raise SACError("LOGIC_ERROR",
                                         "cannot find rac executable")
        else:
            return rac_executable

    ## Parse RAC output.
    # @param s String with rac result.
    # @return List of dictionaries.
    @staticmethod
    def _parse_rac_output(s):
        groups = []
        create_new_group_flag = True
        for l in [l.strip(" \r\n") for l in s.split("\n")]:
            # if current line is empty, assume that new group started on
            # next line
            if l == "":
                create_new_group_flag = True
            else:
                # if flag is set, create new group and reset flag
                if create_new_group_flag:
                    groups.append({})
                    create_new_group_flag = False
                split_line = l.split(":")
                key = split_line[0].strip(" ")
                value = ":".join(split_line[1:]).strip(" \"")
                groups[-1][key] = value
        return groups

    ## Constructor.
    # @param self Pointer to object.
    # @param host RAS host name.
    # @param port RAS port.
    # @param setup_folder Location of 1C:Enterprise installation.
    def __init__(self, host, port, setup_folder):
        self.host = host
        self.port = int(port)
        # get RAC executable path
        self.rac_exe = self.__find_rac_executable(setup_folder)
        # try to get list of clusters as test
        self.execute("cluster", "list", {})

    ## Execute RAC command.
    # @param mode Mode.
    # @param command Command and additional commands, separated by space.
    # @param options Dictionary with parameters.
    # @param key_only_options List of options, which doesn't have a value.
    def execute(self, mode, command, options, key_only_options=()):
        options = ["--{}={}".format(k, v) for k, v in options.items()]
        key_only_options = ["--{}".format(i) for i in key_only_options]
        ras_addr = "{}:{}".format(self.host, self.port)
        commands = [c.strip(" ") for c in command.split(" ")]
        # build command
        args = [self.rac_exe, mode] + commands + options + key_only_options + \
               [ras_addr, ]
        # execute command
        res = run_cmd(args, shell=False)
        if res.returncode != 0:
            raise SACError(
                "CMD_RESULT_ERROR",
                returncode=res.returncode,
                stdout=bootstrap.try_decode(res.stdout),
                stderr=bootstrap.try_decode(res.stderr),
                args=args
            )
        return self._parse_rac_output(bootstrap.try_decode(res.stdout))


## Check that key is in data and conform with specified rules.
# @param data Data.
# @param key_name Key, which is tested.
# @param allowed_types Allowed types of value. If empty list, any allowed.
# @param allowed_values Allowed values. If empty list, allowed any.
# @param nullable Value can be None. Overrides allowed_types and allowed_values.
# @param default Default value, if key not presented.
# @param mapping If value is key in this dictionary, it will be replaced with
#  value of this key.
# @return Value.
def check_value(data, key_name, allowed_types=[str, int],
                allowed_values=(), nullable=True, default=None, mapping={}):
    # try to get value from data
    try:
        value = data[key_name]
    # if failed to obtain key, raise exception, if the key is not nullable or is
    # set
    # default value otherwise
    except:
        if nullable:
            value = default
        else:
            raise SACError(
                "ARGS_ERROR", "Key cannot be omitted", key=key_name)
    # check type. If nullable, append NoneType to allowed types.
    if len(allowed_types) > 0:
        if nullable:
            allowed_types = list(allowed_types) + [type(None)]
        if type(value) not in allowed_types:
            raise SACError(
                "ARGS_ERROR", "Value have invalid type", key=key_name,
                current_type=type(value), allowed_types=allowed_types
            )
    # check value. If nullable, append None to allowed values.
    if len(allowed_values) > 0:
        if nullable:
            allowed_values = list(allowed_values) + [None]
        if value not in allowed_values:
            raise SACError(
                "ARGS_ERROR", "Value incorrect", key=key_name, value=value,
                allowed_values=allowed_values
            )
    # if value is on mapping key, replace the value with the value from
    # dictionary
    if value in mapping:
        value = mapping[value]
    return value


## Prepare administrator data.
# @param data Source data.
# @return Cleaned data.
def clean_admin_data(data):
    clean_data = {}
    clean_data["name"] = check_value(data, "name", [str], nullable=False)
    # required keys.
    keys = ["pwd", "descr", "auth", "os-user"]
    for key in keys:
        clean_data[key] = check_value(data, key)
    # replace | in auth filed with comma
    if clean_data["auth"] is not None:
        clean_data["auth"].replace("|", ",")
    return {k: v for k, v in clean_data.items() if v is not None}


## Set agent administrators.
# @param Rac object.
# @param admins Administrators data (list of dictionaries).
def set_agent_admin_list(rac, admins):

    def get_admin_data():
        options = {}
        # first try to get access without administrator data
        try:
            return None, None, rac.execute("agent", "admin list",
                                           options)
        # if fails, try to get access with any specified admin
        except SACError:
            for admin in admins:
                try:
                    options["agent-user"] = admin["name"]
                    options["agent-pwd"] = admin["pwd"]
                    return admin["name"], admin["pwd"], rac.execute(
                        "agent", "admin list", options
                    )
                except:
                    continue
            raise SACError(
                "LOGIC_ERROR",
                "None of specified agent administrators is "
                "have access"
            )

    # remove all current administrators
    admin_data = get_admin_data()
    options = {}
    if admin_data[0] is not None:
        options["agent-user"] = admin_data[0]
        if admin_data[1] is not None:
            options["agent-pwd"] = admin_data[1]
        # remove all admins
        for admin in admin_data[2]:
            if admin["name"] != admin_data[0]:
                options["name"] = admin["name"]
                rac.execute("agent", "admin remove", options)
        options["name"] = admin_data[0]
        rac.execute("agent", "admin remove", options)
        del options["agent-user"]
        del options["agent-pwd"]
    # create first administrator
    if len(admins) < 1:
        return
    first_admin = None
    # find first administrator with password
    for admin in admins:
        local_options = options.copy()
        data = clean_admin_data(admin)
        if "pwd" in data["auth"] and data["pwd"] is not None:
            for key, value in admin.items():
                local_options[key] = value
            rac.execute("agent", "admin register", local_options)
            first_admin = (data["name"], data["pwd"])
            break
    if first_admin is None:
        raise SACError(
            "ARGS_ERROR",
            "cluster administrators list doesn't contain any administrator "
            "with password authentication"
        )
    # create rest of administrators
    options["agent-user"] = first_admin[0]
    options["agent-pwd"] = first_admin[1]
    for admin in admins:
        local_options = options.copy()
        data = clean_admin_data(admin)
        if data["name"] != first_admin[0]:
            for key, value in data.items():
                local_options[key] = value
            rac.execute("agent", "admin register", local_options)
    global AGENT_USER, AGENT_PWD
    AGENT_USER = first_admin[0]
    AGENT_PWD = first_admin[1]


## Return dictionary with already substituted agent user's values.
# @return Dictionary.
def agent_options():
    options = {}
    if AGENT_USER is not None:
        options["agent-user"] = AGENT_USER
        if AGENT_PWD is not None:
            options["agent-pwd"] = AGENT_PWD
    return options


## Apply pattern parameters to data. Data automatically separated on two
#  dictionaries: for create command and for update.
#  Pattern explanation: each element describe parameter for
#  check_value function: Elements meaning:
#  1 (position) - 0 for create command, 1 for update;
#  2 - keys, which is use this pattern;
#  3 - allowed types;
#  4 - allowed values;
#  5 - nullable;
#  6 - default;
#  7 - meaning.
# @param data Source data.
# @param patterns List of patterns.
# @return 2 dictionaries: for "create" command and for "update".
def apply_parameters_patterns(data, patterns):
    result_dicts = ({}, {})
    for pattern in patterns:
        for key in pattern[1]:
            result_dicts[pattern[0]][key] = check_value(data, key, *pattern[2:])
    return result_dicts


## Prepare cluster data.
# @param data Source data.
# @return Cleaned data.
def prepare_cluster_data(data):
    # parameters patterns
    patterns = [
        [0, ["host"], [str], (), False],
        [0, ["port"], [int], (), False],
        [1, ["name"], [str]],
        [1, ["expiration-timeout", "lifetime-limit", "max-memory-size",
             "max-memory-time-limit", "security-level",
             "session-fault-tolerance-level", "errors-count-threshold"],
         [int]],
        [1, ["kill-problem-processes"], [str, bool, int],
         (True, False, "yes", "no"),
         True, None, {True: "yes", False: "no"}],
        [1, ["load-balancing-mode"], [str], ("performance", "memory")]
    ]
    create_options, update_options = apply_parameters_patterns(data, patterns)
    return {k: v for k, v in create_options.items() if v is not None}, \
        {k: v for k, v in update_options.items() if v is not None}


## Update (and create, if not exists) cluster.
# @param Rac object.
# @param data Source data.
# @return Cluster UUID as string.
def create_or_update_cluster(rac, data):
    create_options, update_options = prepare_cluster_data(data["cluster"])
    cluster_uuid = None
    # find existing cluster
    for cluster in rac.execute("cluster", "list", {}):
        if int(cluster["port"]) == create_options["port"]:
            cluster_uuid = cluster["cluster"]
            break
    # if not found, create it
    if cluster_uuid is None:
        options = join_dicts(create_options,
                             agent_options())
        cluster_uuid = rac.execute(
            "cluster", "insert", options
        )[0]["cluster"]
    # now perform update
    options = join_dicts(update_options, agent_options())
    options["cluster"] = cluster_uuid
    rac.execute("cluster", "update", options)
    return cluster_uuid


## Set cluster administrators.
# @param Rac object.
# @param admins Administrators data (list of dictionaries).
def set_cluster_admin_list(rac, admins):
    def get_admin_data():
        options = {}
        options["cluster"] = CLUSTER_UUID
        # first try to get access without administrator data
        try:
            return None, None, rac.execute("cluster", "admin list",
                                           options)
        # if fails, try to get access with any specified admin
        except:
            for admin in admins:
                try:
                    options["cluster-user"] = admin["name"]
                    options["cluster-pwd"] = admin["pwd"]
                    return admin["name"], admin["pwd"], rac.execute(
                        "cluster", "admin list", options
                    )
                except:
                    continue
            raise SACError(
                "LOGIC_ERROR",
                "None of specified cluster administrators is "
                "have access"
            )

    admin_data = get_admin_data()
    options = {"cluster": CLUSTER_UUID}
    if admin_data[0] is not None:
        options["cluster-user"] = admin_data[0]
        if admin_data[1] is not None:
            options["cluster-pwd"] = admin_data[1]
        # remove all admins
        for admin in admin_data[2]:
            if admin["name"] != admin_data[0]:
                options["name"] = admin["name"]
                rac.execute("cluster", "admin remove", options)
        options["name"] = admin_data[0]
        rac.execute("cluster", "admin remove", options)
        del options["cluster-user"]
        del options["cluster-pwd"]
    # create first admin
    if len(admins) < 1:
        return
    first_admin = None
    for admin in admins:
        data = clean_admin_data(admin)
        if "pwd" in data["auth"] and data["pwd"] is not None:
            options = join_dicts(options, agent_options(), data)
            rac.execute("cluster", "admin register", options)
            first_admin = (data["name"], data["pwd"])
            break
    if first_admin is None:
        raise SACError(
            "ARGS_ERROR",
            "cluster administrators list doesn't contain any administrator "
            "with password authentication"
        )
    # create rest of admins
    options["cluster-user"] = first_admin[0]
    options["cluster-pwd"] = first_admin[1]
    for admin in admins:
        data = clean_admin_data(admin)
        if data["name"] != first_admin[0]:
            options = join_dicts(options, agent_options(), data)
            rac.execute("cluster", "admin register", options)
    global CLUSTER_USER, CLUSTER_PWD
    CLUSTER_USER = first_admin[0]
    CLUSTER_PWD = first_admin[1]


## Return dictionary with already substituted cluster's values.
# @return Dictionary.
def cluster_options():
    options = {"cluster": CLUSTER_UUID}
    if CLUSTER_USER is not None:
        options["cluster-user"] = CLUSTER_USER
        if CLUSTER_PWD is not None:
            options["cluster-pwd"] = CLUSTER_PWD
    return options


## Prepare cluster data.
# @param data Source data.
# @return Cleaned data.
def prepare_server_data(data):
    patterns = [
        [0, ["agent-host"], [str], (), False],
        [0, ["agent-port"], [int], (), False],
        [0, ["port-range"], [str], (), False],
        [0, ["cluster-port"], [int], (), False],
        [0, ["name"], [str], (), False],
        [1, ["port-range"], [str], (), False],
        [1, ["using"], [str], ("main", "normal")],
        [1, ["dedicate-managers"], [str], ("all", "none")],
        [1, ["infobases-limit", "memory-limit",
             "connections-limit", "safe-working-processes-memory-limit",
             "safe-call-memory-limit"],
         [int]]
    ]
    create_options, update_options = apply_parameters_patterns(data, patterns)
    return {k: v for k, v in create_options.items() if v is not None}, \
        {k: v for k, v in update_options.items() if v is not None}


## Update (and create, if not exists) server.
# @param Rac object.
# @param data Source data.
# @return Server UUID as string.
def create_or_update_server(rac, data):
    create_options, update_options = prepare_server_data(data)
    server_uuid = None
    # find existing cluster
    for server in rac.execute("server", "list", cluster_options()):
        if str(server["agent-port"]) == str(create_options["agent-port"]) and \
           str(server["agent-host"]) == str(create_options["agent-host"]):
            server_uuid = server["server"]
            break
    # if not found, create it
    if server_uuid is None:
        server_uuid = rac.execute(
            "server", "insert", join_dicts(create_options, cluster_options())
        )[0]["server"]
    # now perform update
    options = join_dicts(update_options, cluster_options())
    options["server"] = server_uuid
    rac.execute("server", "update", options)
    return server_uuid


## Get information about server.
# @param rac Rac object.
# @param server_uuid Server UUID as string.
# @return Dictionary with data.
def get_server_info(rac, server_uuid):
    options = cluster_options()
    options["server"] = server_uuid
    return rac.execute("server", "info", options)[0]


## Check that working processes on server started.
# @param rac Rac object.
# @param server_uuid Server UUID as string.
def check_server_processes(rac, server_uuid):
    options = cluster_options()
    options["server"] = server_uuid
    started = False
    # make test 3 times with 1 second delay
    for _ in range(0, 3):
        not_started_found = False
        # check each process
        for process in rac.execute("process", "list", options):
            if process["running"] != "yes":
                not_started_found = True
                break
        if not_started_found:
            time.sleep(1)
            continue
        else:
            started = True
            break
    if started:
        return
    raise SACError(
        "LOGIC_ERROR", "Server not started", host=process["host"]
    )


## Remove server from cluster.
# @param rac Rac object.
# @param server_uuid Server UUID as string.
def remove_server(rac, server_uuid):
    # if global flag is disabled, raise an error
    if not ALLOW_REMOVE_UNNECESSARY:
        raise SACError(
            "LOGIC_ERROR", "Removal of unnecessary objects"
            "disabled"
        )
    # if server is main, make in normal before removal
    if get_server_info(rac, server_uuid)["using"] == "main":
        options = cluster_options()
        options["server"] = server_uuid
        options["using"] = "normal"
        rac.execute("server", "update", options)
    options = cluster_options()
    options["server"] = server_uuid
    # remove server
    rac.execute("server", "remove", options)


## Remove clusters on working server.
# @param agent_host Host name of server's agent.
# @param agent_port Port of server's agent.
# @param setup_folder Location of 1C:Enterprise installation.
# @param current_cluster_host Host name of current server.
def clean_clusters(agent_host, agent_port, setup_folder, current_cluster_host):
    # if agent_host is current server, do nothing
    if agent_host == current_cluster_host:
        return
    # start local Ras to remove server
    try:
        ras = Ras(agent_host, agent_port, setup_folder)
    except:
        raise SACError(
            "LOGIC_ERROR", "Cannot start ras for server", host=agent_host,
            port=agent_port
        )
    # execute RAC command
    rac = Rac("localhost", ras.port, setup_folder)
    # check each cluster
    for cluster in rac.execute("cluster", "list", {}):
        # if cluster uuid is equal to current cluster uuid, skip it
        if cluster["cluster"] == CLUSTER_UUID:
            continue
        # get list of servers in cluster
        servers = rac.execute("server", "list",
                              {"cluster": cluster["cluster"]})
        # if at least one server working on same host and port as host and port
        # in parameters, remove that cluster
        for server in servers:
            if server["agent-host"] == agent_host and \
               int(server["agent-port"]) == int(agent_port):
                if not ALLOW_REMOVE_UNNECESSARY:
                    raise SACError(
                        "LOGIC_ERROR", "Removal of unnecessary objects"
                        "disabled"
                    )
                rac.execute("cluster", "remove",
                            {"cluster": cluster["cluster"]})
                break


## Set servers in cluster.
# @param rac Rac object.
# @param data Source data.
# @param setup_folder Location of 1C:Enterprise installation.
# @return List of servers' UUIDs.
def set_server_list(rac, data, setup_folder):
    options = cluster_options()
    # store the list of currently existing servers
    existing_servers = [
        i["server"] for i in rac.execute("server", "list", options)
    ]
    new_servers = []
    # for each server in source data clean it, create new server and check
    # the working processes
    for i in data["servers"]:
        clean_clusters(i["agent-host"], i["agent-port"], setup_folder,
                       data["cluster"]["host"])
        new_servers.append(create_or_update_server(rac, i))
        check_server_processes(rac, new_servers[-1])
    # remove all servers which are not in source data
    for es in existing_servers:
        if es not in new_servers:
            remove_server(rac, es)
    return new_servers


## Prepare rule data.
# @param data Source data.
# @return Cleaned data.
def prepare_rule_data(data):
    patterns = [
        [0, ["position"], [int], (), False],
        [1, ["object-type", "infobase-name", "application-ext"], [str]],
        [1, ["infobase-name"], [str]],
        [1, ["rule-type"], [str], ("auto", "always", "never")],
        [1, ["priority"], [int]]
    ]
    create_options, update_options = apply_parameters_patterns(data, patterns)
    return {k: v for k, v in create_options.items() if v is not None}, \
        {k: v for k, v in update_options.items() if v is not None}


## Update (and create, if not exists) rule.
# @param Rac object.
# @param data Source data.
# @param server_uuid Server UUID as string.
# @return Rule UUID as string.
def create_or_update_rule(rac, data, server_uuid):
    rule_uuid = None
    create_options, update_options = prepare_rule_data(data)
    options = cluster_options()
    options["server"] = server_uuid
    # try to found already existing rule
    for rule in rac.execute("rule", "list", options):
        if int(rule["position"]) == int(data["position"]):
            rule_uuid = rule["rule"]
            break
    # if the rule does not exist, create a new rule
    if rule_uuid is None:
        options = join_dicts(options, create_options)
        rule_uuid = rac.execute("rule", "insert", options)[0]["rule"]
    options["rule"] = rule_uuid
    # update rule
    rac.execute("rule", "update", join_dicts(options, update_options))
    return rule_uuid


## Set rules in cluster.
# @param rac Rac object.
# @param data Source data.
# @param server List of servers' UUIDs.
def set_rule_list(rac, data, servers):
    base_options = cluster_options()
    # first: remove all previous existing rules
    for server_uuid in servers:
        options = base_options.copy()
        options["server"] = server_uuid
        for rule in rac.execute("rule", "list", options):
            options["rule"] = rule["rule"]
            rac.execute("rule", "remove", options)
    servers_hosts = {}
    # map server hosts to its uuid
    for server_uuid in servers:
        d = get_server_info(rac, server_uuid)
        servers_hosts[d["agent-host"]] = server_uuid
    # now create new rules
    for rule_data in data:
        try:
            server_uuid = servers_hosts[rule_data["host"]]
        except:
            raise SACError(
                "ARGS_ERROR", "Cannot find server with specified host",
                host=rule_data["host"]
            )
        create_or_update_rule(rac, rule_data, server_uuid)


## Apply rules in cluster.
# @param rac Rac object.
# @param apply_mode Apply mode for rules. Can be full or partial.
def apply_rules(rac, apply_mode):
    options = cluster_options()
    rac.execute("rule", "apply", options, [apply_mode, ])


## Prepare infobase data.
# @param data Source data.
# @return Cleaned data.
def prepare_infobase_data(data):
    patterns = [
        [0, ["name", "db-server", "db-name", "locale"], [str], (), False],
        [0, ["dbms"], [str], ("MSSQLServer", "PostgreSQL", "IBMDB2",
                              "OracleDatabase"), False],
        [0, ["db-user", "db-pwd"], [str]],
        [0, ["date-offset"], [int, str]],
        [1, ["db-server", "db-name"], [str], (), False],
        [1, ["dbms"], [str], ("MSSQLServer", "PostgreSQL", "IBMDB2",
                              "OracleDatabase"), False],
        [1, ["infobase-user", "infobase-pwd", "db-user", "db-pwd", "descr",
             "denied-from", "denied-to", "denied-message",
             "security-profile-name", "safe-mode-security-profile-name",
             "external-session-manager-connection-string"], [str]],
        [1, ["permission-code"], [str, int]],
        [1, ["sessions-deny", "scheduled-jobs-deny"], [str, bool],
         (True, False, "on", "off"), True, None, {True: "on", False: "off"}],
        [1, ["external-session-manager-required"], [bool, str],
         (True, False, "yes", "no"), True, None, {True: "yes", False: "no"}],
        [1, ["license-distribution"], [str], ("allow", "deny")]
    ]
    create_options, update_options = apply_parameters_patterns(data, patterns)
    return {k: v for k, v in create_options.items() if v is not None}, \
        {k: v for k, v in update_options.items() if v is not None}


## Update (and create, if not exists) infobase.
# @param Rac object.
# @param data Source data.
# @return Infobase UUID as string.
def create_or_update_infobase(rac, data):
    infobase_uuid = None
    create_options, update_options = prepare_infobase_data(data)
    options = cluster_options()
    # try to find already existing infobase
    for ib in rac.execute("infobase", "summary list", options):
            if ib["name"] == create_options["name"]:
                infobase_uuid = ib["infobase"]
                break
    # if infobase does not exists, create the one
    if infobase_uuid is None:
        options = join_dicts(options, create_options)
        key_only_options = []
        if "create-database" in data and \
           data["create-database"] in [True, "yes"]:
            key_only_options.append("create-database")
        infobase_uuid = rac.execute("infobase", "create",
                                    options, key_only_options)[0]["infobase"]
    options = cluster_options()
    options["infobase"] = infobase_uuid
    # update infobase
    rac.execute("infobase", "update", join_dicts(options, update_options))
    return infobase_uuid


## Set infobase list in cluster.
# @param Rac object.
# @param Source data.
# @param drop_data DB drop mode (drop-database or clean-database).
def set_infobase_list(rac, data, drop_data):
    infobases = []
    for entry in data:
        infobases.append(create_or_update_infobase(rac, entry))
    options = cluster_options()
    existing_infobases = rac.execute("infobase", "summary list", options)
    for eib in existing_infobases:
        if eib["infobase"] not in infobases:
            options["infobase"] = eib["infobase"]
            if not ALLOW_REMOVE_UNNECESSARY:
                raise SACError(
                    "LOGIC_ERROR", "Removal of unnecessary objects"
                    "disabled"
                )
            rac.execute("infobase", "drop", options,
                        [drop_data, ] if drop_data is not None else [])


## Validate input structure (try to clean each element).
# @param structure Input structure data.
def validate_structure(structure):
    for admin in structure["agent-admins"]:
        clean_admin_data(admin)
    prepare_cluster_data(structure["cluster"])
    for admin in structure["cluster"]["admins"]:
        clean_admin_data(admin)
    for server in structure["servers"]:
        prepare_server_data(server)
    for rule in structure["rules"]:
        prepare_rule_data(rule)
    for infobase in structure["infobases"]:
        prepare_infobase_data(infobase)


## Connect to cluster via RAS and RAC and setup cluster.
# @param cluster_structure Cluster structure.
# @param ras_host Host name of target RAS.
# @param ras_port Port of target RAS.
def setup_cluster(cluster_structure, ras_host, ras_port, setup_folder):
    # validate input cluster structure
    validate_structure(cluster_structure)
    # try to make connection to cluster via remotely started RAS
    try:
        rac = Rac(ras_host, ras_port, setup_folder)
    # if failed, start RAS locally
    except SACError as err:
        try:
            cluster_agent_server = [
                i for i in cluster_structure["servers"] if \
                i["agent-host"] == cluster_structure["cluster"]["host"]
            ][0]
            ras = Ras(cluster_agent_server["agent-host"],
                      cluster_agent_server["agent-port"])
            time.sleep(1)
            rac = Rac("localhost", ras.port, setup_folder)
        except:
            raise err
    # set global cluster uuid
    global CLUSTER_UUID
    set_agent_admin_list(rac, cluster_structure["agent-admins"])
    CLUSTER_UUID = create_or_update_cluster(rac, cluster_structure)
    set_cluster_admin_list(rac, cluster_structure["cluster"]["admins"])
    servers = set_server_list(rac, cluster_structure, setup_folder)
    set_infobase_list(rac, cluster_structure["infobases"],
                      cluster_structure["infobase-drop-db-mode"])
    set_rule_list(rac, cluster_structure["rules"], servers)
    if cluster_structure["rule-apply-mode"] in ["full", "partial"]:
        apply_rules(rac, cluster_structure["rule-apply-mode"])


class SetupClusterScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["structure-file", StrPathExpanded],
            ["allow-remove-unnecessary", bool],
        ]
        self.config.validate(validate_data)

    def __read_structure(self):
        return read_yaml(self.config["structure-file"])

    def _get_additional_tests(self):
        return [
             ["read-structure", self.__read_structure, False],
        ]

    def _real(self):
        global ALLOW_REMOVE_UNNECESSARY
        ALLOW_REMOVE_UNNECESSARY = self.config["allow-remove-unnecessary"]
        setup_cluster(
            self.__read_structure(), self.config["host"], self.config["port"],
            self.config["setup-folder"]
        )


if __name__ == "__main__":
    SetupClusterScenario.main()
