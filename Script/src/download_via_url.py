# coding: utf-8

import re
import shutil
import os
import configparser
from urllib.parse import urlparse


from lib.common import bootstrap
from lib.base_download_release import BaseDownloadReleaseScenario, \
    save_configuration, save_configuration_accurately, save_accurately, \
    save_platform, save_postgres
from lib.common.errors import SACError
from lib.utils import unpack_archive, splitext_archive


def remove_file_or_dir(path):
    if os.path.isfile(path):
        os.remove(path)
    elif os.path.isdir(path):
        shutil.rmtree(path)
    else:
        raise ValueError("Path not a file nor a directory")


class DownloadViaUrlScenario(BaseDownloadReleaseScenario):

    def _validate_additional_data(self):
        validate_data = [
            ["url", str],
        ]
        self.config.validate(validate_data)

    def _get_url(self):
        # check that URL is belongs to 1C
        if not self.config["url"].startswith("//") and \
           not self.config["url"].startswith("http://") and \
           not self.config["url"].startswith("https://"):
            self.config["url"] = "https://" + self.config["url"]
        if not re.match(".*\\.?1c\\.(?:ru|eu)",
                        urlparse(self.config["url"]).netloc):
            raise SACError(
                "ARGS_ERROR", "URL not seems like 1C portal",
                url=self.config["url"]
            )
        return self.config["url"]

    ## Deduce product type (platform, postgres or configuration).
    # @param self Pointer to object.
    # @param src Source location.
    def __deduce_product_type(self, src):
        if "setup1c" in os.path.basename(src) or \
           "updsetup" in os.path.basename(src):
            return "configuration"
        elif "postgres" in os.path.basename(src):
            return "postgres"
        else:
            found = False
            for i in ["setuptc", "deb", "windows", "rpm"]:
                if i in os.path.basename(src):
                    found = True
                    break
            if found:
                return "platform"
        return None

    def _get_available_tests(self):
        return [
            ["check-url", self._get_url, False],
            ["try-login", self._login_to_portal, False],
        ]

    ## Deduce platform parameters.
    # @param self Pointer to object.
    # @param src Source location.
    # @return Tuple with parameters.
    def __deduce_platform_params(self, src):
        base_src = os.path.basename(src)
        if "setuptc" in base_src or "windows" in base_src:
            os_type = "Windows"
        elif "deb" in base_src:
            os_type = "Linux-deb"
        elif "rpm" in base_src:
            os_type = "Linux-rpm"
        else:
            return None
        if "64" in base_src:
            arch = "64"
        else:
            arch = "32"
        if os_type == "Windows":
            if "full" in base_src:
                distr_type = "full"
            elif "setuptc" in base_src:
                distr_type = "thin-client"
            elif arch == "64":
                distr_type = "server"
            else:
                distr_type = "full"
        else:
            if "thin.client" in base_src:
                distr_type = "thin-client"
            elif "client" in base_src:
                distr_type = "client"
            else:
                distr_type = "server"
        tmp = os.path.join(self.tmp, "unpacked")
        unpack_archive(src, tmp)
        if os_type == "Windows":
            try:
                config = configparser.ConfigParser()
                try:
                    open(os.path.join(tmp, "Setup.ini"))
                    config.read(os.path.join(tmp, "Setup.ini"))
                except:
                    open(os.path.join(tmp, "setup.ini"))
                    config.read(os.path.join(tmp, "setup.ini"))
                version = config["Startup"]["ProductVersion"]
            except:
                shutil.rmtree(tmp)
                return None
        else:
            try:
                # get name of any archive
                archive = [
                    f for f in os.listdir(tmp) \
                    if splitext_archive(f)[1] in [".deb", ".rpm"]
                ][0]
                version = re.search("(\\d+\.\\d+\.\\d+-\\d+)",
                                    archive).groups()[0].replace("-", ".")
            except:
                shutil.rmtree(tmp)
                return None
        return tmp, version, os_type, arch, distr_type

    ## Deduce PostgreSQL parameters.
    # @param self Pointer to object.
    # @param src Source location.
    # @return Tuple with parameters.
    def __deduce_postgres_params(self, src):
        postgres_regex = re.compile(
            r"postgresql(?:-\d+\.\d+|)(?:-|_)(\d+)\.(\d+)\.(\d+)-(\d+)\.1(?:C|С"
            ")(?:_|\.|)(\(x64\)|i386|amd64|i686|x86_64|)\.(msi|deb|rpm)"
        )
        tmp = os.path.join(self.tmp, "unpacked")
        unpack_archive(src, tmp)
        postgres_data = None
        for root, dirs, files in os.walk(tmp):
            for file_ in files:
                match = re.search(postgres_regex, file_)
                if match is not None:
                    postgres_data = match.groups()
                    break
            if postgres_data is not None:
                break
        if postgres_data is None:
            shutil.rmtree(tmp)
            return None
        try:
            version = "{}.{}.{}-{}.1C".format(*postgres_data[0:4])
            os_type = {"deb": "Linux-deb", "rpm": "Linux-rpm",
                       "msi": "Windows"}[postgres_data[5]]
            if os_type == "Linux-deb":
                arch = {"i386": 32, "amd64": 64}[postgres_data[4]]
            elif os_type == "Linux-rpm":
                arch = {"i686": 32, "x86_64": 64}[postgres_data[4]]
            elif os_type == "Windows":
                arch = {"": 32, "(x64)": 64}[postgres_data[4]]
            return tmp, version, os_type, arch
        except:
            return None

    ## Process downloaded files.
    # @param self Pointer to object.
    # @param src Source location.
    def _process_files(self, src):
        try:
            # deduce product type and perform specific action
            deduced_type = self.__deduce_product_type(src)
            if deduced_type == "configuration":
                # if flag set, save product directly to destination folder
                if self.config["save-accurately"]:
                    save_configuration_accurately(
                        src, self.config["download-folder"], self.tmp
                    )
                # otherwise save in folders structure
                else:
                    save_configuration(src, self.config["download-folder"],
                                       self.tmp)
            elif deduced_type == "platform":
                params = self.__deduce_platform_params(src)
                if params is None:
                    raise Exception
                if self.config["save-accurately"]:
                    save_accurately(src, self.config["download-folder"],
                                    self.tmp)
                else:
                    save_platform(params[0], self.config["download-folder"],
                                  self.tmp, *params[1:])
            elif deduced_type == "postgres":
                params = self.__deduce_postgres_params(src)
                if params is None:
                    raise Exception
                if self.config["save-accurately"]:
                    save_accurately(src, self.config["download-folder"],
                                    self.tmp)
                else:
                    save_postgres(params[0], self.config["download-folder"],
                                  self.tmp, *params[1:])
            else:
                raise Exception
        except:
            remove_file_or_dir(src)
            raise SACError("LOGIC_ERROR", "Unknown content")


if __name__ == "__main__":
    DownloadViaUrlScenario.main()
