import os


from lib.common import bootstrap
from lib.common.logger import global_logger
from lib.base_rac_scenario import BaseRacScenario


class ExecuteRacScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["mode", str],
            ["command", str],
            ["options", dict]
        ]
        self.config.validate(validate_data)

    def _real(self):
        # execute RAC command and print output
        res = self._execute_rac(self.config["mode"], self.config["command"],
                                self.config["options"])
        global_logger.info(message="RAC output",
                           rac_output=bootstrap.try_decode(res.stdout))


if __name__ == "__main__":
    ExecuteRacScenario.main()
