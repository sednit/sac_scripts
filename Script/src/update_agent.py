import os
import time
import shlex
import datetime as dt


from lib.common import bootstrap
from lib.common.config import StrPathExpanded
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from lib.utils.fs import copy_file_or_directory
from lib.utils.cmd import run_cmd


def wait_for_time(seconds, period=1, check_func=lambda: False):
    endtime = dt.datetime.now() + \
              dt.timedelta(seconds=seconds)
    while(True):
        if check_func() or dt.datetime.now() >= endtime:
            break
        time.sleep(period)


class UpdateAgentScenario(BaseScenario):

    ## Available object properties:
    # self.config - ScenarioConfiguration object.
    # self.tmp - Path to temporary folder.

    def _validate_specific_data(self):
        validate_data = [
            ["distr-folder", StrPathExpanded],
            ["setup-folder", StrPathExpanded],
            ["service-name", str],
            ["keep-configs", bool],
            ["dumps-folder", StrPathExpanded],
        ]
        self.config.validate(validate_data)

    def _after_init(self):
        if self.config["os-type"] == "Windows":
            from lib.win_utils.service import Service as Service
        else:
            from lib.linux_utils.service import SystemdService as Service
        self.ServiceCls = Service
        self.service = Service(self.config["service-name"])

    def _connect_service(self):
        self.service.connect()

    def __check_distr_folder(self):
        if not os.path.exists(self.config["distr-folder"]):
            raise SACError(
                "ARGS_ERROR", "Distr folder not exists",
                location=self.config["distr-folder"]
            )

    def _get_available_tests(self):
        return [
            ("check-distr-folder", self.__check_distr_folder, False),
            ("test-sc-permissions", self.ServiceCls.test_sc_permissions, False),
            ("check-service-existence", self._connect_service, True)
        ]

    def _stop_service(self):
        self.service.stop(False)
        wait_for_time(30, 1, lambda: not self.service.started)
        if self.service.started:
            self.service.stop(True, self.config["dumps-folder"])
            wait_for_time(30, 1, lambda: not self.service.started)
            if self.service.started:
                raise SACError("SERVICE_ERROR",
                               "service not stopped",
                               name=self.service.name)

    def _start_service(self):
        self.service.start()
        wait_for_time(30, 1, lambda: self.service.started)
        if not self.service.started:
            raise SACError("SERVICE_ERROR",
                           "service not started",
                           name=self.service.name)

    def _real(self):
        self._stop_service()
        CONFIGS_EXT = [".xml", ".conf", ".cfg", ".ini", ".xsl", ".yml", ".yaml"]
        for f in os.listdir(self.config["distr-folder"]):
            if (self.config["keep-configs"] and \
                os.path.splitext(f)[1] in CONFIGS_EXT and \
                os.path.exists(os.path.join(self.config["setup-folder"], f))) \
                or f == "logs":
                continue
            # files.append(os.path.join(self.config["distr-folder"], f))
            copy_file_or_directory(os.path.join(self.config["distr-folder"], f),
                                   self.config["setup-folder"], replace=True)
        # if running on Windows, update path in service
        if self.config["os-type"] == "Windows":
            new_agentqmc_exe_path = os.path.join(self.config["setup-folder"],
                                                 "AgentQMC.exe")
            new_cmd = self.service.cmd.replace(
                self.service.exe_name,
                new_agentqmc_exe_path
            )
            res = run_cmd(["sc.exe", "config", self.config["service-name"],
                           "binPath=", new_cmd])
            if res.returncode != 0:
                raise SACError("LOGIC_ERROR", "Cannot update path in service")
            # update path in Apache Common Daemon service
            classpath = "{}\\;{}".format(
                self.config["setup-folder"],
                os.path.join(
                    self.config["setup-folder"], "com._1c.etp.agentqmc.jar"
                )
            )
            change_apache_daemon_cmd = [
                new_agentqmc_exe_path,
                "//US//{}".format(self.config["service-name"]),
                "--Classpath={}".format(classpath)
            ]
            res = run_cmd(change_apache_daemon_cmd)
            if res.returncode != 0:
                raise SACError("LOGIC_ERROR",
                               "Cannot update path in Apache Daemon service")
        self._start_service()


if __name__ == "__main__":
    UpdateAgentScenario.main()
