# coding: utf-8

import os
import socket
import subprocess as sp
import yaml


from lib.common import bootstrap
from lib.common.errors import SACError
from lib.base_rac_scenario import BaseRacScenario
from lib.utils import detect_actual_os_type
from lib.utils.cmd import run_cmd, open_file, try_open_file
from lib.common.config import StrPathExpanded, iter_leaves


## Find unused port, which can be used by user.
# @return Port number.
def find_free_port():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(("", 0))
    addr, port = sock.getsockname()
    sock.close()
    return port


## Join multiple dictionaries into one. If key appears in multiple dictionaries,
#  last found value will be used. If called without arguments, return empty
#  dictionary.
# @param *dicts Dictionaries.
# @return Dictionary.
def join_dicts(*dicts):
    r = {}
    for d in dicts:
        for k, v in d.items():
            r[k] = v
    return r


## Class which represents started RAS
class Ras:

    @staticmethod
    def __find_ras_executable(setup_folder=""):
        if detect_actual_os_type() == "Windows":
            from lib.win_utils import find_file_in_1c_installation
            ras_executable = find_file_in_1c_installation(
                "ras.exe", StrPathExpanded(setup_folder)
            )
        else:
            from lib.linux_utils import find_file_in_1c_installation
            ras_executable = find_file_in_1c_installation("ras")
        if ras_executable is None or not os.path.exists(ras_executable):
            raise SACError("LOGIC_ERROR", "cannot find ras executable")
        else:
            return ras_executable

    def __init__(self, host, port, setup_folder=""):
        self.target_host = host
        self.target_port = int(port)
        self.port = find_free_port()
        self.ras_exe = self.__find_ras_executable(setup_folder)
        self._proc = sp.Popen(
            [self.ras_exe, "cluster", "-p", str(self.port),
             "{}:{}".format(self.target_host, self.target_port)],
            stdout=sp.PIPE, stderr=sp.PIPE
        )

    def __del__(self):
        self._proc.terminate()


## Class, which represents RAC connection to RAS.
class Rac:

    ## Find RAC executable.
    # @param setup_folder Path to installed 1C:Enterprise. Ignored on Linux.
    @staticmethod
    def __find_rac_executable(setup_folder=""):
        if detect_actual_os_type() == "Windows":
            from lib.win_utils import find_file_in_1c_installation
            rac_executable = find_file_in_1c_installation(
                "rac.exe", StrPathExpanded(setup_folder)
            )
        else:
            from lib.linux_utils import find_file_in_1c_installation
            rac_executable = find_file_in_1c_installation("rac")
        if rac_executable is None or not os.path.exists(rac_executable):
            raise SACError("LOGIC_ERROR",
                                         "cannot find rac executable")
        else:
            return rac_executable

    ## Parse RAC output.
    # @param s String with rac result.
    # @return List of dictionaries.
    @staticmethod
    def _parse_rac_output(s):
        groups = []
        create_new_group_flag = True
        for l in [l.strip(" \r\n") for l in s.split("\n")]:
            if l == "":
                create_new_group_flag = True
            else:
                if create_new_group_flag:
                    groups.append({})
                    create_new_group_flag = False
                split_line = l.split(":")
                key = split_line[0].strip(" ")
                value = ":".join(split_line[1:]).strip(" \"")
                groups[-1][key] = value
        return groups

    ## Constructor.
    # @param self Pointer to object.
    # @param host RAS host name.
    # @param port RAS port.
    # @param setup_folder Path to installed 1C:Enterprise. Ignored on Linux.
    def __init__(self, host, port, setup_folder=""):
        self.host = host
        self.port = int(port)
        self.rac_exe = self.__find_rac_executable(setup_folder)
        self.execute("cluster", "list", {})

    ## Execute RAC command.
    # @param mode Mode.
    # @param command Command and additional commands, separated by space.
    # @param options Dictionary with parameters.
    # @param key_only_options List of options, which doesn't have a value.
    def execute(self, mode, command, options, key_only_options=()):
        options = ["--{}={}".format(k, v) for k, v in options.items()]
        key_only_options = ["--{}".format(i) for i in key_only_options]
        ras_addr = "{}:{}".format(self.host, self.port)
        commands = [c.strip(" ") for c in command.split(" ")]
        args = [self.rac_exe, mode] + commands + options + key_only_options + \
               [ras_addr, ]
        res = run_cmd(args, shell=False)
        if res.returncode != 0:
            raise SACError(
                "CMD_RESULT_ERROR",
                returncode=res.returncode,
                stdout=bootstrap.try_decode(res.stdout),
                stderr=bootstrap.try_decode(res.stderr),
                args=args
            )
        return self._parse_rac_output(bootstrap.try_decode(res.stdout))


def read_agent_admins(rac, agent_user, agent_pwd):
    options = {}
    if agent_user not in [None, ""]:
        options["agent-user"] = agent_user
        if agent_pwd not in [None, ""]:
            options["agent-pwd"] = agent_pwd
    return rac.execute("agent", "admin list", options)


def read_cluster_parameters(rac, cluster_user, cluster_pwd, cluster_port):
    for cluster in rac.execute("cluster", "list", {}):
        if str(cluster["port"]) == str(cluster_port):
            return cluster
    raise SACError("LOGIC_ERROR",
                                 "Cluster with specified port not found")


def read_cluster_admins(rac, cluster_user, cluster_pwd, cluster_uuid):
    options = {}
    if cluster_user not in [None, ""]:
        options["cluster-user"] = cluster_user
        if cluster_pwd not in [None, ""]:
            options["cluster-pwd"] = cluster_pwd
    options["cluster"] = cluster_uuid
    return rac.execute("cluster", "admin list", options)


def read_servers_parameters(rac, cluster_user, cluster_pwd, cluster_uuid):
    options = {}
    if cluster_user not in [None, ""]:
        options["cluster-user"] = cluster_user
        if cluster_pwd not in [None, ""]:
            options["cluster-pwd"] = cluster_pwd
    options["cluster"] = cluster_uuid
    return rac.execute("server", "list", options)


def read_rules_parameters(rac, cluster_user, cluster_pwd, cluster_uuid,
                          server_uuid):
    options = {}
    if cluster_user not in [None, ""]:
        options["cluster-user"] = cluster_user
        if cluster_pwd not in [None, ""]:
            options["cluster-pwd"] = cluster_pwd
    options["cluster"] = cluster_uuid
    options["server"] = server_uuid
    return rac.execute("rule", "list", options)


def read_cluster_structure(ras_host, ras_port, cluster_port, agent_user,
                           agent_pwd, cluster_user, cluster_pwd,
                           setup_folder=""):
    rac = Rac(ras_host, ras_port, setup_folder)
    structure = {}
    structure["agent-admins"] = read_agent_admins(rac, agent_user, agent_pwd)
    structure["cluster"] = read_cluster_parameters(rac, cluster_user,
                                                   cluster_pwd, cluster_port)
    cluster_uuid = structure["cluster"]["cluster"]
    structure["cluster"]["admins"] = read_cluster_admins(rac, cluster_user,
                                                         cluster_pwd,
                                                         cluster_uuid)
    structure["servers"] = read_servers_parameters(rac, cluster_user,
                                                   cluster_pwd, cluster_uuid)
    structure["rules"] = []
    for server in structure["servers"]:
        rules = read_rules_parameters(rac, cluster_user, cluster_pwd,
                                      cluster_uuid, server["server"])
        counter = 0
        for rule in rules:
            rule["host"] = server["agent-host"]
            rule["position"] = counter
            counter += 1
        structure["rules"] += rules
    return structure


def convert_structure(structure):
    del structure["cluster"]["cluster"]
    for i in structure["servers"]:
        del i["server"]
    for i in structure["rules"]:
        del i["rule"]
    for o, k, v in iter_leaves(structure):
        if v == "":
            o[k] = None
            continue
        try:
            o[k] = int(v)
        except:
            pass
    return structure


class ExportClusterStructureScenario(BaseRacScenario):

    def _validate_specific_data(self):
        self._validate_rac_data()
        validate_data = [
            ["output-file", StrPathExpanded],
            ["cluster-port", int],
            ["cluster-user", str],
            ["cluster-pwd", str],
            ["agent-user", str],
            ["agent-pwd", str],
        ]
        self.config.validate(validate_data)

    def __try_create_output_file(self):
        try_open_file(self.config["output-file"], "w", False)

    def __try_connect_to_ras(self):
        Rac(self.config["host"], self.config["port"],
            self.config["setup-folder"])

    def _get_additional_tests(self):
        return [
            ["try-create-output-file", self.__try_create_output_file, False],
            ["try-connect-to-ras", self.__try_connect_to_ras, True],
        ]

    def _real(self):
        structure = read_cluster_structure(
            self.config["host"], self.config["port"],
            self.config["cluster-port"], self.config["agent-user"],
            self.config["agent-pwd"], self.config["cluster-user"],
            self.config["cluster-pwd"], self.config["setup-folder"]
        )
        structure["infobases"] = []
        structure["infobase-drop-db-mode"] = "drop-database"
        structure["rule-apply-mode"] = "full"
        structure = convert_structure(structure)
        open_file(self.config["output-file"], "w").write(
            yaml.dump(structure, default_flow_style=False)
        )


if __name__ == "__main__":
    ExportClusterStructureScenario.main()
