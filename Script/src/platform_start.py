#!/usr/bin/python3
# coding: utf-8
import sys


from lib.common import bootstrap
from lib.common.config import *
from lib.platform_ctl import PlatformCtlScenario
from lib.utils import *


if __name__ == "__main__":
    PlatformCtlScenario.main("start")
