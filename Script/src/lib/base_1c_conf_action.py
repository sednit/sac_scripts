# coding: utf-8
import os
import subprocess

from lib.common import bootstrap
from lib.common.base_scenario import BaseActionScenario
from lib.common.config import StrPathExpanded
from lib.common.const import LANGS
from lib.common.errors import SACError
from lib.utils import try_open_file, execute_1c_client
from lib.utils.cmd import run_cmd


class Base1CConfActionScenario(BaseActionScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["host", str, str, lambda x: len(x) > 0],
            ["port", int],
            ["ib", str, str, lambda x: len(x) > 0],
            ["setup-folder", StrPathExpanded],
            ["lang", str, str, LANGS],
            ["username", str],
            ["password", str],
            ["access-code", str],
        ]
        self.config.validate(validate_data)
        if self.action == "update-cfg":
            validate_data = [["cf-path", StrPathExpanded], ]
            self.config.validate(validate_data)

    def _after_init(self):
        if self.config["os-type"] != "Windows":
            res = run_cmd(["find", "/opt/1C", "-iname", "1cv8"])
            try:
                self.config["setup-folder"] = os.path.split(
                    bootstrap.try_decode(res.stdout).split()[0]
                )[0]
            except:
                if res.returncode:
                    raise SACError(
                        "LOGIC_ERROR", "Cannot find 1cv8 executable",
                    )
        # building path to 1cv8 executable
        client_exe = os.path.join(self.config["setup-folder"], "1cv8")
        if self.config["os-type"] == "Windows":
            client_exe += ".exe"
        self.config["client-executable"] = client_exe
        # if OS is Windows, also try to find the file in the bin subfolder.
        if self.config["os-type"] == "Windows":
            try:
                try_open_file(self.config["client-executable"])
            except:
                try:
                    new_client_executable = os.path.join(
                        os.path.split(self.config["client-executable"])[0],
                        "bin",
                        os.path.split(self.config["client-executable"])[1]
                    )
                    try_open_file(new_client_executable)
                    self.config["client-executable"] = new_client_executable
                except:
                    pass
        if self.action == "update-cfg":
            # detect update type
            ext = os.path.splitext(self.config["cf-path"])[1]
            if ext == ".cf":
                update_type = "cf"
            elif ext == ".cfu":
                update_type = "cfu"
            else:
                raise SACError(
                    "ARGS_ERROR", "Unknown configuration file extension",
                    file=self.config["cf-path"]
                )
            self.config["update-type"] = update_type

    def _check_client_executable(self):
        try:
            try_open_file(self.config["client-executable"])
        except SACError as err:
            if err.str_code == "FILE_NOT_EXIST":
                raise SACError(
                    "LOGIC_ERROR", "Cannot find 1cv8 executable",
                    setup_folder=self.config["setup-folder"]
                )
            else:
                raise

    def _check_cf_path(self):
        try_open_file(self.config["cf-path"])

    def _get_available_tests(self):
        lst = [
            ("check-client-executable", self._check_client_executable, False),
            # this could be True on last arg, if we assume that IB
            # could be created during composite scenario
            # ("check-ib-availability", self._check_ib_availability, False),
        ]
        if self.action == "update-cfg":
            lst.append(("check-cf-path", self._check_cf_path, True))
        return lst

    def _real(self):
        # set command line parameters depend on self.action
        if self.action == "update-cfg":
            specific_action = [
                "/LoadCfg" if self.config["update-type"] == "cf" \
                else "/UpdateCfg",
                self.config["cf-path"]
            ]
        elif self.action == "update-db-cfg":
            specific_action = ["/UpdateDBCfg", ]
        elif self.action == "rollback-cfg":
            specific_action = ["/RollbackCfg", ]
        # build list of cmd args
        conn_str = "Srvr={}:{};Ref={};".format(self.config["host"],
                                               self.config["port"],
                                               self.config["ib"])
        cmd = [
            #self.config["client-executable"], "DESIGNER", "/IBConnectionString",
            #conn_str
            self.config["client-executable"], "DESIGNER", "/F", self.config["db-path"]
        ] + specific_action + [
            "/L" + self.config["lang"], "/DisableStartupDialogs",
            "/DisableStartupMessages"
        ]
        # append access-code, if specified
        if "access-code" in self.config and self.config["access-code"] != "":
            cmd += ["/UC", str(self.config["access-code"])]
        # if user and password set in config, add them to string
        if "username" in self.config and "password" in self.config and \
           self.config["username"] != "":
            cmd += ["/N", str(self.config["username"]), "/P",
                    str(self.config["password"])]
        # execute cmd
        res = execute_1c_client(cmd)
        if res.returncode != 0:
            raise SACError("CMD_RESULT_ERROR",
                           returncode=res.returncode,
                           stdout=bootstrap.try_decode(res.stdout),
                           stderr=bootstrap.try_decode(res.stderr),
                           args=res.args)
        cmde = '"C:\\Program Files (x86)\\1cv8\\8.3.13.1644\\bin\\1cv8.exe" ENTERPRISE /F ' + self.config["db-path"] + ' /N "АбрамовГС (директор)" /P "123" /WA- /C ВыполнитьОбновлениеИЗавершитьРаботу'
        proc = subprocess.call(cmd, shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)
        return res.returncode
