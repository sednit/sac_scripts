# coding: utf-8

import os
import re


from ..common import bootstrap
from ..common.errors import SACError
from ..common.logger import global_logger, LogFunc
from ..utils import detect_actual_os_type, PlatformVersion, PlatformModules
from ..utils.cmd import run_cmd
from .apache import replace_ws_ext_in_all_apaches


## Class, which represent packages, related to 1C:Enterprise platform.
class Platform1CPackageFile:

    ## Constructor.
    # @param self Pointer to object.
    # @param path Path to folder with installer.
    def __init__(self, path):
        os_type = detect_actual_os_type()
        # set specific package manager module
        if os_type == "Linux-deb":
            from . import deb
            self.pm_module = deb
        elif os_type == "Linux-rpm":
            from . import rpm
            self.pm_module = rpm
        else:
            raise SACError("ARGS_ERROR", "wrong os type", value=os_type)
        self.os_type = os_type
        self._path, self._file_real_name = os.path.split(path)
        # parse string
        regex = {
            "Linux-deb": "1c-enterprise\\d+-([a-zA-Z\\-]+?)(?:(-nls))?_"
            "(\\d+\\.\\d+.\\d+-\\d+)_(amd64|i386)\\.deb",
            "Linux-rpm": "1c_enterprise\\d+-([a-zA-Z\\-]+?)(?:(-nls))?-"
            "(\\d+\\.\\d+.\\d+-\\d+)\\.(x86_64|i386)\\.rpm"
        }
        match = re.search(regex[os_type], self._file_real_name, re.I)
        if match is None:
            raise SACError(
                "ARGS_ERROR",
                "string doesn't contain valud 1c entrprise package name",
                string=self._file_real_name
            )
        self.content = match.groups()[0]
        self.nls = match.groups()[1] is not None
        self.version = PlatformVersion(match.groups()[2])
        if match.groups()[3] in ["amd64", "x86_64"]:
            self.arch = 64
        elif match.groups()[3] == "i386":
            self.arch = 32
        else:
            raise SACError("ARGS_ERROR", "Unknown architecture",
                                         arch=match.groups()[3])

    ## Get name of file.
    # @param self Pointer to object.
    # @return String.
    @property
    def file_name(self):
        if self.os_type == "Linux-deb":
            return "1c-enterprise{}{}-{}{}_{}_{}.deb".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else "", self.version.str_linux(),
                "i386" if self.arch == 32 else "amd64"
            )
        else:
            return "1C_Enterprise{}{}-{}{}-{}.{}.rpm".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else "", self.version.str_linux(),
                "i386" if self.arch == 32 else "x86_64"
            )

    ## Get name without architecture and file extension.
    # @param self Pointer to object.
    # @return String.
    @property
    def common_name(self):
        if self.os_type == "Linux-deb":
            return "1c-enterprise{}{}-{}{}".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else ""
            )
        else:
            return "1C_Enterprise{}{}-{}{}".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else ""
            )

    ## Get real path to package.
    # @param self Pointer to object.
    # @return String.
    @property
    def real_path(self):
        return self._path

    ## Get REAL name of package.
    # @param self Pointer to object.
    # @return String.
    @property
    def real_name(self):
        return self._file_real_name

    ## Get real full path.
    # @param self Pointer to object.
    # @return String.
    @property
    def real_full_path(self):
        return os.path.join(self._path, self._file_real_name)

    ## Get list of provided modules.
    # @param self Pointer to object.
    # @return List of modules.
    def provide_modules(self):
        if self.arch == 32 and self.content == "common":
            return (PlatformModules.LINUX_COMMON, )
        if self.arch == 64 and self.content == "common":
            return (PlatformModules.LINUX_COMMON64, )
        if self.arch == 32 and self.content == "client":
            return (PlatformModules.THIN, PlatformModules.CLIENT)
        if self.arch == 64 and self.content == "client":
            return (PlatformModules.THIN64, PlatformModules.CLIENT64)
        if self.arch == 32 and self.content == "thin-client":
            return (PlatformModules.THIN, )
        if self.arch == 64 and self.content == "thin-client":
            return (PlatformModules.THIN64, )
        if self.arch == 32 and self.content == "server":
            return (PlatformModules.SERVER, )
        if self.arch == 64 and self.content == "server":
            return (PlatformModules.SERVER64, )
        if self.arch == 32 and self.content == "ws":
            return (PlatformModules.WEB, )
        if self.arch == 64 and self.content == "ws":
            return (PlatformModules.WEB64, )
        return tuple()

    def __str__(self):
        return str(self.file_name)

    def __repr__(self):
        return "<Platform1CPackageFile: {}>".format(self.file_name)

    @staticmethod
    def install_multiple_packages(packages, simulate=False, force=False):
        os_type = detect_actual_os_type()
        # set specific package manager module
        if os_type == "Linux-deb":
            from . import deb
            pm_module = deb
        elif os_type == "Linux-rpm":
            from . import rpm
            pm_module = rpm
        else:
            raise SACError("ARGS_ERROR", "wrong os type",
                                         value=os_type)
        paths = [p.real_full_path for p in packages]
        pm_module.install_multiple_packages(paths, simulate, force)
        if not simulate:
            installed_packages = pm_module.get_installed_platform_packages()
            for pkg in packages:
                found = False
                for inst_pkg in installed_packages:
                    if pkg.content == inst_pkg["name"]:
                        found = True
                        break
                if not found:
                    raise SACError(
                        "INSTALL_ERROR",
                        "Package seems to be not installed",
                        pkg_name=pkg.file_name
                    )

    ## Recursively detect packages in specified folder.
    # @param cls Packages' class.
    # @param path Path, where packages should be searched.
    # @return List of packages.
    @classmethod
    def detect_packages(cls, path):
        packages = []
        for entry in os.listdir(path):
            _path = os.path.join(path, entry)
            try:
                packages.append(cls(_path))
                continue
            except:
                pass
            if os.path.isdir(_path):
                packages += cls.detect_packages(_path)
        return packages

    ## Add to list of modules all necessary dependencies of other platform
    #  modules.
    # @param modules Necessary modules.
    # @param thin_as_usual_client Flag, which indicate should thin client be
    #  installed from same package as thick or not.
    # @return Complete list of modules.
    @staticmethod
    def complete_modules_list(modules, thin_as_usual_client = False):
        modules = list(modules)
        if not (set(modules) == set([PlatformModules.THIN, ]) \
                or set(modules) == set([PlatformModules.THIN64, ])) \
                or thin_as_usual_client:
            if "64" in modules[0].value:
                modules.append(PlatformModules.LINUX_COMMON64)
            else:
                modules.append(PlatformModules.LINUX_COMMON)
        if set([PlatformModules.CLIENT, PlatformModules.WEB]) & set(modules):
            modules.append(PlatformModules.SERVER)
        if set([PlatformModules.CLIENT64, PlatformModules.WEB64]) \
           & set(modules):
            modules.append(PlatformModules.SERVER64)
        if PlatformModules.THIN in set(modules) and thin_as_usual_client:
            modules.append(PlatformModules.SERVER)
        if PlatformModules.THIN64 in set(modules) and thin_as_usual_client:
            modules.append(PlatformModules.SERVER64)
        return set(modules)

    ## Add national language support package to packages list.
    # @param Source packages list.
    # @return
    @staticmethod
    def _add_nls_packages(packages_list, result_packages):
        nls_packages = []
        for pkg in result_packages:
            found = False
            for _pkg in packages_list:
                if _pkg.nls and _pkg.content == pkg.content \
                   and _pkg.version == pkg.version and \
                   _pkg.arch == pkg.arch:
                    nls_packages.append(_pkg)
                    found = True
                    break
            if found:
                continue
            raise SACError(
                "INSTALL_ERROR",
                "cannot find nls package for specified package",
                specified_package=pkg
            )
        return result_packages + nls_packages

    @classmethod
    def get_installation_sequence(cls, path, version, required_modules, langs):
        required_modules = list(required_modules)
        thin_only = False
        # first: filter out packages, which versions doesn't match
        packages = [p for p in cls.detect_packages(path) \
                    if p.version == version]
        # add common module if necessary
        if set(required_modules) == set([PlatformModules.THIN, ]) \
           or set(required_modules) == set([PlatformModules.THIN64, ]):
            thin_only = True
        required_modules = cls.complete_modules_list(required_modules)
        # now filter packages which necessary for installing required modules
        copy_reqm = required_modules.copy()
        result_packages = []
        # if thin_only, try to find package and return, if found
        if thin_only:
            for package in [p for p in packages if p.content == "thin-client" \
                            and not p.nls]:
                if len(copy_reqm) < 1:
                    break
                if set(package.provide_modules()).issubset(copy_reqm):
                    result_packages.append(package)
                    copy_reqm -= set(package.provide_modules())
            if len(copy_reqm) == 0:
                if set(langs.upper().split(",")) - set(["RU", "EN"]):
                    return cls._add_nls_packages(packages, result_packages), \
                        required_modules
                return result_packages, required_modules
            required_modules = cls.complete_modules_list(required_modules, True)
        # now find packages which is not thin-client
        result_packages = []
        copy_reqm = required_modules.copy()
        for package in [p for p in packages if p.content != "thin-client" and \
                        not p.nls]:
            if len(copy_reqm) < 1:
                break
            if set(package.provide_modules()) & (copy_reqm):
                result_packages.append(package)
                copy_reqm -= set(package.provide_modules())
        # if left modules, which cannot be installed, and thin_only is False,
        # raise exception
        if len(copy_reqm) > 0:
            raise SACError(
                "INSTALL_ERROR",
                "Packages in specified path cannot meet requirements for "
                "installing next modules",
                modules=[i.value for i in required_modules], version=version,
                path=path
            )
        if set(langs.upper().split(",")) - set(["RU", "EN"]):
            return cls._add_nls_packages(packages, result_packages), \
                required_modules
        return result_packages, required_modules


class Platform1CInstaller:

    ## Constructor.
    # @param self Pointer to object.
    # @param config common::config::Configuration object.
    def __init__(self, config):
        self.config = config
        # setting pm (pm refer to Package manager) module (deb or rpm)
        if self.config["os-type"] == "Linux-deb":
            from . import deb
            self.pm_module = deb
        elif self.config["os-type"] == "Linux-rpm":
            from . import rpm
            self.pm_module = rpm
        self.packages, self.actual_modules = Platform1CPackageFile \
                          .get_installation_sequence(
                              self.config["distr-folder"],
                              self.config["version"],
                              self.config["platform-modules"],
                              self.config["languages"]
                          )
        global_logger.info(message="Packages to install", value=self.packages)

    ## Testing installation permissions.
    # @param self Pointer to object.
    @staticmethod
    def test_install_permissions():
        _ = LogFunc(message="testing permissions")
        os_type = detect_actual_os_type()
        if os_type == "Linux-deb":
            from . import deb as pm
        elif os_type == "Linux-rpm":
            from . import rpm as pm
        pm.test_permissions()

    ## Test update (ie is packages is correct, can be installed etc).
    # @param self Pointer to object.
    def test_installer(self):
        _ = LogFunc(message="Testing update")
        Platform1CPackageFile.install_multiple_packages(self.packages, True,
                                                        True)

    def install(self):
        _ = LogFunc(message="Install 1C:Enterprise Platform")
        # for package in self.packages:
        #     package.install(False, True)
        Platform1CPackageFile.install_multiple_packages(self.packages, False,
                                                        True)
        if PlatformModules.SERVER in self.actual_modules or \
           PlatformModules.SERVER64 in self.actual_modules:
            fail = False
            try:
                result = run_cmd(["update-rc.d", "srv1cv{}{}".format(
                    self.config["version"].version[0],
                    self.config["version"].version[1]
                ), "disable"])
            except:
                try:
                    result = run_cmd(["chkconfig", "srv1cv{}{}".format(
                        self.config["version"].version[0],
                        self.config["version"].version[1]
                    ), "off"])
                except:
                    fail = True
            if result.returncode != 0 or fail:
                global_logger.warning(
                    message="Disable of srv1cv{}{} failed".format(
                        self.config["version"].version[0],
                        self.config["version"].version[1]
                    )
                )


def test_web():
    try:
        replace_ws_ext_in_all_apaches("", True)
    except SACError as err:
        if err.str_code != "SERVICE_ERROR":
            raise


def setup_web(setup_folder):
    res = run_cmd(["find", setup_folder, "-iname", "wsapch2.so"])
    try:
        setup_path = os.path.split(
            bootstrap.try_decode(res.stdout).split()[0]
        )[0]
    except:
        if res.returncode:
            raise SACError(
                "ARGS_ERROR", "Cannot find platform setup folder",
            )
    # perform update of web extension data at web server:
    if not replace_ws_ext_in_all_apaches(setup_path, False):
        raise SACError(
            "LOGIC_ERROR", "Apache found, but web extension not "
            "mentioned in configuration, so it is not updated"
        )
