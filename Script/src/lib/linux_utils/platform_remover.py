# coding: utf-8

import sys


from ..common.errors import *
from .linux_utils import *
from .service import *
from ..utils import *


class Platform1CRemover():

    def __init__(self, config):
        self.config = config
        # setting pm_module (deb or rpm)
        if self.config["os-type"] == "Linux-deb":
            from . import deb as pm_module
        elif self.config["os-type"] == "Linux-rpm":
            from . import rpm as pm_module
        self.pm_module = pm_module

    def test_old_version(self):
        # get list of installed platform packages
        packages = self.pm_module.get_installed_platform_packages()
        # generate list of installed package versions
        versions = list(set([pkg["version"] for pkg in packages]))
        # if old-version not empty and not in versions, raise an error
        if self.config["version"] != "" and \
           self.config["version"].str_linux() not in versions:
            raise SACError(
                "UNINSTALL_ERROR",
                "real old version doesn't match version in configuration",
                installed_versions=[str(PlatformVersion(version)) \
                                    for version in versions],
                expected=self.config["version"]
            )
        if len(packages) < 1:
            if self.config["version"] != "":
                raise SACError(
                    "INSTALL_ERROR",
                    "Can't uninstall old version, because nothing to uninstall",
                    old_version=self.config["version"]
                )
            else:
                global_logger.info(message="Nothing to uninstall")

    ## Uninstall old version of platform.
    # @param self Pointer to object.
    def uninstall_old(self):
        l = LogFunc(message="Uninstall old version of platform")
        # get list of installed platform packages
        packages = self.pm_module.get_installed_platform_packages()
        # uninstall packages
        self.pm_module.uninstall_multiple_packages(packages, False, True)
