import subprocess as sp
import shlex
import os
from functools import reduce


from ..utils.apache import detect_apache_str_param, get_apache_version, \
    replace_path_in_load_module_string
from ..common.errors import SACError
from ..common.logger import global_logger


## Invoke whereis utility.
# @param name Name to find.
# @return List of paths.
def whereis(name):
    proc = sp.Popen(args=["whereis", name], stdout=sp.PIPE,
                    stderr=sp.DEVNULL)
    out, _ = proc.communicate()
    out = shlex.split(out.decode())
    try:
        return out[1:]
    except:
        return []


## Find all unique apache executables.
# @return List of paths.
def find_all_apache_executables():

    # define function which is check that specified executable is apache
    def test_is_executable_apache(path):
        try:
            proc = sp.Popen(args=[path, "-V"], stdout=sp.PIPE,
                            stderr=sp.DEVNULL)
            out, _ = proc.communicate()
            out = out.decode()
            return "apache" in out.lower() and \
                "Server version".lower() in out.lower()
        except:
            return False

    executables = []
    # find apache executables which have names apache2 or httpd
    apache2_files = whereis("apache2")
    for path in apache2_files:
        if test_is_executable_apache(path):
            executables.append(os.path.realpath(os.path.abspath(
                path
            )))
    apache2_files = whereis("httpd")
    for path in apache2_files:
        if test_is_executable_apache(path):
            executables.append(os.path.realpath(os.path.abspath(
                path
            )))
    # in case of apache not set in path try to find in sbin
    for path in ["/usr/sbin/apache2", "/usr/sbin/httpd"]:
        if test_is_executable_apache(path):
            executables.append(os.path.realpath(os.path.abspath(
                path
            )))
    return list(set(executables))


## Find and replace ws extensions paths in apache configuration.
# @param exe Path to apache executable.
# @param new_path New path to 1C:Enterprise platform installation.
# @param test If True, doesn't actually change files.
# @return True, if something replaced, False otherwise.
def replace_ws_ext_in_apache_config(exe, new_path, test):
    config = os.path.normpath(os.path.join(
        detect_apache_str_param(exe, "HTTPD_ROOT"),
        detect_apache_str_param(exe, "SERVER_CONFIG_FILE")
    ))
    cfg = open(config, "r")
    content = cfg.read().split("\n")
    content_copy = content.copy()
    cfg.close()
    names = {
        "20": "wsapch2.so",
        "22": "wsap22.so",
        "24": "wsap24.so",
    }
    # build full_new_path
    new_path = os.path.join(new_path,
                            names["".join(get_apache_version(exe)[0:2])])
    replaced = False
    # find and replace
    for i in range(0, len(content)):
        if "_1cws_module" in content[i] and \
           content[i].strip(" \n\r\t")[0] != "#":
            content[i] = replace_path_in_load_module_string(content[i],
                                                            new_path)
            replaced = True
    # if nothing replaced, return None
    if not replaced:
        return False
    # if test set, return without actual changing in config
    if test:
        return True
    # try to make backup
    try:
        h, t = os.path.split(config)
        cfg_backup = open(os.path.join(h, "backup_" + t), "w")
        cfg_backup.write("\n".join(content_copy))
        cfg_backup.close()
    except:
        raise SACError("INSTALL_ERROR", "Unable to create backup"
                                     "of Apache config file",
                                     apache_config_path=config)
    # write config back
    cfg = open(config, "w")
    cfg.write("\n".join(content))
    cfg.close()
    global_logger.info(message="Updated info about 1C:Enterprise web extension",
                       apache_config_path=config,
                       apache_version=".".join(get_apache_version(exe)),
                       new_path=new_path)
    return True


## Find all files, which can be apache root configurations.
# @return List of paths.
def detect_apache_roots_and_versions():
    possible_roots = [
        "/etc/apache2",
        "/etc/httpd",
    ]
    possible_confs = [
        "conf/apache2.conf",
        "apache2.conf",
        "conf/httpd.conf",
        "httpd.conf",
    ]
    result = []
    for i in possible_roots:
        for j in possible_confs:
            p = os.path.join(i, j)
            if os.path.exists(p):
                result.append(p)
    return result


## Find and replace ws extensions paths in apache configuration.
# @param config path to apache configuration.
# @param new_path New path to 1C:Enterprise platform installation.
# @param test If True, doesn't actually change files.
# @return True, if something replaced, False otherwise.
def replace_ws_ext_in_apache_config_via_path(config, new_path, test):
    cfg = open(config, "r")
    content = cfg.read().split("\n")
    content_copy = content.copy()
    cfg.close()
    replaced = False
    path = None
    # find and replace
    for i in range(0, len(content)):
        if "wsap24.so" in content[i]:
            lib_name = "wsap24.so"
        elif "wsap22.so" in content[i]:
            lib_name = "wsap22.so"
        elif "wsapch2.so" in content[i]:
            lib_name = "wsapch2.so"
        else:
            continue
        if "_1cws_module" in content[i] and \
           content[i].strip(" \n\r\t")[0] != "#":
            path = os.path.join(new_path, lib_name)
            content[i] = replace_path_in_load_module_string(content[i], path)
            replaced = True
    # if nothing is replaced, return None
    if not replaced:
        return False
    # if test is set, return without actual changing in config
    if test:
        return True
    # try to make backup
    try:
        h, t = os.path.split(config)
        cfg_backup = open(os.path.join(h, "backup_" + t), "w")
        cfg_backup.write("\n".join(content_copy))
        cfg_backup.close()
    except:
        raise SACError("INSTALL_ERROR", "Unable to create backup"
                                     "of Apache config file",
                                     apache_config_path=config,)
    # write config back
    cfg = open(config, "w")
    cfg.write("\n".join(content))
    cfg.close()
    global_logger.info(message="Updated info about 1C:Enterprise web extension",
                       apache_config_path=config,
                       new_path=path)
    return True


## Find all apache installations and replace web-extension data.
# @param new_path New path to web-extension.
# @param test Test mode flag.
def replace_ws_ext_in_all_apaches(new_path, test):
    executables = find_all_apache_executables()
    # OR all results
    exe_result = reduce(
        lambda acc, x: acc or replace_ws_ext_in_apache_config(
            x, new_path, test
        ),
        executables,
        False
    )
    # if at least one replace performed, return True
    if exe_result:
        return True
    # otherwise try to find apache configurations by filename
    else:
        configs = detect_apache_roots_and_versions()
        # if both list of executables and list of configurations is empty,
        # raise an exception which indicate that apache is not found
        if len(configs) < 1 and len(executables) < 1:
            raise SACError("SERVICE_ERROR",
                                         "Apache not found")
        cfg_result = reduce(
            lambda acc, x: acc or replace_ws_ext_in_apache_config_via_path(
                x, new_path, test
            ),
            detect_apache_roots_and_versions(),
            False
        )
        return cfg_result
