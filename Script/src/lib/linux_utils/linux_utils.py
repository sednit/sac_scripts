# coding: utf-8

import os
import re


from ..common import bootstrap
from ..common.errors import SACError
from ..common.logger import global_logger
from ..utils.cmd import run_cmd
from ..utils import PlatformModules, PlatformVersion, detect_actual_os_type


class Platform1CPackageFile:

    def __init__(self, path):
        os_type = detect_actual_os_type()
        # set specific package manager module
        if os_type == "Linux-deb":
            from . import deb
            self.pm_module = deb
        elif os_type == "Linux-rpm":
            from . import rpm
            self.pm_module = rpm
        else:
            raise SACError("ARGS_ERROR", "wrong os_type",
                                         value=os_type)
        self.os_type = os_type
        self._path, self._file_real_name = os.path.split(path)
        # parse string
        regex = {
            "Linux-deb": "1c-enterprise\\d+-([a-zA-Z\\-]+?)(?:(-nls))?_"
            "(\\d+\\.\\d+.\\d+-\\d+)_(amd64|i386)\\.deb",
            "Linux-rpm": "1c_enterprise\\d+-([a-zA-Z\\-]+?)(?:(-nls))?-"
            "(\\d+\\.\\d+.\\d+-\\d+)-(x86_64|i386)\\.rpm"
        }
        match = re.search(regex[os_type], self._file_real_name, re.I)
        if match is None:
            raise SACError(
                "ARGS_ERROR",
                "string doesn't contain valud 1c entrprise package name",
                string=self._file_real_name
            )
        self.content = match.groups()[0]
        self.nls = match.groups()[1] is not None
        self.version = PlatformVersion(match.groups()[2])
        if match.groups()[3] in ["amd64", "x64_64"]:
            self.arch = 64
        elif match.groups()[3] == "i386":
            self.arch = 32
        else:
            raise Exception

    @property
    def file_name(self):
        if self.os_type == "Linux-deb":
            return "1c-enterprise{}{}-{}{}_{}_{}.deb".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else "", self.version.str_linux(),
                "i386" if self.arch == 32 else "amd64"
            )
        else:
            return "1C_Enterprise{}{}-{}{}-{}-{}.rpm".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else "", self.version.str_linux(),
                "i386" if self.arch == 32 else "x86_64"
            )

    @property
    def common_name(self):
        if self.os_type == "Linux-deb":
            return "1c-enterprise{}{}-{}{}".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else ""
            )
        else:
            return "1C_Enterprise{}{}-{}{}".format(
                self.version.version[0], self.version.version[1], self.content,
                "-nls" if self.nls else ""
            )

    @property
    def real_path(self):
        return self._path

    @property
    def real_name(self):
        return self._file_real_name

    @property
    def real_full_path(self):
        return os.path.join(self._path, self._file_real_name)

    def provide_modules(self):
        if self.arch == 32 and self.content == "common":
            return (PlatformModules.LINUX_COMMON, )
        if self.arch == 64 and self.content == "common":
            return (PlatformModules.LINUX_COMMON64, )
        if self.arch == 32 and self.content == "client":
            return (PlatformModules.THIN, PlatformModules.CLIENT)
        if self.arch == 64 and self.content == "client":
            return (PlatformModules.THIN64, PlatformModules.CLIENT64)
        if self.arch == 32 and self.content == "thin-client":
            return (PlatformModules.THIN, )
        if self.arch == 64 and self.content == "thin-client":
            return (PlatformModules.THIN64, )
        if self.arch == 32 and self.content == "server":
            return (PlatformModules.SERVER, )
        if self.arch == 64 and self.content == "server":
            return (PlatformModules.SERVER64, )
        if self.arch == 32 and self.content == "ws":
            return (PlatformModules.WEB, )
        if self.arch == 64 and self.content == "ws":
            return (PlatformModules.WEB64, )

    def __str__(self):
        return str(self.file_name)

    def __repr__(self):
        return "<Platform1CPackageFile: {}>".format(self.file_name)

    def install(self, simulate=False, force=True):
        self.pm_module.install_package(self.real_full_path, simulate, force)
        if not simulate:
            for pkg in self.pm_module.get_installed_platform_packages():
                if pkg["name"] == self.content:
                    return
            raise SACError("INSTALL_ERROR",
                                         "Package seems to be not installed",
                                         pkg_name=self.file_name)

    ## Recursively find 1C:Enterprise platform packages in path.
    # @param cls Class.
    # @param path Path to file or directory.
    @classmethod
    def detect_packages(cls, path):
        packages = []
        if os.path.isfile(path):
            try:
                packages.append(cls(path))
            except:
                pass
        elif os.path.isdir(path):
            for entry in os.listdir(path):
                _path = os.path.join(path, entry)
                # main logic of method is here. If path OK for constructor,
                # append it to return list
                try:
                    packages.append(cls(_path))
                    continue
                except:
                    pass
                if os.path.isdir(_path):
                    packages += cls.detect_packages(_path)
        else:
            raise ValueError("path must be existing file or directory")
        return packages

    ## Add to list of modules necessary dependencies.
    # @param modules List of modules.
    # @param thin_as_usual_client Install thin client alongside with other
    #  modules or as standalone package.
    # @return Complete set of modules.
    @staticmethod
    def complete_modules_list(modules, thin_as_usual_client = False):
        modules = list(modules)
        if not (set(modules) == set([PlatformModules.THIN, ]) \
                or set(modules) == set([PlatformModules.THIN64, ])) \
                or thin_as_usual_client:
            if "64" in modules[0].value:
                modules.append(PlatformModules.LINUX_COMMON64)
            else:
                modules.append(PlatformModules.LINUX_COMMON)
        if set([PlatformModules.CLIENT, PlatformModules.WEB]) & set(modules):
            modules.append(PlatformModules.SERVER)
        if set([PlatformModules.CLIENT64, PlatformModules.WEB64]) & \
           set(modules):
            modules.append(PlatformModules.SERVER64)
        if PlatformModules.THIN in set(modules) and thin_as_usual_client:
            modules.append(PlatformModules.SERVER)
        if PlatformModules.THIN64 in set(modules) and thin_as_usual_client:
            modules.append(PlatformModules.SERVER64)
        return set(modules)

    ## Add national languages support packages to list of packages.
    # @param packages_list List existing packages of packages.
    # @param required_packages List of packages that require nls packages.
    # @return Complete list of packages.
    @staticmethod
    def _add_nls_packages(packages_list, required_packages):
        nls_packages = []
        for pkg in required_packages:
            found = False
            for _pkg in packages_list:
                if _pkg.nls and _pkg.content == pkg.content \
                   and _pkg.version == pkg.version and \
                   _pkg.arch == pkg.arch:
                    nls_packages.append(_pkg)
                    found = True
                    break
            if found:
                continue
            raise SACError(
                "INSTALL_ERROR",
                "cannot find nls package for specified package",
                specified_package=pkg
            )
        return required_packages + nls_packages

    ## Get lists of packages and modules, required for specific modules, version
    #  and languages.
    # @cls Class.
    # @path Path where to find packages.
    # @param version Required version.
    # @param required_modules List of required modules.
    # @param langs String with required languages, separated by comma.
    @classmethod
    def get_installation_sequence(cls, path, version, required_modules, langs):
        required_modules = list(required_modules)
        thin_only = False
        # first: filter out packages, which versions doesn't match
        packages = [p for p in cls.detect_packages(path) \
                    if p.version == version]
        # add common module if necessary
        if set(required_modules) == set([PlatformModules.THIN, ]) \
           or set(required_modules) == set([PlatformModules.THIN64, ]):
            thin_only = True
        required_modules = cls.complete_modules_list(required_modules)
        # now filter packages which necessary for installing required modules
        copy_reqm = required_modules.copy()
        result_packages = []
        # if thin_only, try to find package and return, if found
        if thin_only:
            for package in [p for p in packages if p.content == "thin-client" \
                            and not p.nls]:
                if len(copy_reqm) < 1:
                    break
                if set(package.provide_modules()).issubset(copy_reqm):
                    result_packages.append(package)
                    copy_reqm -= set(package.provide_modules())
            if len(copy_reqm) == 0:
                if set(langs.upper().split(",")) - set(["RU", "EN"]):
                    return cls._add_nls_packages(packages, result_packages), \
                        required_modules
                return result_packages, required_modules
            required_modules = cls.complete_modules_list(required_modules, True)
        # now find packages which is not thin-client
        result_packages = []
        copy_reqm = required_modules.copy()
        for package in [p for p in packages if p.content != "thin-client" and \
                        not p.nls]:
            if len(copy_reqm) < 1:
                break
            if set(package.provide_modules()) & (copy_reqm):
                result_packages.append(package)
                copy_reqm -= set(package.provide_modules())
        # if left modules, which cannot be installed, and thin_only is False,
        # raise exception
        if len(copy_reqm) > 0:
            raise Exception
        if set(langs.upper().split(",")) - set(["RU", "EN"]):
            return cls._add_nls_packages(packages, result_packages), \
                required_modules
        return result_packages, required_modules

    ## Copy package to destination folder.
    # @param dst Destination folder.
    def copy_distr(self, dst):
        src = os.path.join(self.real_full_path)
        dst = os.path.join(dst, str(self.version), self.os_type, str(self.arch))
        dst_filename = os.path.join(dst, self.file_name)
        # calculate hash in remote and local distr and if match, omit copy
        if not os.path.exists(dst_filename) \
           or compute_recursive_hash([dst_filename, ]) \
           != compute_recursive_hash([src, ]):
            if os.path.exists(dst_filename):
                os.remove(dst_filename)
            if not os.path.exists(dst):
                os.makedirs(dst)
            copy_file_or_directory(src, dst_filename, False)
        else:
            global_logger.info("Package found, omitting copy")
        # update path
        self.path = dst


## Get list of process' id by name.
# @param name Name of executable..
# @param filter Additional filter, which used as argument to grep.
# @return List of tuples (pid, proc_image, full_cmd).
def get_processes_id_by_name(name, filter=None):
    # find processes with specified name and filter, if necesary
    cmd = "ps -eo pid,command --no-header | grep -i \"{}\"".format(
        os.path.basename(name)
    ) + (
        " | grep -i {}".format(filter)
        if filter is not None else ""
    ) + (
        " | grep -i {}".format(os.path.dirname(name))
        if os.path.dirname(name) != "" else ""
    ) + " | grep -v grep"
    res = run_cmd(cmd, shell=True)
    # gather results to list
    processes = [tuple(i.split())
                 for i in bootstrap.try_decode(res.stdout).split("\n")][:-1]
    result = []
    for proc in processes:
        result.append(tuple([int(proc[0]), proc[1], " ".join(proc[1:])]))
        global_logger.info("found process", name=proc[1], proc_pid=proc[0])
    return result


class ProcessStamp:

    def __init__(self, pid):
        self.pid = pid
        self.start_time = bootstrap.try_decode(run_cmd(
            "date -d\"$(ps -p {} -o lstart= | head -n 1)\" --rfc-3339=ns" \
            .format(pid), shell=True
        ).stdout).split("\n")[0]


def get_proc_children(root_proc, ignore_procs=[]):
    try:
        if not proc_alive(root_proc.pid) or \
           ProcessStamp(root_proc.pid).start_time != root_proc.start_time:
            return []
    except:
        return []
    child_procs = [ProcessStamp(pid) for pid in \
                   bootstrap.try_decode(
                       run_cmd(["pgrep", "-P", str(root_proc.pid)]).stdout
                   ).split("\n")[:-1]]
    child_procs = [i for i in child_procs if \
                   i.start_time >= root_proc.start_time and \
                   not any(i.start_time == j.start_time and i.pid == j.pid \
                           for j in ignore_procs)]
    procs = [root_proc, ]
    for proc in child_procs:
        procs += get_proc_children(proc)
    return procs


def get_procs_children(root_procs):
    return sum([get_proc_children(proc, root_procs) for proc in root_procs], [])


def get_proc_children_by_pid(pid):
    return get_procs_children([ProcessStamp(pid), ])


## Helper class, which allow to control states of processes in tree.
class ProcessTree:

    ## Constructor.
    # @param self Pointer to object.
    # @param pid Process ID of root process.
    def __init__(self, pid):
        self.procs = get_proc_children_by_pid(pid)
        self.root_pid = pid

    ## Update statuses.
    # @param self Pointer to object.
    # @return Number of processes in tree.
    def update(self):
        self.procs = get_procs_children(self.procs)
        return len(self.procs)

    ## Kill processes in tree.
    # @param self Pointer to object.
    # @param force Forced termination of processes.
    # @return True, if not alive process left, False otherwise.
    def kill(self, force=False):
        for proc in self.procs[::-1]:
            try:
                kill_process_tree(proc.pid)
            except:
                pass
        self.update()
        return len(self.procs) == 0


## Kill process and its children.
# @param Process PID.
# @return True if successful, False otherwise
def kill_process_tree(pid, force=True):
    pids = [pid, ] + [i.pid for i in get_proc_children_by_pid(pid)]
    return run_cmd("kill {} {}".format(
        "-9" if force else "-2",
        " ".join([str(pid) for pid in pids])
    ), shell=True).returncode == 0


## Make dump of process. Dump stored in core.<pid> file.
# @param pid Process id (PID).
# @param folder Folder, where dump should be stored. Default - ./ .
# @return True on success, False otherwise.
def make_dump(pid, folder="."):
    res = run_cmd("gcore {}".format(pid),
                  shell=True)
    if res.returncode != 0:
        return False
    # move core file to folder/core.PID
    res = run_cmd(
        "mv core.{0} {1}/{0}.dump".format(
            pid, folder
        ), shell=True
    )
    return res.returncode == 0


## Check, is process alive.
# @param pid Process id (PID).
# @return True, if alive, False otherwise.
def proc_alive(pid):
    return run_cmd("ps -e -o pid | grep {}".format(pid), shell=True) \
        .returncode == 0


def find_file_in_1c_installation(filename, setup_folder="/opt/1C"):
    res = run_cmd(["find", setup_folder, "-iname", filename])
    try:
        return bootstrap.try_decode(res.stdout).split()[0]
    except:
        return None
