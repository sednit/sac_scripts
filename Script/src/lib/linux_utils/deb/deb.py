# coding: utf-8

import re
import subprocess as sp


from ...common import bootstrap
from ...common.errors import SACError
from ...common.logger import global_logger, LogFunc
from ...utils import PlatformVersion, try_open_file
from ...utils.cmd import run_cmd


## Class, which describe name and version of deb-package.
class PackageName:
    ## Constructor.
    # @param self Pointer to object
    # @param name Middle part of name.
    # @param version Package version. Can be string or
    #  lib::utils::main::PlatformVersion object.
    # @param arch Package arch (64 or 32).
    def __init__(self, name, version, arch):
        self.name = name
        self.version = version if isinstance(version, PlatformVersion) else \
            PlatformVersion(version)
        self.prefix = "1c-enterprise{}{}-".format(*self.version.version[0:2])
        self.arch = "amd64" if arch == 64 else "i386"

    ## String representation of object.
    # @param self Pointer to object.
    def __str__(self):
        return "{}{}_{}_{}.deb".format(self.prefix, self.name,
                                       self.version.str_linux(),
                                       self.arch)

    ## Debug representation of object.
    # @param self Pointer to object.
    def __repr__(self):
        return "PackageName: " + str(self)


## Install multiple packages.
# @param paths List of full paths to packages.
# @param simulate Simulate action or not.
# @param force Enable forcing or not..
# @exception SACError(*)
def install_multiple_packages(paths, simulate=False, force=False):
    args = ["dpkg", "-i"]
    if force:
        args += ["--force-all", ]
    if simulate:
        args += ["--simulate", ]
    args += paths
    # run install command
    try:
        res = run_cmd(args)
        global_logger.debug("package installation",
                            returncode=res.returncode,
                            stdout=bootstrap.try_decode(res.stdout),
                            stderr=bootstrap.try_decode(res.stderr)
        )
    except sp.TimeoutExpired as err:
        raise SACError("TIMEOUT_ERROR")
    else:
        if res.returncode != 0:
            raise SACError("DPKG_ERROR",
                                         reason=bootstrap.try_decode(res.stderr))


## Testing permissions dpkg
def test_dpkg_perm():
    res = run_cmd(["dpkg", "-i", "test.deb", "--simulate"])
    if res.returncode == 2:
        if re.search(r"superuser", bootstrap.try_decode(res.stderr)):
            return False
        else:
            return True
    if res.returncode == 0:
        return True


## Testing permissions.
# @param update_data Ignore.
# @exception SACError(*)
def test_permissions():
    l = LogFunc(message="permission test")
    # checking permissions of dpkg
    if test_dpkg_perm() is False:
        raise SACError("DPKG_PERM_DENIED")
    # testing access to /etc/systemd/system
    try_open_file("/etc/systemd/system/srv1cv8", "a", False)
    # testing access to /tmp folder
    try_open_file("/tmp/test.deb", "a", False)


## Find installed version of package.
# @param name Name (or its part) of package/
# @return String with version or None.
def find_package_installed(name):
    # getting package status
    res = run_cmd(["dpkg", "--status", name])
    if res.returncode != 0:
        return None
    # extracting full package name from this data
    search_package_result = re.search("^Package.*?([^: ]*)$",
                                      bootstrap.try_decode(res.stdout),
                                      re.MULTILINE)
    if name != search_package_result.groups()[0]:
        return None
    # sure that package installed
    search_installed_result = re.search("installed",
                                        bootstrap.try_decode(res.stdout),
                                        re.MULTILINE)
    # extracting package version
    if search_package_result is not None \
            and search_installed_result is not None:
        search_version_result = re.search("^Version.*?([^: ]*)$",
                                          bootstrap.try_decode(res.stdout),
                                          re.MULTILINE)
        return search_version_result.groups()[0]
    else:
        return None


## Getting installed platform version.
# @param config dict or dict-like object with "version" and "arch" values.
# @return lib::utils::main::PlatformVersion object.
def get_installed_platform_version(config):
    package_name = PackageName(
        None, "common", config["version"],
        config["arch"]
    )
    package_name = package_name.prefix + package_name.name
    return PlatformVersion(find_package_installed(package_name))


## Return list of installed 1C:Enterprise Platform packages.
# @return List of dicts.
def get_installed_platform_packages():
    # query all installed 1c-enterprise* packages
    res = run_cmd(
        "dpkg-query -W -f='${binary:Package}\\t${Architecture}\\t${Version}\\t\\n' "
        "'1c-enterprise*' | grep -e 'enterprise[0-9]\\{1,\\}'", shell=True,
    )
    # if query returned nothing, return empty list
    if res.returncode != 0:
        return list()
    packages = list()
    # parse each line and fill dict
    for package_str in bootstrap.try_decode(res.stdout).split("\n")[:-1]:
        parts = package_str.split("\t")
        package = {"name": "-".join(parts[0].split("-")[2:]),
                   "fullname": parts[0], "version": parts[2],
                   "arch": 64 if parts[1] == "amd64" else 32}
        packages.append(package)
    return packages


## Uninstall packages.
# @param packages List of dicts, returned by get_installed_platform_packages().
# @param simulate Simulate action or not.
# @param force Enable forcing or not.
# @exception SACError("TIMEOUT_ERROR") If time expired.
# @exception SACError("DPKG_ERROR") If error occurred during
#  uninstall.
def uninstall_multiple_packages(packages_names, simulate=False, force=True):
    args = ["dpkg", "-r"]
    if force:
        args += ["--force-all", ]
    if simulate:
        args += ["--simulate", ]
    args += [pkg["fullname"] for pkg in packages_names]
    try:
        res = run_cmd(args, shell=False)
    except sp.TimeoutExpired as err:
        raise SACError("TIMEOUT_ERROR")
    else:
        if res.returncode != 0:
            raise SACError(
                "DPKG_ERROR", reason=bootstrap.try_decode(res.stderr)
            )
