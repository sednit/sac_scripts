import os


from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from lib.utils.cmd import run_cmd
from lib.common.config import StrPathExpanded


## Base class for all scenarios, which is built around RAC.
class BaseRacScenario(BaseScenario):

    def _validate_rac_data(self):
        # validate host and port of RAS and setup_folder of 1C:Enterprise
        # platform
        validate_data = [
            ["host", str],
            ["port", int],
            ["setup-folder", StrPathExpanded],
        ]
        self.config.validate(validate_data)

    ## Find RAC executable in 1C:Enterprise installation location.
    # @param self Pointer to object.
    def __find_rac_executable(self):
        if self.config["os-type"] == "Windows":
            rac_executable = self.__os_utils_module \
                                 .find_file_in_1c_installation(
                                     "rac.exe",
                                     self.config["setup-folder"]
                                 )
        else:
            rac_executable = self.__os_utils_module \
                                 .find_file_in_1c_installation(
                                     "rac",
                                     self.config["setup-folder"],
                                 )
        if rac_executable is None or not os.path.exists(rac_executable):
            raise SACError("LOGIC_ERROR",
                                         "cannot find rac executable")
        else:
            return rac_executable

    ## Execute RAC command.
    # @param self Pointer to object.
    # @param mode Mode.
    # @param command Command and additional commands, separated by space.
    # @param options dictionary of parameters.
    # @return RAC stdout.
    def _execute_rac(self, mode, command, options):
        rac_executable = self.__find_rac_executable()
        options = ["--{}={}".format(k, v) for k, v in options.items()
                   if v is not None]
        ras_addr = "{}:{}".format(self.config["host"], self.config["port"])
        commands = [c.strip(" ") for c in command.split(" ")]
        args = [rac_executable, mode] + commands + options + [ras_addr, ]
        res = run_cmd(args, shell=False)
        if res.returncode != 0:
            raise SACError(
                "CMD_RESULT_ERROR",
                returncode=res.returncode,
                stdout=bootstrap.try_decode(res.stdout),
                stderr=bootstrap.try_decode(res.stderr),
                args=args
            )
        return res

    ## Parse RAC output.
    # @param s Input text.
    # @result List of dictionaries.
    @staticmethod
    def _parse_rac_output(s):
        groups = []
        create_new_group_flag = True
        for l in [l.strip(" \r\n") for l in s.split("\n")]:
            if l == "":
                create_new_group_flag = True
            else:
                if create_new_group_flag:
                    groups.append({})
                    create_new_group_flag = False
                split_line = l.split(":")
                key = split_line[0].strip(" ")
                value = ":".join(split_line[1:]).strip(" \"")
                groups[-1][key] = value
        return groups

    @staticmethod
    def _get_object_by_field(obj, key, value, convert_to=str):
        for i in obj:
            if convert_to(i[key]) == convert_to(value):
                return i
        return None

    ## Get cluster port by its UUID.
    # @param self Pointer to object.
    def _get_cluster_id_by_port(self):
        res = self._execute_rac("cluster", "list", {})
        parsed_output = self._parse_rac_output(bootstrap.try_decode(res.stdout))
        cluster = self._get_object_by_field(
            parsed_output, "port", self.config["cluster-port"], int
        )
        if cluster is None:
            raise SACError(
                "ARGS_ERROR", "Cluster not found",
                cluster_port=self.config["cluster-port"]
            )
        return cluster["cluster"]

    ## Add user credential and password to provided object.
    # @param obj Object, where data should be stored.
    # @para user_type User prefix.
    def _add_user_credentials(self, obj, user_type="cluster"):
        user_key = user_type + "-user"
        pwd_key = user_type + "-pwd"
        if user_key in self.config and \
           self.config[user_key] is not None:
            obj[user_key] = self.config[user_key]
            if pwd_key in self.config and \
               self.config[pwd_key] is not None:
                obj[pwd_key] = self.config[pwd_key]

    def _after_init(self):
        if self.config["os-type"] == "Windows":
            from lib import win_utils as m
        else:
            from lib import linux_utils as m
        self.__os_utils_module = m

    def _get_additional_tests(self):
        return []

    def _get_available_tests(self):
        return [
            ["check-rac-executable", self.__find_rac_executable, True],
        ] + self._get_additional_tests()
