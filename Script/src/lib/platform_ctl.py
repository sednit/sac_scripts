# coding: utf-8

import time


from .common.config import *
from .utils import *
from .common.base_scenario import *


SERVICE_CONTROL_DELAY = 10


class PlatformCtlScenario(BaseActionScenario):

    def _after_init(self):
        self.services = list()
        self.ServiceCls = None
        self.WebServiceCls = None
        # fill list of services names
        self.config["services"] = list()
        if self.config["server-role"] != "web":
            self.config["services"].append(self.config["service-1c"]["name"])
            self.config["services"].append(self.config["ras"]["name"])
        if self.config["server-role"] != "app":
            if self.config["os-type"] == "Windows":
                if self.config["web-server"].upper() == "IIS":
                    self.config["services"].append("IIS")
                else:
                    from .win_utils.service import get_apache_service_name
                    self.config["services"].append(get_apache_service_name())
            else:
                from .linux_utils.service import get_apache_service_name
                self.config["services"].append(get_apache_service_name())
        # select proper Service classes
        if self.config["os-type"] == "Windows":
            from .win_utils.service import Service, IISService
            self.ServiceCls = Service
            self.WebServiceCls = IISService
        else:
            from .linux_utils.service import SystemdService
            self.ServiceCls = SystemdService
            self.WebServiceCls = SystemdService
        # create ServiceCls objects
        self.services = list()
        for service_name in self.config["services"]:
            if self.config["os-type"] == "Windows" and \
               service_name.upper() == "IIS":
                self.services.append(self.WebServiceCls())
            else:
                self.services.append(self.ServiceCls(service_name))

    def _validate_specific_data(self):
        # build validate data
        validate_data = [
            ["server-role", str, str, ["all", "app", "web"]],
        ]
        if self.action != "start":
            validate_data += [
                ["dumps-folder", StrPathExpanded],
            ]
        # now validate first bunch of parameters
        self.config.validate(validate_data)
        # make second bunch of data
        validate_data = []
        if self.config["server-role"] in ["app", "all"]:
            validate_data += [
                ["service-1c/name", str],
                ["ras/name", str],
            ]
        if self.config["server-role"] in ["web", "all"]:
            # make list of allowed web-servers
            allowed_web_servers = ["apache",]
            if self.config["os-type"] == "Windows":
                allowed_web_servers.append("iis")
            # perform validation. Comparison with allowed values
            # performs in case-insensitive way.
            validate_data += [
                ["web-server", str, str, lambda e: e.lower() \
                 in allowed_web_servers],
            ]
        self.config.validate(validate_data)

    def _connect_services(self):
        for service in self.services:
            service.connect()

    def _get_available_tests(self):
        return [
            ("test-sc-permissions", self.ServiceCls.test_sc_permissions, False),
            ("check-services-existence", self._connect_services, True)
        ]

    ## Start platform.
    # @param self Pointer to object.
    def _start(self):
        l = LogFunc(message="starting 1C:Enterprise Platform")
        # 3. Start services
        for service in self.services:
            service.start()
        # 4. Check services started after timeout
        global_logger.info(message="Give services time to start...")
        time.sleep(SERVICE_CONTROL_DELAY)
        for service in self.services:
            if not service.started:
                raise SACError("SERVICE_ERROR",
                                             "service not started",
                                             name=service.name)

    ## Stop platform.
    # @param self Pointer to object.
    def _stop(self):
        l = LogFunc(message="stopping 1C:Enterprise Platform")
        # 3. Stop services gracefully
        for service in self.services:
            service.stop()
        # 4. Check services stopped. If not, kill their processes.
        global_logger.info(message="Give services time to stop...")
        time.sleep(SERVICE_CONTROL_DELAY)
        for service in self.services:
            if service.started:
                service.stop(True)
                time.sleep(SERVICE_CONTROL_DELAY)
                if service.started:
                    raise SACError("SERVICE_ERROR",
                                                 "service not stopped",
                                                 name=service.name)

    def _real(self):
        self._connect_services()
        if self.action == "start":
            self._start()
        elif self.action == "stop":
            self._stop()
        elif self.action == "restart":
            self._stop()
            self._start()
        return 0
