import os
import time
import sys
import subprocess as sp


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(BASE_PATH, ".."))


from lib.utils import detect_actual_os_type
from lib.utils.cmd import run_cmd


if detect_actual_os_type() == "Windows":
    from lib.win_utils import ProcessTree
else:
    from lib.linux_utils import ProcessTree


proc = sp.Popen(sys.argv[1:], shell=False)
pt = ProcessTree(proc.pid)
print(proc.pid)
while True:
    time.sleep(0.05)
    if pt.update() < 1:
        break
pt.kill(True)
sys.exit(proc.poll())
