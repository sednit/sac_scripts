# coding: utf-8

import hashlib
import os
import sys
import platform
import re
import yaml
import time
import subprocess as sp
import datetime as dt
import tarfile
import rarfile
import zipfile
import tempfile
import datetime
from distutils.dir_util import remove_tree
from itertools import islice
from functools import lru_cache
import collections
from enum import Enum


from .fs import copy_file_or_directory, get_inter_paths, copystat, yield_content
from .cmd import run_cmd, get_psexec_session_command, CompletedProcess
from ..common.errors import SACError
from ..common.logger import global_logger, LogFunc


class PlatformModules(Enum):
    THIN = "thin"
    CLIENT = "client"
    SERVER = "server"
    WEB = "web"
    THIN64 = "thin64"
    CLIENT64 = "client64"
    SERVER64 = "server64"
    WEB64 = "web64"
    ALL = "all"
    ALL64 = "all64"
    LINUX_COMMON = "common"      # this 2 values used only
    LINUX_COMMON64 = "common64"  # under Linux

    @classmethod
    def from_string(cls, s, separator=","):
        values = []
        for i in s.split(separator):
            if i == "":
                continue
            # if ALL or ALL64 in string, set all necessary modules and
            # break
            if i.lower() == cls.ALL64.value:
                values = [cls.THIN64, cls.CLIENT64, cls.SERVER64, cls.WEB64]
                if detect_actual_os_type() != "Windows":
                    values.append(cls.LINUX_COMMON64)
                break
            if i.lower() == cls.ALL.value:
                values = [cls.THIN, cls.CLIENT, cls.SERVER, cls.WEB]
                if detect_actual_os_type() != "Windows":
                    values.append(cls.LINUX_COMMON)
                break
            # otherwise compare each value to allowed and add them
            found = False
            for c in list(cls):
                if c.value.lower() == i.lower():
                    values.append(c)
                    found = True
                    break
            # if no modules found, raise an error
            if not found:
                raise SACError("ARGS_ERROR", "Unknown module",
                                             module=i)
        return set(values)


## Class, represents platform version.
class PlatformVersion:

    ## Constructor.
    # @param self Pointer to object.
    # @param version Version. Can be string, None or another PlatformVersion
    #  object.
    def __init__(self, version):
        if version is None:
            self.version = ()
            return
        if isinstance(version, PlatformVersion):
            self.version = version.version
            return
        self.version = tuple(re.findall(r"\d+", version))

    ## String representation of object for Linux.
    # @param self Pointer to object.
    def str_linux(self):
        return "{}.{}.{}-{}".format(*self.version) if len(self.version) == 4 \
            else str(self)

    ## String representation of object.
    # @param self Pointer to object.
    def __str__(self):
        return ".".join(self.version)

    ## Debug representation of object.
    # @param self Pointer to object.
    def __repr__(self):
        return "PlatformVersion: " + str(self.version)

    ## Get part of version by index.
    # @param self Pointer to object.
    def __getitem__(self, index):
        return self.version[index]

    ## Compare object with other version.
    # @param self Pointer to object.
    # @param other Object, which represent version. Should be str or
    #  lib::utils::main::PlatformVersion object.
    # @return True, False or None, if object is not a str or
    #  lib::utils::main::PlatformVersion
    def __eq__(self, other):
        if isinstance(other, PlatformVersion):
            return self.version == other.version
        elif isinstance(other, str):
            return self.version == PlatformVersion(other).version
        elif isinstance(other, type(None)):
            return self.version == PlatformVersion(other).version
        else:
            return None

    def __gt__(self, other):
        if isinstance(other, str):
            other = PlatformVersion(other)
        elif isinstance(other, type(None)):
            return None
        for i in range(0, min(len(self.version), len(other.version))):
            if int(self.version[i]) > int(other.version[i]):
                return True
            elif int(self.version[i]) < int(other.version[i]):
                return False
        if len(self.version) > len(other.version):
            return True
        return False

    def __lt__(self, other):
        if isinstance(other, str):
            other = PlatformVersion(other)
        elif isinstance(other, type(None)):
            return None
        for i in range(0, min(len(self.version), len(other.version))):
            if int(self.version[i]) < int(other.version[i]):
                return True
            elif int(self.version[i]) > int(other.version[i]):
                return False
        if len(self.version) < len(other.version):
            return True
        return False


## Dummy function.
# @param *args Ignored.
# @param **kwargs Ignored.
# @return None.
def dummy(*args, **kwargs):
    return None


## Decorator, which allow to add function a static variables like in C++.
#  Static variables stored in function_name.__static_vars__ object.
# @param Name of the variable.
# @param initial_value Value, which this variable will have initial.
def static_var(name, initial_value=None):
    def wrapper(func):
        if not hasattr(func, "__static_vars__"):
            setattr(func, "__static_vars__", type('__static_vars__', (), {})())
        setattr(func.__static_vars__, name, initial_value)
        return func
    return wrapper


## Check, if current platform is 64 bit.
# @return True or False.
# @exception SACError("NO_ARCHITECTURE") Raised if cannot detect
#  platform architecture.
def is_64bit_arch():
    bit32id = ["32bit", "x86"]
    bit64id = ["64bit", "x86_64", "AMD64"]
    bit_ids = bit32id + bit64id

    # trying to retrieve platform architecture in different ways
    res = platform.machine()
    if res not in bit_ids:
        res = platform.processor()
    else:
        return res in bit64id
    if res not in bit_ids:
        res = platform.architecture()
    else:
        return res in bit64id
    for i in res:
        if i in bit_ids:
            res = i
            return res in bit64id
    raise SACError("NO_ARCHITECTURE")


## Try to open file and convert exceptions to SACError.
# @param path Path to file.
# @param mode Mode.
# @param should_exist Should file exist. If set to False, file will be created,
#  if it doesn't exist, and removed. It can be useful to check, can file,
#  specified by path argument, be created.
# @exception SACError(*)
def try_open_file(path, mode="r", should_exist=True):
    delete_on_exit = False
    try:
        # if file not exist and shouldn't, then create it and delete on exit
        if not os.path.exists(path) and not should_exist:
            delete_on_exit = True
            f = open(path, "w")
        else:
            f = open(path, mode)
    # handle exceptions
    except FileNotFoundError:
        raise SACError("FILE_NOT_EXIST", path=path)
    except PermissionError:
        raise SACError("FILE_PERM_DENIED", path=path)
    except Exception as e:
        raise SACError("UNKNOWN", str(e))
    else:
        f.close()
    # if set delete_on_exit, then delete file
    if delete_on_exit:
        try:
            os.remove(path)
        except Exception:
            pass


## Open file and convert exceptions to SACError.
# @param path Path to file.
# @param mode Mode.
# @return file object.
# @exception SACError(*)
def open_file(path, mode="r", encoding="utf-8"):
    try:
        f = open(path, mode=mode, encoding=encoding)
    except FileNotFoundError:
        raise SACError("FILE_NOT_EXIST", path=path)
    except PermissionError:
        raise SACError("FILE_PERM_DENIED", path=path)
    except Exception as e:
        raise SACError("UNKNOWN", e)
    else:
        return f


## Open and read YAML file.
# @param path Path to YAML file.
# @return Content of YAML file.
# @exception SACError("YAML_PROBLEM_MARK") Incorrect syntax.
# @exception SACError("YAML_COMMON_ERROR") Unknown error while
#  processing YAML file.
def read_yaml(path):
    try:
        f = open_file(path, encoding="utf-8")
        data = yaml.load(f)
    except yaml.YAMLError as err:
        if hasattr(err, "problem_mark"):
            mark = err.problem_mark
            raise SACError("YAML_PROBLEM_MARK",
                                         mark.line + 1, mark.column + 1,
                                         str(err), path)
        else:
            raise SACError("YAML_COMMON_ERROR",
                                         str(err), path)
    f.close()
    return data


## Detect OS type.
# @return one of values: "Windows", "Linux-deb", "Linux-rpm".
# @exception SACError("UNKNOWN", "OS not supported")
@lru_cache(maxsize=1)
def detect_actual_os_type():
    os_str = platform.system()
    if os_str not in ["Windows", "Linux"]:
        raise SACError("UNKNOWN", "OS not supported")
    if os_str == "Linux":
        linux_type = "deb" if run_cmd("dpkg --version", shell=True) \
            .returncode == 0 else "rpm"
        return os_str + "-" + linux_type
    else:
        return os_str


# .exe for SFX RAR doesn't included cuz it also could be just executable
# file. If you sure that .exe is SFX, then add it yourself
KNOWN_ARCHIVE_EXTENSIONS = [".rar", ".zip", ".tar.gz", ".tar.xz", ".tar.bz2",
                            ".tar"]


## Trying to split filename to name and archive extension. If known
#  archive extension (like .tar.gz) not found, acts like an os.path.splitext().
#  If file name contain only archive extensions, like ".tar.gz", then all
#  (2 in this case) extensions considers as one whole, and will be returned as
#  (".tar.gz", "") for this case.
# @param path Path to file.
# @param additional_exts List of extensions, which you would like to also
#  process in special way.
# @param replace_exts If True, replace known extensions with additional_exts
#  instead of adding them to already known.
# @return String with extension.
def splitext_archive(path, additional_exts=[], replace_exts=False):
    # get file name with extension
    folder, filename = os.path.split(path)
    # list of known extensions
    if replace_exts:
        known_extensions = additional_exts
    else:
        known_extensions = KNOWN_ARCHIVE_EXTENSIONS \
                           + list(additional_exts)
    for ext in known_extensions:
        if ext == filename[len(filename) - len(ext):]:
            if ext == filename:
                return ext, ""
            return os.path.join(folder, filename.replace(ext, "")), ext
    return os.path.splitext(path)


## Unpack archive. Type of archive is determined by its extension.
# @param path Path to archive file.
# @param dst Destination folder.
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @return List of extracted files.
def unpack_archive(path, dst, with_root=False, pattern="",
                   invert=False, replace=True):
    l = LogFunc(message="Unpacking archive", src=path, dst=dst)
    # if with_root set, append to dst path name of archive without extension
    if with_root:
        archive_base_name = splitext_archive(os.path.basename(path))[0]
        dst = os.path.join(dst, archive_base_name)
    # create destination folder, if it doesn't exists
    if not os.path.exists(dst):
        os.makedirs(dst)
    _, archive_ext = splitext_archive(path)
    unpack_functions = {
        ".exe": unpack_sfx_rar,
        ".rar": unpack_rar,
        ".zip": unpack_zip,
        ".tar": unpack_tar,
        ".tar.gz": unpack_tar,
        ".tar.xz": unpack_tar,
        ".tar.bz2": unpack_tar,
    }
    try:
        result = unpack_functions[archive_ext](path, dst, pattern, invert,
                                               replace)
    except KeyError:
        raise ValueError("Unsupported file extension.")
    return result


## Unpack RAR archive.
# @param path Path to archive file.
# @param dst Destination folder.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @return List of extracted files.
def unpack_rar(path, dst, pattern, invert, replace):
    rarfile.UNRAR_TOOL = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "..", "..", "..",
        "external_utils", "unrar",
        "unrar.exe" if platform.system() == "Windows" else "unrar"
    )
    archive = rarfile.RarFile(path)
    files = []
    # select files from archive
    for file_ in archive.infolist():
        A = pattern != ""
        B = re.search(pattern, file_.filename)
        if A and ((B and invert) or not (B or invert)):
            continue
        files.append(file_)
    # extract files
    for file_ in files:
        dst_ = os.path.join(dst, file_.filename)
        if not replace and os.path.exists(dst_):
            raise SACError(
                "LOGIC_ERROR",
                "Destination path exists and replace is not allowed",
                destination_path=dst_
            )
        if not os.path.exists(os.path.dirname(dst_)):
            os.makedirs(os.path.dirname(dst_))
        archive.extract(file_, path=dst)
    return list([os.path.join(dst, f.filename) for f in files])


## Unpack SFX RAR archive.
# @param path Path to archive file.
# @param dst Destination folder.
# @return List of extracted files.
def unpack_sfx_rar(path, dst, *args):
    rarfile.UNRAR_TOOL = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "..", "..", "..",
        "external_utils", "unrar",
        "unrar.exe" if platform.system() == "Windows" else "unrar"
    )
    # first check, that path is actually SFX RAR archive
    if run_cmd([rarfile.UNRAR_TOOL, "t", path]).returncode != 0:
            raise SACError("UNPACK_ERROR", "bad SFX RAR archive",
                                         path=path)
    # perform extraction to temp folder
    tmp_folder = tempfile.mkdtemp()
    global_logger.debug(message="Created temporary folder", path=tmp_folder)
    res = run_cmd([rarfile.UNRAR_TOOL, "x", path, os.path.join(tmp_folder, "")])
    # if not successful, raise an error
    if res.returncode != 0:
        raise SACError("UNPACK_ERROR",
                                     "error while extracting SFX archive",
                                     path=path)
    # copy files from temporary location to dst
    copy_file_or_directory(tmp_folder, dst, False)
    # if successful, store all file names and relative paths
    files = []
    for root, dirnames, filenames in os.walk(tmp_folder):
        for filename in filenames:
            # replace temp folder part with nothing, for create relative paths
            rel_file_path = os.path.join(root, filename) \
                                   .replace(os.path.join(tmp_folder, ""), "")
            files.append(os.path.join(dst, rel_file_path))
    # remove tmp_folder
    remove_tree(tmp_folder)
    return files


## Unpack ZIP archive.
# @param path Path to archive file.
# @param dst Destination folder.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @param try_encode Try to decode names in specified encoding.
# @return List of extracted files.
def unpack_zip(path, dst, pattern, invert, replace, try_encode=None):
    archive = zipfile.ZipFile(path, "r")
    files = []
    # select files
    for file_ in archive.infolist():
        # fix
        if try_encode is not None:
            try:
                file_path = file_.filename.encode("cp437").decode(try_encode)
            except:
                file_path = file_.filename
        else:
            file_path = file_.filename
        A = pattern != ""
        B = re.search(pattern, file_path)
        if A and ((B and invert) or not (B or invert)):
            continue
        files.append(file_)
    # extract files
    for file_ in sorted(files, key=lambda x: len(get_inter_paths(x.filename))):
        dst_ = os.path.join(dst, file_.filename)
        if not replace and os.path.exists(dst_):
            raise SACError(
                "LOGIC_ERROR",
                "Destination path exists and replace is not allowed",
                destination_path=dst_
            )
        # make necessary folders for file
        folders_created = False
        if not os.path.exists(os.path.dirname(dst_)):
            os.makedirs(os.path.dirname(dst_))
            folders_created = True
        # extract file
        archive.extract(file_, path=dst)
        # set permissions
        copystat(dst, dst_)  # set permissions to file itself
        # if any folder in dst_ created, set permissions for folders too
        if folders_created:
            for subpath_ in get_inter_paths(os.path.dirname(file_.filename)):
                copystat(dst, os.path.join(dst, subpath_))
    return list([os.path.join(dst, f.filename) for f in files])


## Unpack TAR, GZTAR, BZTAR and XZTAR archive.
# @param path Path to archive file.
# @param dst Destination folder.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @return List of extracted files.
def unpack_tar(path, dst, pattern, invert, replace):
    archive = tarfile.open(path, "r:*")
    files = []
    # select files
    for file_ in archive.getmembers():
        A = pattern != ""
        B = re.search(pattern, file_.name)
        if A and ((B and invert) or not (B or invert)):
            continue
        files.append(file_)
    # extract files. Sort files list in order of increasing of number of \
    # intermediate folders
    for file_ in sorted(files, key=lambda x: len(get_inter_paths(x.name))):
        dst_ = os.path.join(dst, file_.name)
        if not replace and os.path.exists(dst_):
            raise SACError(
                "LOGIC_ERROR",
                "Destination path exists and replace is not allowed",
                destination_path=dst_
            )
        # make necessary folders for file
        folders_created = False
        if not os.path.exists(os.path.dirname(dst_)):
            os.makedirs(os.path.dirname(dst_))
            folders_created = True
        # extract file
        archive.extract(file_, path=dst, set_attrs=True)
        # if any folder in dst_ created, set permissions for folders
        if folders_created:
            for subpath_ in get_inter_paths(os.path.dirname(file_.name)):
                copystat(dst, os.path.join(dst, subpath_))
    return list([os.path.join(dst, f.name) for f in files])


## Pack source location to archive.
# @param src Source location.
# @param store_path Folder name, where archive should be stored.
# @param ext Archive extension (zip, tar, tar.gz, tar.bz2, tar.xz).
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @return Path to result archive.
def pack_archive(src, store_path, ext, with_root=False, pattern="",
                 inverse=False, replace=True):
    unpack_functions = {
        "zip": pack_zip,
        "tar": pack_tar,
        "tar.gz": lambda x, y, z, a, b, c: pack_tar(x, y, z, a, b, c, "gz"),
        "tar.xz": lambda x, y, z, a, b, c: pack_tar(x, y, z, a, b, c, "xz"),
        "tar.bz2": lambda x, y, z, a, b, c: pack_tar(x, y, z, a, b, c, "bz2"),
    }
    try:
        return unpack_functions[ext](src, store_path, with_root, pattern,
                                     inverse, replace)
    except KeyError as err:
        raise ValueError("Unsupported file extension.")


## Pack source location to TAR archive.
# @param src Source location.
# @param store_path Folder name, where archive should be stored.
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @param additional_ext Additional extension (gz, xz, bz2).
# @return Path to result archive.
def pack_tar(src, store_path, with_root=False, pattern="",
             invert=False, replace=True, additional_ext=""):
    # make archive name
    archive_name = os.path.basename(src) + ".tar"
    if additional_ext in ["gz", "xz", "bz2"]:
        archive_name += "." + additional_ext
    archive_path = os.path.join(store_path, archive_name)
    if os.path.exists(archive_path) and not replace:
        raise SACError(
            "LOGIC_ERROR", "Destination path exists and replace is not allowed",
            destination_path = archive_path
        )
    # select files
    files = [(os.path.join(src, *file_), os.path.join(*file_)) for file_ in
             yield_content(src, pattern, invert)]
    # remove old archive, if exists
    if os.path.exists(archive_path):
        os.remove(archive_path)
    # create archive
    if additional_ext in ["gz", "xz", "bz2"]:
        archive = tarfile.open(archive_path, "x:" + additional_ext)
    else:
        archive = tarfile.open(archive_path, "x:")
    # archived folders
    archived_folders = set()
    # pack files into archive
    for f in files:
        # add to archive each folder
        for dir_ in get_inter_paths(os.path.dirname(f[1]))[::-1]:
            # if folder not already added to archive, add it
            if dir_ not in archived_folders:
                archive.add(os.path.join(src, dir_), dir_, recursive=False)
                archived_folders.add(dir_)
        # name inside archive
        arcname = os.path.join(os.path.basename(src), f[1]) if with_root \
                  else f[1]
        # add file to archive
        archive.add(f[0], arcname)
    archive.close()
    return archive_path


## Pack source location to ZIP archive.
# @param src Source location.
# @param store_path Folder name, where archive should be stored.
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @return Path to result archive.
def pack_zip(src, store_path, with_root=False, pattern="",
             invert=False, replace=True):
    # make archive name
    archive_name = os.path.basename(src) + ".zip"
    archive_path = os.path.join(store_path, archive_name)
    if os.path.exists(archive_path) and not replace:
        raise SACError(
            "LOGIC_ERROR", "Destination path exists and replace is not allowed",
            destination_path=archive_path
        )
    # select files
    files = [(os.path.join(src, *file_), os.path.join(*file_)) for file_ in
             yield_content(src, pattern, invert)]
    # remove old archive, if exists
    if os.path.exists(archive_path):
        os.remove(archive_path)
    # create archive
    archive = zipfile.ZipFile(archive_path, "w", zipfile.ZIP_DEFLATED)
    # pack files into archive
    for f in files:
        # add file to archive
        arcname = os.path.join(os.path.basename(src), f[1]) if with_root \
                  else f[1]
        archive.write(f[0], arcname)
    archive.close()
    return archive_path


## Detect installation type of 1C:Enterprise platform.
# @param archive_name Name of archive, which needed by archive type of
#  installation
# @param folder Path to distr folder.
# @return "setup" or "copy"
def detect_installation_type(archive_name, folder):
    file_path = os.path.join(folder, archive_name)
    return "setup" if os.path.isfile(file_path) else "copy"


## Compute hash of files and directories recursively.
# @param paths List of paths, that should be included to hash calculation.
# @return MD5 hash.
def compute_recursive_hash(paths):
    _f = LogFunc(message="calculating hash", paths=paths)

    ## Calculate hash of file.
    def update_hash_from_file(file_path, hash_obj):
        with open(file_path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_obj

    ## Calculate hash over directory.
    def update_hash_from_folder(path, hash_obj):
        for entry in os.listdir(path):
            if os.path.isdir(os.path.join(path, entry)):
                hash_obj = update_hash_from_folder(
                    os.path.join(path, entry),
                    hash_obj
                )
            else:
                hash_obj = update_hash_from_file(
                    os.path.join(path, entry),
                    hash_obj
                )
        return hash_obj

    hash_md5 = hashlib.md5()
    # apply hash functions to all paths
    for path in paths:
        if os.path.isdir(path):
            hash_md5 = update_hash_from_folder(path, hash_md5)
        else:
            hash_md5 = update_hash_from_file(path, hash_md5)
    return hash_md5.hexdigest()


## Walk over dictionary and apply function to all leaves.
# @param node Dictionary.
# @param func Function, that should accept leaf value and return value, that
#  will be set to leaf.
# @param pass_key Should key be passed to func as second arg or not.
def apply_to_leaves(node, func, pass_key=False):
    for key, value in node.items():
        if hasattr(value, "items"):
            apply_to_leaves(value, func, pass_key)
        else:
            if pass_key:
                node[key] = func(value, key)
            else:
                node[key] = func(value)


## Return regex for matching symbols, which appears similarly in latin (English)
#  and cyrrilic (Russian) alphabets.
# @param char Symbol.
# @return Regex with both (latin and cyrrilic) symbols, if it appears similarly
#  or just symbol, if it is unique.
def get_regex_for_similar_char(char):
    latin    = "A a B C c E e H K k M O o P p T X x".split(" ")
    cyrrilic = "А а В С с Е е Н К к М О о Р р Т Х х".split(" ")
    blank = "(?:{}|{})"
    index = None
    if char in latin:
        index = latin.index(char)
    if char in cyrrilic:
        index = cyrrilic.index(char)
    return char if index is None else blank.format(latin[index],
                                                   cyrrilic[index])


## Replace in regex symbols, which appears similarly in latin (English)
#  and cyrrilic (Russian) alphabets, with regex like (?:x|х).
# @param regex Input regex string.
# @return Regex with replaced symbols.
def replace_similar_symbols(regex):
    output = ""
    for char in regex:
        output += get_regex_for_similar_char(char)
    return output


## Replace in regex special symbols with escaped sequences.
# @param regex Input regex string.
# @param additional_symbols Additional symbols, which should be escaped.
# @return Regex string with escaped symbols.
def escape_special(regex, additional_symbols=[]):
    to_replace = ". ( ) \\".split(" ") + additional_symbols
    result = ""
    for sym in regex:
        if sym in to_replace:
            result += "\\" + sym
        else:
            result += sym
    return result


## Advance the iterator n-steps ahead. If n is None, consume entirely.
#  https://github.com/tmr232/awesomelib/blob/master/awesome/iterator.py#L17
def consume(iterator, n):
    if n is None:
        collections.deque(iterator, maxlen=0)
    else:
        next(islice(iterator, n, n), None)


class XvfbWrapper:
    def __init__(self):
        self.__display_file_fd, self.__display_file_path = tempfile.mkstemp()
        # start Xvfb and make it write its display to temporary file
        try:
            self.__xvfb_proc = sp.Popen(["Xvfb", "-displayfd",
                                         str(self.__display_file_fd)],
                                        stdout=sp.PIPE, stderr=sp.PIPE,
                                        pass_fds=(self.__display_file_fd,))
        except:
            raise SACError("RUNTIME_ERROR", "Cannot start Xvfb")
        # detect DISPLAY number
        os.close(self.__display_file_fd)
        # now read file and set display number
        for i in range(0, 10):
            try:
                with open(self.__display_file_path) as f:
                    time.sleep(1)
                    display_line = f.readline()
                    self.__display_number = re.search("(\\d+)",
                                                      display_line).groups()[0]
                    break
            except:
                if i < 9:
                    continue
                else:
                    os.remove(self.__display_file_path)
                    self.__xvfb_proc.terminate()
                    raise SACError(
                        "RUNTIME_ERROR", "Cannot detect Xvfb display number"
                    )
        os.remove(self.__display_file_path)
        os.putenv("DISPLAY", ":{}.0".format(self.__display_number))
        global_logger.debug(message="Temporary display number",
                            display_number=self.__display_number)

    def __del__(self):
        self.__xvfb_proc.terminate()
        os.unsetenv("DISPLAY")


## Execute 1C:Enterprise client. On Windows it is possible to use PsExec utility
#  to bypass Session 0 Isolation.
# @param cmd Command line.
# @param timeout Execution time limit for command.
# @param break_check Additional success check.
# @return Process result.
def execute_1c_client(cmd, timeout=None, break_check=None):
    # append /out, if not presented
    if not any(map(lambda x: x.lower() in ["/out", "--out", "-out"], cmd)):
        cmd.append("/Out")
        full_file_name = os.path.join(
            os.path.abspath(os.getcwd()), "script_logs",
            "1cOut_" + os.path.basename(sys.modules["__main__"].__file__)[:-3] \
            + "_" + str(datetime.datetime.now().strftime("%y%m%d_%H%M%S")) + \
            "_" + str(os.getpid()) + ".log"
        )
        cmd.append(full_file_name)
        global_logger.info(message="1C:Enterprise client output",
                           path=full_file_name)
    # get functions
    if detect_actual_os_type() == "Windows":
        from lib.win_utils import ProcessTree
        # try to bypass Session 0 Isolation with PsExec utility
        if int(platform.win32_ver()[1][0]) >= 6:
            _, cmd = get_psexec_session_command(cmd)
    else:
        from lib.linux_utils import ProcessTree
    global_logger.info(message="Running command",
                       cmd=sp.list2cmdline(cmd))
    # create root process and its process tree
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=False)
    pt = ProcessTree(proc.pid)
    # temporary workaround for Xvfb
    if detect_actual_os_type() != "Windows":
        time.sleep(1)
        proc.poll()
        if proc.returncode not in [0, None]:
            proc.terminate()
            pt.update()
            pt.kill()
            # this variable will be automatically deleted
            xvfb = XvfbWrapper()
            proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE,
                            shell=False)
            pt = ProcessTree(proc.pid)
    # now real action begin
    endtime = dt.datetime.now() + dt.timedelta(seconds=timeout) \
        if timeout is not None else None
    # execute until at least one condition hit
    while True:
        time.sleep(1)
        proc.poll()
        # if break_check hit, return 0, which mean successful execution
        if break_check is not None and break_check():
            pt.kill()
            retcode = proc.poll()
            stdout, stderr = proc.communicate()
            return CompletedProcess(proc.args, 0, stdout, stderr)
        if pt.update() < 1:
            proc.wait()
            break
        if endtime is not None and dt.datetime.now() >= endtime:
            pt.kill()
            raise SACError("TIMEOUT_ERROR")
    retcode = proc.poll()
    stdout, stderr = proc.communicate()
    return CompletedProcess(proc.args, retcode, stdout, stderr)
