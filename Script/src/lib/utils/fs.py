import os
import re
import shutil
import stat


from ..common.errors import SACError


## Check, if path is a correct path in FS (ie can it be used in open() function)
# @param path String with path.
# @return True, False or None (if check fails).
def is_os_path(path):
    try:
        f = open(path, "r")
        f.close()
    except OSError as err:
        if err.errno == 22:
            return False
    except Exception:
        return None
    return True


## Get all intermediate paths.
# @param path Path.
# @return List of intermediate path, where first element is full path and last
#  element is root folder, ie if input path is /tmp/folder/subfolder result
#  would be (/tmp/folder/subfolder, /tmp/folder, /tmp, /).
def get_inter_paths(path):
    parts = split_path(path)
    return [os.path.join(*parts[0:i + 1]) for i in range(0, len(parts))[::-1]]


## Set owner user write permission (which on Windows clear read-only flag).
# @param path PAth to file.
def clear_readonly_flag(path):
    os.chmod(path, stat.S_IWRITE)


## Apply function to each subfolder in path.
# @param path Path.
# @param func Function to apply.
def apply_to_each_subfolder(path, func):
    for dir_ in get_inter_paths(path[0]):
        func(dir_)


## Remove file. On PermissionError try to clean readonly flag and try again.
# @param path Path of file to delete.
def remove_file(path):
    try:
        os.remove(path)
    except PermissionError:
        try:
            clear_readonly_flag(path)
            os.remove(path)
        except OSError as err:
            raise SACError("OS_ERROR", "error while removing file",
                           error=str(err))


## Copy stats and owner data (on Windows very limited).
# @param src Source path.
# @param dst Destination path.
def copystat(src, dst):
    shutil.copystat(src, dst)
    # on Windows os.chown not available, so just catch and ignore error
    try:
        stat = os.stat(src)
        os.chown(dst, stat.st_uid, stat.st_gid)
    except:
        pass


## Enumerate files in path.
# @param path Source path.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @return Generator, where each element is (subdirectory, file name).
def yield_content(path, pattern, invert):

    def onerror_handler(error):
        raise SACError("OS_ERROR", str(error))

    # append the separator to the end of path
    path = path + os.sep if os.path.split(path)[1] != "" else path
    for dir_, subdirs, files in os.walk(path, onerror=onerror_handler):
        for file_ in files:
            # compare file path with pattern
            A = pattern != ""
            B = re.search(pattern, os.path.join(dir_, file_))
            if A and ((B and invert) or not (B or invert)):
                continue
            # return subfolder and file name
            yield (dir_[len(path):], file_)


## Apply specified function to files in source location.
# @param func Function to apply.
# @param src Source location.
# @param dst Destination location.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @return Destination location.
def func_file_tree(func, src, dst, pattern, invert, replace):

    def preserve_user(path_):
        try:
            copystat(os.path.join(src, path_), os.path.join(dst, path_))
        except:
            pass

    for file_ in yield_content(src, pattern, invert):
        dirname = os.path.join(dst, file_[0])
        if not os.path.exists(dirname):
            try:
                os.makedirs(dirname)
            except OSError as err:
                raise SACError("OS_ERROR",
                               "Cannot create directory",
                               path=dirname,
                               action=func.__name__,
                               error=str(err))
            apply_to_each_subfolder(dirname, preserve_user)
        dst_ = os.path.join(dst, *file_)
        if not replace and os.path.exists(dst_):
            raise SACError(
                "LOGIC_ERROR",
                "Destination path already exists and replace disabled",
                destination_path=dst_
            )
        if os.path.exists(dst_):
            remove_file(dst_)
        func(os.path.join(src, *file_), dst_)
        # use lexists, because dst_ can be symlink
        if not os.path.lexists(dst_):
            raise SACError("OS_ERROR",
                                         "Cannot perform action on file",
                                         from_=os.path.join(src, *file_),
                                         to=dst_,
                                         action=func.__name__)
        try:
            os.removedirs(os.path.join(src, file_[0]))
        except:
            pass
    return dst


## Apply func to directory tree or file.
# @param func Function to apply.
# @param src Source location.
# @param dst Destination location (directory).
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
# @exception ValueError If src either not directory and file.
def func_file_or_directory(func, src, dst, with_root, pattern,
                           invert, replace):
    if not os.path.exists(dst):
        os.makedirs(dst)
    if os.path.isdir(src):
        # cut last folder from src and create it in destination location,
        # if with_root flag set
        if with_root:
            src_dir = os.path.basename(src)
            dst_dir = os.path.join(dst, src_dir)
            if not os.path.exists(dst_dir):
                os.makedirs(dst_dir)
            func_file_tree(func, src, dst_dir, pattern, invert, replace)
        else:
            func_file_tree(func, src, dst, pattern, invert, replace)
    elif os.path.isfile(src):
        # test file against pattern and perform action
        A = pattern != ""
        B = re.search(pattern, src)
        if A and ((B and invert) or not (B or invert)):
            return
        if replace and os.path.exists(os.path.join(dst, os.path.basename(src))):
            remove_file(os.path.join(dst, os.path.basename(src)))
        func(src, dst)
    else:
        raise ValueError("'src' must be a file or directory")


## Copy file from source location to destination.
# @param src Source location.
# @param dst Destination location (directory).
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
# @param replace Allow to replace files in destination location.
def copy_file_or_directory(src, dst, with_root=True, pattern="",
                           invert=False, replace=False):

    def copy(src, dst):
        try:
            shutil.copy2(src, dst)
        except OSError as err:
            raise SACError("OS_ERROR", "error while copy file", error=str(err))
        try:
            stat = os.stat(src)
            os.chown(dst, stat.st_uid, stat.st_gid)
        except:
            pass

    return func_file_or_directory(copy, src, dst, with_root, pattern,
                                  invert, replace)


## Move file from source location to destination.
# @param src Source location.
# @param dst Destination location (directory).
# @param with_root If True, perform action on src itself. If False, perform
#  action on src content.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
def move_file_or_directory(src, dst, with_root=True, pattern="",
                           invert=False):

    def move(src, dst):
        try:
            shutil.move(src, dst)
        except OSError as err:
            raise SACError("OS_ERROR", "error while move file", error=str(err))
        except shutil.Error as err:
            raise SACError("OS_ERROR", "error while move file", error=str(err))

    return func_file_or_directory(move, src, dst, with_root, pattern,
                                  invert, True)


## Remove file or directory.
# @param src Path to remove.
# @param pattern Pattern, which is used to include (or exclude) files.
# @param invert If True, pattern exclude files.
def remove_file_or_directory(src, pattern="", invert=False):
    if os.path.isdir(src):
        # if pattern not specified, use standard function
        if pattern == "":
            shutil.rmtree(src)
        # otherwise get list of files, which match with the pattern
        # and delete them
        else:
            files = yield_content(src, pattern, invert)
            for file_ in files:
                remove_file(os.path.join(src, *file_))
            dirs = set([i[0] for i in files])
            for dir_ in dirs:
                while True:
                    try:
                        os.rmdir(dir_)
                        dir_ = os.path.dirname(dir_)
                    except:
                        break
    elif os.path.isfile(src):
        A = pattern != ""
        B = re.search(pattern, src)
        if A and ((B and invert) or not (B or invert)):
            return
        remove_file(src)
    else:
        raise ValueError("'src' must be a file or directory")


## Split path to elements. Root directory (ie leading slash) is preserved as
#  element. Drive letter or UNC (\\host\dir) prefix is stored as first element
#  if presented and OS support it. This function
# @param path Path to split.
# @return list of elements.
def split_path(path):

    def _split(_path):
        result = []
        h, t = os.path.split(_path)
        if h == "":
            return [t, ]
        elif t == "":
            if h == _path:
                return [h, ]
            else:
                return _split(h)
        else:
            return [t, ] + _split(h)

    h, t = os.path.splitdrive(path)
    result = []
    # if drive (ie h) is not empty< add it
    if h != "":
        result.append(h)
    result += [i for i in _split(t)[::-1]]
    return result


## Expand user folder and variables in path, make it absolute and resolve links.
# @param path Path.
# @return Expanded path.
def expand(path):
    return os.path.realpath(os.path.expanduser(os.path.expandvars(
        path
    )))


## Check that paths overlaps (ie one not fully contain in others or they
#  not same).
# @param path Path.
# @param paths Additional paths.
# @return True, of overlaps, False otherwise.
def overlaps(path, *paths):
    path = split_path(expand(path))
    paths = [split_path(expand(p)) for p in paths]
    for elements in zip(path, *paths):
        if len(set(elements)) > 1:
            return False
    return True


## Return common path of multiple paths.
# @param path Path.
# @param paths Additional paths.
# @return Common path.
def common_path(path, *paths):
    path = split_path(expand(path))
    paths = [split_path(expand(p)) for p in paths]
    result = ""
    for elements in zip(path, *paths):
        if len(set(elements)) > 1:
            break
        result = os.path.join(result, elements[0])
    return result
