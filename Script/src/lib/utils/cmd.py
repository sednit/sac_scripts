# coding: utf-8

import subprocess as sp
import tempfile
import os
import sys


from ..common.errors import SACError
from ..common.logger import global_logger


## Class, which represent completed process.
# @description Class, which represent completed process. Its implementation same
#  as in CPython 3.5 except check_errors,
class CompletedProcess:

    ## Constructor.
    # @param args String or list of command line arguments.
    # @param returncode Code, that was returned by process.
    # @param stdout Content of standard output.
    # @param stderr Content of standard error output.
    def __init__(self, args, returncode, stdout=None, stderr=None):
        self.args = args
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr

    ## Debug representation of object.
    # @param self Pointer to object.
    def __repr__(self):
        args = ['args={!r}'.format(self.args),
                'returncode={!r}'.format(self.returncode)]
        if self.stdout is not None:
            args.append('stdout={!r}'.format(self.stdout))
        if self.stderr is not None:
            args.append('stderr={!r}'.format(self.stderr))
        return "{}({})".format(type(self).__name__, ', '.join(args))


## Simple implementation of subprocess.run from CPython 3.5.
# @param args String or list of command line arguments.
# @param shell Should args be executed in shell or not.
# @param timeout Time for process execution. If execution time exceeds timeout,
#  sp.TimeoutExpired raised.
# @return CompletedProcess object.
# @exception TimeoutExpired
def run_cmd(args, shell=False, timeout=None):
    try:
        with sp.Popen(args, stdout=sp.PIPE, stderr=sp.PIPE, shell=shell) \
             as process:
            global_logger.debug(args=process.args, pid=process.pid)
            try:
                stdout, stderr = process.communicate(timeout=timeout)
            except sp.TimeoutExpired:
                process.kill()
                stdout, stderr = process.communicate()
                raise sp.TimeoutExpired(process.args, timeout, output=stdout,
                                        stderr=stderr)
            except Exception as err:
                process.kill()
                process.wait()
                raise
            retcode = process.poll()
            cp = CompletedProcess(process.args, retcode,
                                  stdout.replace(b"\\U", b"\\\\U"),
                                  stderr.replace(b"\\U", b"\\\\U"))
            global_logger.debug(message="Process result", args=cp.args,
                                pid=process.pid, stdout=cp.stdout,
                                stderr=cp.stderr)
            return cp
    except OSError as os_err:
        err_args = {"args": args, "os_str_error": os_err.strerror,
                    "shell_enabled": shell, "timeout": timeout}
        if hasattr(os_err, "winerror"):
            err_args["os_error_code"] = os_err.winerror
        else:
            err_args["os_error_code"] = os_err.errno
        raise SACError("OS_ERROR", "error occurred on Popen creation",
                       **err_args)


def get_active_sessions():
    query_exe_path = None
    try:
        run_cmd("C:\\Windows\\Sysnative\\query.exe")
        query_exe_path = "C:\\Windows\\Sysnative\\query.exe"
    except:
        try:
            run_cmd("query.exe")
            query_exe_path = "query.exe"
        except:
            return []
    res = run_cmd("chcp 437 & \"{}\" user".format(query_exe_path), shell=True)
    try:
        output = filter(
            bool,
            res.stdout.decode("437").replace("\r", "").split("\n")
        )
        users = [list(filter(bool, i.split(" "))) for i in list(output)[2:]]
        return [(i[2], i[1]) for i in users if i[3].lower() == "active"]
    except:
        return []


def get_psexec_session_command(args):
    fallback = False
    try:
        psexec_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "..", "..", \
            "..", "external_utils", "psexec", "psexec.exe"
        )
        if not os.path.exists(psexec_path):
            raise Exception
    except:
        fallback = True
    if fallback:
        if not hasattr(get_psexec_session_command, "warned"):
            setattr(get_psexec_session_command, "warned", True)
            global_logger.warning(
                message="Graphical user interface of started 1C:Enterprise "
                "client disabled. To enable it, download PsExec utility from "
                "Windows Sysinternals web site and place it in "
                "{autiomation_library_root_folder}/external_utils/psexec folder"
            )
        return False, args
    proc_tree_controller_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "..", \
        "psexec_proc_tree_controller.py"
    )
    # get active sessions
    active_sessions = get_active_sessions()
    if len(active_sessions) == 0:
        if not hasattr(get_psexec_session_command, "warned"):
            setattr(get_psexec_session_command, "warned", True)
            global_logger.warning(message="Cannot detect active sessions")
        return False, args
    # try start something in that session
    test_args = [psexec_path, "-s", "-i", active_sessions[0][0], "/accepteula",
                 sys.executable, "--version"]
    res = run_cmd(test_args)
    if res.returncode != 0:
        if not hasattr(get_psexec_session_command, "warned"):
            setattr(get_psexec_session_command, "warned", True)
            global_logger.warning(
                message="Cannot use PsExec.exe to run programs",
                args=test_args,
                returncode=res.returncode,
                stdout=res.stdout,
                stderr=res.stderr
            )
        return False, args
    # create temporary file, where arguments will be stored (this is necessary
    # because PsExed cannot accept arguments longer than 256 symbols)
    fd, path = tempfile.mkstemp()
    os.close(fd)
    with open(path, "w") as f:
        [f.write(line + "\n") for line in args]
    args = [psexec_path, "-s", "-i", active_sessions[0][0], "/accepteula",
            sys.executable, proc_tree_controller_path, path]
    return True, args
