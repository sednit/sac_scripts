import subprocess as sp
import shlex
import re
import os


from .fs import *


def detect_apache_str_param(apache_executable, param_name):
    proc = sp.Popen(args=[apache_executable, "-V"], stdout=sp.PIPE,
                    stderr=sp.DEVNULL)
    out, _ = proc.communicate()
    out = out.decode()
    value = re.search("-D {}+=\"(.*?)\"".format(param_name), out).groups()[0]
    return value


def replace_path_in_load_module_string(s, new_path):
    return " ".join([" ".join(s.split(" ")[0:2]), "\"" + new_path + "\""])


def get_apache_version(apache_executable):
    proc = sp.Popen(args=[apache_executable, "-V"], stdout=sp.PIPE,
                    stderr=sp.DEVNULL)
    out, _ = proc.communicate()
    out = out.decode()
    return re.search("Server version: +apache\\/(\\d+)\\.(\\d+)\\.(\\d+)",
                      out, re.I).groups()[0:3]
    return value
