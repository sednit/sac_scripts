from enum import Enum


class ReturnCode(Enum):
    SUCCESS = 0
    UNKNOWN_ERROR = 1
    ERROR_STATE_NOT_CHANGED = 2
    ERROR_STATE_CHANGED = 4
    ROLLBACKED = 3  # this code only appears in composite scenario runner


## Langs, available in platform.
LANGS = ["az", "en", "bg", "hu", "vi", "ka", "zh", "lv", "lt", "de", "pl", "ro",
         "ru", "tr", "uk", "fr"]
