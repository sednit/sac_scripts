import sys
import os
import tempfile
import shutil
import inspect


from . import bootstrap
from .bootstrap import *
from .errors import *
from .logger import *
from ..utils import *
from ..utils.cmd import run_cmd
from . import global_vars as gv
from .const import ReturnCode
from .config import *



class BaseScenario:

    # these methods should not be redefined in inherited classes without
    # strong reason

    def _prepare_files(self):
        # setting up pid file
        pid = os.getpid()
        self._pid_filename = get_pid_filename(pid)
        pid_file = open(self._pid_filename, "w")
        pid_file.write(str(pid))
        pid_file.close()
        # setting up logger
        log_folder = "script_logs"
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)
        global_logger.add_file_handler(
            get_log_filename(
                # set file name from name of file where class declared
                os.path.basename(sys.modules["__main__"].__file__)[:-3], pid,
                log_folder
            )
        )
        global_logger.add_stream_handler(sys.stdout)

    ## Constructor.
    # @param self Pointer to object.
    def __init__(self):
        self._prepare_files()
        # creating Configuration
        self.config = ScenarioConfiguration(read_yaml(sys.argv[1]))
        cmd_args = parse_cmd_args(sys.argv[2:])
        self.config.add_cmd_args(cmd_args[1], True)
        if "composite-scenario-name" in self.config:
            global_logger.info(
                message="Execute as part of composite scenario",
                composite_scenario_name=self.config["composite-scenario-name"]
            )
        else:
            global_logger.info(
                message="Execute as standalone scenario"
            )
        global_logger.info(message="Script parameters", args=cmd_args[1])
        set_debug_values(cmd_args[1])
        # create temporary folder. This folder would be removed when
        # scenario object is destroyed, this folder is like
        # "session data". Name is made with "1c" prefix + random name +
        # PID.
        self.tmp = tempfile.mkdtemp("-" + str(os.getpid()), "1c-")
        # validate configuration
        self.__validate_config()
        # set global test mode variable
        gv.TEST_MODE = self.config["test-mode"]
        # perform after init action
        self._after_init()
        # log self.configuration
        global_logger.debug("Scenario data: " + str(self.config))

    ## Destructor.
    # @param self Pointer to object.
    def __del__(self):
        try:
            shutil.rmtree(self.tmp)
        except Exception as err:
            global_logger.warning(message="Cannot remove temporary folder",
                                  error=str(err))
        try:
            os.remove(self._pid_filename)
        except:
            global_logger.warning(message="Couldn't remove pid file",
                                  pid_filename=self._pid_filename)

    ## Validating config.
    # @param self Pointer to object.
    # @exception SACError("OPTION_NOT_FOUND")
    def __validate_config(self):
        # first piece of data
        validate_data = [
            # common parameters
            ["test-mode", bool],
            ["time-limit", int, int, lambda x: x > 0],
            ["os-type", str, str, ["Windows", "Linux-deb", "Linux-rpm"]],
        ]
        self.config.validate(validate_data)
        if "restrict-tests" in self.config:
            validate_data = [["restrict-tests", bool], ]
            self.config.validate(validate_data)
        else:
            self.config["restrict-tests"] = False
        self._validate_specific_data()

    ## Execute tests.
    # @param self Pointer to object.
    def __tests(self):
        l = LogFunc(message="Running tests")
        available_tests = self._get_available_tests()
        for name, test, standalone_only in available_tests:
            if self.config["restrict-tests"] and standalone_only:
                global_logger.info(message="Omitting test", name=name)
                continue
            test()

    ## Execute scenario.
    # @param self Pointer to object.
    def execute(self):
        self.__tests()
        if self.config["test-mode"]:
            global_logger.info("Test mode completed successfully")
        else:
            bootstrap.SYSTEM_STATE_CHANGED = True
            self._real()

    @classmethod
    ## Main method.
    # @param Cls Scenario class.
    # @param init_args Positional arguments, which will be passed to class
    #  constructor.
    # @param init_kwargs Named arguments, which will be passed to class
    #  constructor.
    def main(Cls, *init_args, **init_kwargs):

        def log_result_and_get_code(_op_uuid, _error_encountered):
            code = ReturnCode.SUCCESS if not _error_encountered else \
               ReturnCode.ERROR_STATE_CHANGED if \
               bootstrap.SYSTEM_STATE_CHANGED else \
               ReturnCode.ERROR_STATE_NOT_CHANGED
            time = global_logger.finish_operation(_op_uuid)
            global_logger.info(
                message="Scenario execution finished",
                duration=int(
                    time.microseconds * 10**-3 + time.seconds * 10**3
                ),
                scenario_name=Cls.__name__, result_code=code.value)
            return code

        def break_by_timeout(_op_uuid):
            try:
                raise SACError("TIMEOUT_ERROR")
            except Exception as e:
                Cls._handle_top_level_exception(e)
            os._exit(log_result_and_get_code(_op_uuid, True).value)

        op_uuid = global_logger.start_operation()
        # execute scenario
        try:
            scenario = Cls(*init_args, **init_kwargs)
            # after creating scenario object start timer
            t = threading.Timer(scenario.config["time-limit"], break_by_timeout,
                                (op_uuid, ))
            t.start()
            # execute scenario
            scenario.execute()
        except Exception as err:
            Cls._handle_top_level_exception(err)
            error_encountered = True
        else:
            error_encountered = False
        # stop timer, if set
        try:
            t.cancel()
        except:
            pass
        sys.exit(log_result_and_get_code(op_uuid, error_encountered).value)

    # these functions can be (which is recommended) redefined in inherited
    # classes to provide necessary functionality.

    ## Method, which handles validation of scenario-specific data. It called
    #  in BaseScenario.validate_config() method after validation of common
    #  values like test-mode, time-limit, os-type and standalone.
    # @param self Pointer to object.
    def _validate_specific_data(self):
        pass

    ## Method, which is called right after main __init__() part, but before
    #  printing config property. This method should handle scenario-specific
    #  preparations for it execution, like set additional values, etc.
    #  STRONGLY not recommended to change state of object in this method!
    # @param self Pointer to object.
    def _after_init(self):
        pass

    ## Method, which return list of available tests.
    # @param self Pointer to object.
    # @return List of tuples (test string name, function, standalone only flag).
    def _get_available_tests(self):
        return []

    ## Real part of scenario execution, ie where actual work takes place.
    # @param self Pointer to object.
    def _real(self):
        pass

    # these methods is already implemented, so they could be used as-is in
    # inherited classes as well as they could be redefined.

    ## Method, which handles exceptions. This method SHOULD NOT throw exceptions
    #  in any case. This method should catch ANY exception (ie what inherited
    #  from Exception class). Default implementation of
    #  this method print any SACError via global_logger. Any
    #  non-SACError converted to SACError("UNKNOWN")
    #  and printed via global_logger.
    # @param err Exception object.
    @staticmethod
    def _handle_top_level_exception(err):
        if isinstance(err, SACError):
            global_logger.error(**err.to_dict())
        else:
            err = SACError("UNKNOWN", err)
            global_logger.error(**err.to_dict())


class BaseActionScenario(BaseScenario):

    ## Constructor.
    # @param self Pointer to object.
    # @param config lib::common::config::Configuration object.
    # @param action String with action.
    def __init__(self, action):
        # set action
        self.action = action
        # init base scenario
        super().__init__()
