## Path to .pid file.
PID_PATH = "./"
## Execute only tests or not.
TEST_MODE = None
## global common::config::Configuration object.
CONFIG = None
## Should debug messages be printed.
DEBUG = False
## Should multi line string in log be collapsed.
COLLAPSE_TRACEBACK = True
## Global encoding variable.
ENCODING = "raw_unicode_escape"
## Should function names be printed in log records.
PRINT_FUNCTION = False
## Should operation UUID be printed in log records.
PRINT_UUID = False
## Should operation begin be printed in log records.
PRINT_BEGIN = False
## Escape key values in log records.
ESCAPE_STRINGS = True
