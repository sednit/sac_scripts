# coding: utf-8

import sys


__temp_exc_data = None


## Exception hook, which just store first raised exception.
# @param exctype Exception type.
# @param value Exception.
# @param traceback Traceback object.
def __temp_hook(exctype, value, traceback):
    global __temp_exc_data
    if __temp_exc_data is not None:
        __temp_exc_data = (exctype, value, traceback)


## Setup global exception hook. First exception hook is just store first raised
#  exception.
sys.excepthook = __temp_hook


## Now make imports
import threading
import datetime
import platform
import subprocess as sp
import shlex
import re
import time
import os
from enum import Enum


## Change umask for process
os.umask(0o022)


from .logger import *
from .errors import *
from .const import ReturnCode


# Compare current version of Python interpreter against minimal required
if sys.version_info < (3, 4, 3):
    raise SACError("PYTHON_INCOMPATIBLE_VERSION")


SYSTEM_STATE_CHANGED = False


## Now set real hook, which can perform logging of error.

## Exception hook. Catch error, read SYSTEM_STATE_CHANGED global variable,
#  try log error and exit with specific code.
# @param exctype Exception type.
# @param value Exception.
# @param traceback Traceback object.
def my_except_hook(exctype, value, traceback):
    if SYSTEM_STATE_CHANGED:
        code = ReturnCode.ERROR_STATE_CHANGED
        # wrap logging in try...catch because logger import can fail
        try:
            global_logger.info(message="HINT: FATAL uncaught exception. State "
                               "changed")
        except:
            pass
    else:
        code = ReturnCode.ERROR_STATE_NOT_CHANGED
        # wrap logging in try...catch because logger import can fail
        try:
            global_logger.info(message="HINT: FATAL uncaught exception. State "
                               "not changed")
        except:
            pass
    try:
        if isinstance(value, SACError):
            global_logger.error(**value.to_dict())
        else:
            value = SACError("UNKNOWN", value)
            global_logger.error(**value.to_dict())
        global_logger.info(message="Result code", result_code=code.value)
    except:
        pass
    os._exit(code.value)


## Set global hook.
sys.excepthook = my_except_hook


## If some error raised before setting of real hook, raise that error.
if __temp_exc_data is not None:
    my_except_hook(*__temp_exc_data)


## Detect encoding of default shell.
# @return String, which represent encoding. If decoding cannot be detected,
#  "raw_unicode_escape" returns.
def detect_console_encoding():
    encoding = "raw_unicode_escape"
    system = platform.system()
    try:
        # if system is Windows, then detect via chcp
        if system == "Windows":
            popen = sp.Popen("chcp", shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
            stdout, _ = popen.communicate(timeout=2)
            encoding = re.search(b" ([a-zA-Z0-9_\\-]+)", stdout).groups()[0] \
                .decode(gv.ENCODING)
        # if Linux, set encoding to utf-8
        elif system == "Linux":
            encoding = "utf-8"
        test = "trying encode string".encode(encoding)
        return encoding
    except Exception as err:
        return "raw_unicode_escape"


from ..utils.cvt import str_to_bool


## Set global lib::common::global_vars::ENCODING variable.
gv.ENCODING = detect_console_encoding()


## This function accept bytes and attempt to decode it via chardet or,
#  if it doesn't available, via gv.ENCODING.
# @param bstr Bytes.
# @return Decoded string OR original bytes.
def try_decode(bstr):
    # this is
    if not hasattr(try_decode, "__first_start"):
        setattr(try_decode, "__first_start", True)
        chardet_imported = False
        try:
            import chardet
            chardet.detect(b"")
            chardet_imported = True
        except:
            pass
        if chardet_imported:
            global_logger.debug(message="Chardet found")
        else:
            global_logger.debug(message="Chardet not found")
    try:
        return bstr.decode(chardet.detect(bstr)["encoding"])
    except:
        try:
            return bstr.decode(gv.ENCODING)
        except:
            return "".join(map(chr, bstr))



## Default function for second argument in parse_cmd_args.
def set_debug_values(args):
    for key, value in args.items():
        if key.lower() in ["debug", "collapse-traceback", "print-begin",
                           "print-uuid", "print-function", "escape-strings"]:
            try:
                setattr(gv, key.upper().replace("-", "_"),
                        value)
            except ValueError:
                raise SACError("ARGS_ERROR",
                               "This arg should be True or False",
                               key=key, current_value=value)


## Parse input command line arguments (from sys.argv) an return tuple with
#  (positional_args, named_args).
# @param s Input list or string. If None, then args retrieved from sys.argv[2:].
#  If string, then args splitted via shlex.
# @return Tuple with (positional_args, named_args), where positional_args
#  is list of args and named_args is dict.
def parse_cmd_args(s=None):
    args = ([], {})
    if s is None:
        s = sys.argv
    elif isinstance(s, list):
        pass
    elif isinstance(s, str):
        s = shlex.split(s)
    else:
        raise TypeError("s should be None, str or list")
    for arg in s:
        key = None
        value = "True"
        # split arg on (keys string, value)
        splitted_arg = re.search("--([^=\n]+)(?:=(.*))?", arg)
        # if split is successful, set first captured group as key and
        # if the second group is not None, set it as value ("True" string
        # otherwise)
        if splitted_arg is not None:
            key = splitted_arg.groups()[0]
            if splitted_arg.groups()[1] is not None:
                value = splitted_arg.groups()[1]
        # if split fails, assume that we have positional argument and set value
        # to arg, when keeping key as None.
        else:
            value = arg
        # First, try to convert value to bool, after that to int, and then
        # keep as str
        try:
            value = int(value)
        except:
            pass
        try:
            value = str_to_bool(value)
        except:
            pass
        if key is None:
            args[0].append(value)
        else:
            args[1][key] = value
    return args


def get_pid_filename(pid):
    return os.path.join(gv.PID_PATH, "SAC_{}.pid".format(pid))


def get_log_filename(script_name, pid, log_folder, timestamp=None):
    if timestamp is None:
        timestamp = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
    return os.path.join(
            gv.PID_PATH, log_folder, script_name + "_" + str(timestamp)
            + "_" + str(pid) + ".log"
        )
