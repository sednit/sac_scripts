# coding: utf-8

import struct
import signal
import re
import os
import datetime as dt
from ctypes import windll, c_char_p, byref, sizeof, GetLastError
from ctypes.wintypes import DWORD, BOOL, UINT


from ..common import bootstrap
from ..common import global_vars as gv
from ..common.logger import global_logger
from ..common.errors import SACError
from ..utils import PlatformVersion, try_open_file
from ..utils.cmd import run_cmd


ARCHIVE64_DISTR_NAME = "setup.exe"
ARCHIVE32_DISTR_NAME = "setup.exe"


FormatMessage = windll.kernel32.FormatMessageA


## Analog of macro MAKELANGID
def MAKELANGID(p, s):
    return (s << 10) | p


LANG_ENGLISH = DWORD(0x0C09)
FORMAT_MESSAGE_FLAGS = DWORD(0x1000 | 0x100 | 0x200)


## Get error string representation.
# @param code Code, returned by GetLastError().
# @return String representation.
def format_message(code):
    buffer = c_char_p()
    FormatMessage(FORMAT_MESSAGE_FLAGS, None, code, MAKELANGID(0x09, 0x01),
                  byref(buffer), 0, None)
    # decode always use ascii because FormatMessage called with English language
    return buffer.value.decode("ascii")


## Get list of process' id by name.
# @param name Name of executable.
# @param filter Additional filter, which used as argument to regex search in cmd
# @return List of tuples (pid, proc_image, port, full_cmd).
def get_processes_id_by_name(name, filter=None):
    processes_buffer = (DWORD * 100000)()
    bytes_returned = DWORD(0)
    windll.psapi.EnumProcesses(byref(processes_buffer),
                               DWORD(sizeof(processes_buffer)),
                               byref(bytes_returned))
    target_processes = []
    try:
        procs = execute_wmic_get_command(
            "process", "caption like '%{}%'".format(name),
            "processid,commandline,executablepath", True
        )
    except:
        return []
    # filter founded procs
    for proc in procs:
        if filter is not None and filter not in proc["CommandLine"]:
            continue
        target_processes.append((proc["ProcessId"], proc["ExecutablePath"],
                                 proc["CommandLine"]))
    # log all founded processes
    for proc in target_processes:
        global_logger.debug("found process", name=proc[1], proc_pid=proc[0],
                            cmd=proc[2])
    return target_processes


## Kill process.
# @param pid Process id (PID).
def kill_process(pid):
    handle = windll.kernel32.OpenProcess(DWORD(0x0001), BOOL(True), pid)
    windll.kernel32.TerminateProcess(handle, UINT(-1))
    windll.kernel32.CloseHandle(handle)


## Make dump of process. Dump stored in core.<pid> file.
# @param pid Process id (PID).
# @param folder Folder, where dump should be stored. Default - ./ .
# @return True on success, False otherwise.
def make_dump(pid, folder="."):
    MAXIMUM_ALLOWED = 33554432
    GENERIC_READ = (-2147483648)
    GENERIC_WRITE = 1073741824
    GENERIC_EXECUTE = 536870912
    #  https://msdn.microsoft.com/en-us/library/windows/desktop/ms680519(v=vs.85).aspx
    MiniDumpWithFullMemory = 0x2
    # build dump file name
    if not os.path.exists(folder):
        os.makedirs(folder)
    dump_file_name = c_char_p(os.path.join(
        folder, "{}.dump".format(pid)
    ).encode(gv.ENCODING))
    # create dump file
    hfile = windll.kernel32.CreateFileA(
        dump_file_name, DWORD(GENERIC_WRITE), DWORD(0), None, DWORD(2),
        DWORD(0x80), None
    )
    # if CreateFile fails, log error and return -1
    if hfile == -1:
        windll.kernel32.CloseHandle(hfile)
        global_logger.warning(message="Fail when creating dump",
                              error=format_message(GetLastError()))
        return False
    # open process
    handle = windll.kernel32.OpenProcess(DWORD(0x0400 | 0x0010 | 0x0040),
                                         BOOL(True), pid)
    # create dump
    res = windll.dbghelp.MiniDumpWriteDump(handle, pid, hfile,
                                           DWORD(MiniDumpWithFullMemory),
                                           None, None, None)
    # close handles
    windll.kernel32.CloseHandle(hfile)
    # if dump creation fails, delete dump file
    if res == 0:
        windll.kernel32.DeleteFileA(dump_file_name)
        return False
    windll.kernel32.CloseHandle(handle)
    return True


## Get Version property of executable.
# @param fq_path Full path to file.
# @return String with version or None, if version not set.
def get_exe_version(fq_path):
    if not os.path.exists(fq_path):
        return None
    # extract Version property from file
    res = run_cmd(
        "wmic datafile where Name=\"{}\" get Version".format(
            # double backslashes, cuz wmic accept paths with doubled slashes
            fq_path.replace("\\", "\\\\")
        ),
        shell=True
    )
    # extract version value
    re_search = re.search(r"(\d+\.\d+\.\d+\.\d+)",
                          bootstrap.try_decode(res.stdout), re.MULTILINE)
    # if re_search have groups attribute, then extraction successful
    if hasattr(re_search, "groups"):
        return re_search.groups()[0]
    else:
        return None


## Get version of installed platform.
# @param config lib::common::config::Configuration object.
# @return lib::utils::main::PlatformVersion object.
def get_installed_platform_version(config):
    version = None
    if os.path.exists(os.path.join(config["setup-folder"], "ragent.exe")):
        version = get_exe_version(os.path.join(
            config["setup-folder"], "ragent.exe"
        ))
    else:
        version = get_exe_version(os.path.join(
            config["setup-folder"], "bin", "ragent.exe"
        ))
    if version is None:
        return PlatformVersion(None)
    else:
        return PlatformVersion(version)


## Execute 'wmic <alias> where <conditions> get <properties>' command.
# @param alias Alias for wmic.
# @param conditions String with conditions for wmic. If None, get everything.
# @param properties String with comma separated list of necessary properties.
#  If None, request all properties.
# @param rearrange_result If set, results will be rearranged to list of dicts,
#  which represent row.
# @return CSV-like list of lists, where first list is names of properties
#  (header), and next lists is a rows with values OR list of dicts (rows)
def execute_wmic_get_command(alias, conditions=None, properties=None,
                             rearrange_result=False):
    import csv
    # run wmic request
    condition_line = "where \"{}\"".format(conditions) if conditions else ""
    properties_line = "get {}".format(properties) if properties else "get *"
    cmdline = "wmic {} {} {} /Format:csv".format(alias, condition_line,
                                                 properties_line)
    res = run_cmd(cmdline, shell=True)
    # if returncode is not 0, raise exception
    if res.returncode != 0:
        raise SACError("WMIC_ERROR", res=res)
    # if return code is 0, but stderr not empty, assume that nothing found
    # return empty list
    if len(res.stderr) > 0:
        return []
    # read stdout to csv
    reader = csv.reader(
        [i for i in bootstrap.try_decode(res.stdout).splitlines() if len(i) > 0]
    )
    if rearrange_result:
        csv = [i for i in reader]
        result = list()
        for row in csv[1:]:
            item = dict()
            for index in range(0, len(csv[0])):
                item[csv[0][index]] = row[index]
            result.append(item)
        return result
    else:
        return [i for i in reader]


## Convert WMIC timestamp to datetime
# @param ts Timestamp string.
# @return Datetime object.
def wmic_timestamp_to_datetime(ts):
    return dt.datetime(*[int(i) for i in [ts[0:4], ts[4:6], ts[6:8], ts[8:10],
                                          ts[10:12], ts[12:14], ts[15:21]]])


## Return all child processes (recursively), ie child of child of ...
# @param parent_pid Parent PID.
# @return List of PIDs (int).
def get_all_child_procs(parent_pid, ignore_procs=[]):
    if len(ignore_procs):
        pids = [int(i["ProcessId"]) for i in execute_wmic_get_command(
            "process", "(ParentProcessId={})".format(parent_pid), "ProcessId",
            True
        )]
    else:
        procs = execute_wmic_get_command(
            "process", "(ParentProcessId={})".format(parent_pid), None, True
        )
        for proc in procs:
            proc["ProcessId"] = int(proc["ProcessId"])
            proc["ParentProcessId"] = int(proc["ParentProcessId"])
            proc["CreationDate"] = wmic_timestamp_to_datetime(
                proc["CreationDate"]
            )
    for pid in pids:
        pids += get_all_child_procs(pid, ignore_procs)
    return list(set(pids))


## Get list of all process' children and this process. Root process always first
#  in returned list.
# @param root_proc Process dictionary (with CreationDate and ProcessId keys).
# @param ignore_procs List of dictionaries describing processes, which should be
#  ignored.
# @return List of dictionaries.
def get_proc_children(root_proc, ignore_procs=[]):
    # if root process not alive or its creation date doesn't match to the date
    # date of real process, return empty list, because root_process no longer
    # exists
    try:
        if not proc_alive(root_proc["ProcessId"]) or execute_wmic_get_command(
            "process", "(ProcessId={})".format(root_proc["ProcessId"]),
            None, True
        )[0]["CreationDate"] != root_proc["CreationDate"]:
            return []
    except:
        return []
    # get all processes, whose parent pid is equal to root process pid and
    # which are created after root process
    child_procs = [i for i in execute_wmic_get_command(
        "process", "(ParentProcessId={})".format(root_proc["ProcessId"]),
        None, True
    ) if i["CreationDate"] > root_proc["CreationDate"] and \
                   not any(i["ProcessId"] == j["ProcessId"] and \
                           i["CreationDate"] == j["CreationDate"] \
                           for j in ignore_procs)]
    procs = [root_proc, ]
    # recursively get processes
    for proc in child_procs:
        procs += get_proc_children(proc)
    return procs


## Get lists of children processes of multiple processes.
# @param root_procs List of dictionaries describing the processes.
# @return List of dictionaries describing the processes.
def get_procs_children(root_procs):
    return sum([get_proc_children(proc, root_procs) for proc in root_procs], [])


## Get list of all processes in tree, where root have specific pid.
# @param pid Process ID.
# @return List of dictionaries describing the processes.
def get_proc_children_by_pid(pid):
    return get_procs_children(execute_wmic_get_command(
        "process", "(ProcessId={})".format(pid), None, True
    ))


## Helper class, which allow to control states of processes in tree.
class ProcessTree:

    ## Constructor.
    # @param self Pointer to object.
    # @param pid Process ID of root process.
    def __init__(self, pid):
        self.procs = get_proc_children_by_pid(pid)
        self.root_pid = pid

    ## Update statuses.
    # @param self Pointer to object.
    # @return Number of processes in tree.
    def update(self):
        self.procs = get_procs_children(self.procs)
        return len(self.procs)

    ## Kill processes in tree.
    # @param self Pointer to object.
    # @param force Forced termination of processes.
    # @return True, if not alive process left, False otherwise.
    def kill(self, force=False):
        for proc in self.procs[::-1]:
            try:
                kill_process_tree(proc["ProcessId"])
            except:
                pass
        self.update()
        return len(self.procs) == 0


## Kill process and its children.
# @param Process PID.
def kill_process_tree(pid, force=True):
    if force:
        return run_cmd("taskkill {} /t /pid {}".format(
            "/f" if force else "", pid
        ), shell=True).returncode == 0
    else:
        for _pid in [pid, ] + \
            [i["ProcessId"] for i in get_proc_children_by_pid(pid)]:
            os.kill(_pid, signal.CTRL_C_EVENT)


## Register DLL in registry.
# @param path Path (with file name) to DLL.
def regsvr_register_dll(path):
    try_open_file(path)
    res = run_cmd(["regsvr32", "/s", "/i", "/n", path])
    if res.returncode != 0:
        raise SACError(
            "CMD_RESULT_ERROR", args=res.args,
            returncode=res.returncode,
            stdout=bootstrap.try_decode(res.stdout),
            stderr=bootstrap.try_decode(res.stderr)
        )


## Unregister DLL in registry.
# @param path Path (with file name) to DLL
def regsvr_unregister_dll(path):
    try_open_file(path)
    res = run_cmd(["regsvr32", "/s", "/u", path])
    if res.returncode != 0:
        raise SACError(
            "CMD_RESULT_ERROR", args=res.args,
            returncode=res.returncode,
            stdout=bootstrap.try_decode(res.stdout),
            stderr=bootstrap.try_decode(res.stderr)
        )


## Get executable machine architecture (can identify x86 3 or 64 bit).
# @param path Path (with file name) to file.
# @return 64, 32 or None, of file is not executable or for unknown machine.
def get_exe_arch(path):
    f = open(path, "rb")
    f.seek(0x3C)
    # not sure which size of offsets used in PE Signature, but assume 4 bytes
    sig_offset = struct.unpack("<i", f.read(4))[0]
    f.seek(sig_offset)
    sig = f.read(6)
    # compare signature
    if sig[0:4] != b'PE\0\0':
        return None
    # get machine type
    if sig[4:6] == b'\x4c\x01':
        return 32
    elif sig[4:6] == b'\x64\x86':
        return 64
    else:
        return None


## Check, is process alive.
# @param pid Process id (PID).
# @return True, if alive, False otherwise.
def proc_alive(pid):
    return run_cmd("tasklist /FI \"PID eq {}\" | findstr {}".format(pid, pid),
                   shell=True).returncode == 0


## Find file in 1C installation.
# @param filename File name.
# @param platform_folder Platform's installation folder.
# @return Full path to file or None, if not found.
def find_file_in_1c_installation(filename, platform_folder):
    test_path = os.path.join(platform_folder, filename)
    if os.path.exists(test_path):
        return test_path
    test_path = os.path.join(platform_folder, "bin", filename)
    if os.path.exists(test_path):
        return test_path
    return None


## Separate credential to username and domain. If domain not provided,
#  set domain to dot (".").
# @param s Input credential.
# @return Tuple (username, domain)
def separate_credential(s):
    down_level_logon_name_pattern = re.compile("([^\\\\]*)\\\\([^\\\\]+)")
    user_principal_name_pattern = re.compile("([^@]+)@([^@]+)")
    match = re.match(down_level_logon_name_pattern, s)
    if match is None:
        match = re.match(user_principal_name_pattern, s)
        if match is None:
            return s, None, None
        return match.groups()[0], match.groups()[1], "@"
    return match.groups()[1], match.groups()[0], "\\"


## Build credential string from user data.
# @param username Username.
# @param domain Domain. Can be None, which mean that PC doesn't in domain.
# @param separator Separator, which should be used.
# @return String with credentials.
def build_credential(username, domain, separator):
    if domain is None:
        separator = "\\"
        domain = "."
    if separator == "@":
        return username + separator + domain
    if separator == "\\":
        return domain + separator + username
