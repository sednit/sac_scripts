# coding: utf-8

import os
import collections
import configparser
import platform
import re
from enum import Enum
from functools import reduce


from ..common.logger import global_logger, LogFunc
from ..common.errors import SACError
from ..utils import PlatformVersion, PlatformModules
from ..utils.fs import copy_file_or_directory
from ..utils.cmd import run_cmd
from .win_utils import find_file_in_1c_installation, get_exe_version, \
    get_exe_arch, execute_wmic_get_command
from .apache import replace_ws_ext_in_all_apaches
from .iis import replace_handler_in_all_objects
from .permissions import test_is_admin, check_always_elevated_update


## Enumeration for type of installers
class InstallerType(Enum):
    SETUP = "setup"
    COPY = "copy"


## Function which detect parameters of setup.exe in folder.
# @param path path to installer.
# @return Tuple (version, (tuple of modules), arch).
def detect_setup_exe_type(path):
    msi = None
    for f in os.listdir(path):
        if ("1c" in f.lower() or "1с" in f.lower()) and ".msi" in f.lower():
            msi = f.lower()
            break
    if msi is None:
        return ((), None)
    # detect arch
    if "64" in msi:
        arch = 64
    else:
        arch = 32
    if arch is None:
        return ((), None)
    # check which modules this installer can install
    if "thin client" in msi:
        modules = (PlatformModules.THIN, ) if arch == 32 \
                  else (PlatformModules.THIN64, )
    elif "server" in msi:
        modules = (PlatformModules.SERVER, PlatformModules.WEB) if arch == 32 \
                  else (PlatformModules.SERVER64, PlatformModules.WEB64)
    else:
        modules = (PlatformModules.CLIENT, PlatformModules.SERVER,
                   PlatformModules.WEB, PlatformModules.THIN) if arch == 32 \
                   else (PlatformModules.CLIENT64, PlatformModules.SERVER64,
                         PlatformModules.WEB64, PlatformModules.THIN64)
    # parse setup.ini and extract version property
    config = configparser.ConfigParser()
    config.read(os.path.join(path, "setup.ini"))
    version = PlatformVersion(config["Startup"]["ProductVersion"])
    return (version, modules, arch)


## Detect modules in installed 1C:Enterprise platform folder.
# @param path path to folder.
# @return Tuple (version, (tuple of modules), arch).
def detect_modules_in_installation(path):
    # dictionary with files, which indicates files, specific for modules
    test_files = {
        PlatformModules.CLIENT: {"1cv8.exe", },
        PlatformModules.SERVER: {"ragent.exe", "rmngr.exe", "rphost.exe",
                                 "ras.exe", "rac.exe"},
        PlatformModules.WEB: {"wsisapi.dll", "wsap22.dll", "wsap24.dll",
                              "wsapch2.dll"},
        PlatformModules.THIN: {"1cv8c.exe", }
    }
    # dictionary, which map 32 bit modules to 64 bit
    replace_arch = {
        PlatformModules.CLIENT: PlatformModules.CLIENT64,
        PlatformModules.SERVER: PlatformModules.SERVER64,
        PlatformModules.WEB: PlatformModules.WEB64,
        PlatformModules.THIN: PlatformModules.THIN64,
    }
    # get set of files in directory
    files = {f for f in os.listdir(path)}
    result_modules = []
    # check each test_files entry for
    for key, value in test_files.items():
        if value.issubset(files):
            result_modules.append(key)
    # if nothing found, return tuple of 2 empty tuples
    if len(result_modules) < 1:
        return ((), None)
    # assume that all files in directory have same arch
    arch = get_exe_arch(os.path.join(path,
                                     list(test_files[result_modules[0]])[0]))
    version = PlatformVersion(get_exe_version(os.path.join(
        path, list(test_files[result_modules[0]])[0]
    )))
    if arch == 64:
        result_modules = tuple([replace_arch[i] for i in result_modules])
    return (version, result_modules, arch)


## Class, which represent platform installer (ie something, that can be used for
#  platform installation, like setup.exe or already copy of installed platform).
class Windows1CEnterprisePlatformInstaller:

    ## Constructor.
    # @param self Pointer to object.
    # @param path Path to folder with installer.
    def __init__(self, path):
        # if path is not a folder, raise and error
        if not os.path.isdir(path):
            raise SACError("ARGS_ERROR",
                                         "Specified path is not a folder",
                                         path=path)
        # detect installer properties. First, try to detect as setup.exe. If
        # fails, try as copy. Otherwise, raise an error
        data = detect_setup_exe_type(path)
        if data == ((), None):
            data = detect_modules_in_installation(path)
            if data == ((), None):
                raise SACError(
                    "ARGS_ERROR",
                    "Specified path doesn't contain any 1C:Enterprise "
                    "Platform installers",
                    path=path
                )
            self.type = InstallerType.COPY
        else:
            self.type = InstallerType.SETUP
        self.version = data[0]
        self.modules = data[1]
        self.arch = data[2]
        self.path = path

    ## Check that installer can install module(s).
    # @param self Pointer to object.
    # @param other Module or list of modules to test.
    # @return True, if all can be installed, False otherwise.
    def __contains__(self, other):
        if isinstance(other, collections.Iterable):
            return reduce(lambda acc, x: x in self.modules and acc, other, True)
        else:
            return other in self.modules

    ## Recursively detect installers in specified folder.
    # @param cls Installer's class.
    # @param path Path, where installers should be searched.
    # @return List of installers.
    @classmethod
    def detect_installers(cls, path):
        installers = []
        # first, try to find installer in current folder. If successful,
        # return immediately.
        try:
            installers.append(cls(path))
            return installers
        except:
            pass
        for entry in os.listdir(path):
            _path = os.path.join(path, entry)
            # first: try to find installer in this folder
            try:
                installers.append(cls(_path))
                continue
            except:
                pass
            # next: try to go deeper
            if os.path.isdir(_path):
                installers += cls.detect_installers(_path)
        return installers

    ## Detect installer in folder and detect, which of them is needed for
    #  installing platform with specific version and modules.
    # @param cls Installer's class.
    # @param path Path, where installers should be searched.
    # @param version Needed version of platform.
    # @param required_modules List of required modules.
    # @return Windows1CEnterprisePlatformInstaller.
    @classmethod
    def get_installer(cls, path, version, required_modules):
        available_installers = Windows1CEnterprisePlatformInstaller \
                               .detect_installers(path)
        required_modules = set(required_modules)
        suitable_installer = None
        # filter out installers which cannot install all modules
        for installer in available_installers:
            if not set(installer.modules).issuperset(required_modules):
                continue
            if installer.version != version:
                continue
            suitable_installer = installer
        if suitable_installer is None:
            raise SACError(
                "INSTALL_ERROR",
                "Installers in the specified path cannot meet requirements for "
                "installing specified modules",
                modules=[i.value for i in required_modules], version=version,
                path=path
            )
        else:
            return suitable_installer

    ## Perform installation.
    # @param self Pointer to object.
    # @param setup_folder Folder, where platform should be installed.
    # @param required_modules List of required modules.
    # @param langs Necessary languages.
    def install(self, setup_folder, required_modules, langs):
        if self.type == InstallerType.COPY:
            self._copy_install(setup_folder, required_modules)
        else:
            self._setup_exe_install(setup_folder, required_modules, langs)
        # after installation make sure that necessary files exists
        self.validate_installation(setup_folder, required_modules)

    ## Perform install from copy.
    # @param self Pointer to object.
    def _copy_install(self, setup_folder, required_modules):
        _ = LogFunc(message="Install platform via copy")
        # create install folder, if necessary
        if not os.path.exists(setup_folder):
            os.makedirs(setup_folder)
        # perform installation (copy)
        copy_file_or_directory(self.path, setup_folder, False)

    def _setup_vc_redist(self, setup_folder):
        redist_guids = {
            "86": "{37B55901-995A-3650-80B1-BBFD047E2911}",
            "64": "{FAAD7243-0141-3987-AA2F-E56B20F80E41}"
        }
        vc_redist_installer_regex = re.compile("vc_redist\\.x(\\d{2})\\.exe")
        redist_name_match = None
        # find redist installer
        for file_ in os.listdir(self.path):
            match = re.match(vc_redist_installer_regex, file_)
            if match:
                redist_name_match = match
                break
        # check that redist is installed
        if redist_name_match is not None:
            installed_products = execute_wmic_get_command(
                "product", properties="Name,IdentifyingNumber",
                rearrange_result=True
            )
            # find installed redist
            redist_installed = reduce(
                lambda acc, x: x["IdentifyingNumber"].upper() == \
                redist_guids[redist_name_match.groups()[0]] or acc,
                installed_products, False
            )
            if redist_installed:
                global_logger.info(message="Microsoft Visual C++ "
                                   "Redistributable already installed")
                return
            # if not found, install it
            redist_path = os.path.join(self.path, redist_name_match.string)
            run_cmd("echo.>\"{}:Zone.Identifier\"".format(redist_path),
                    shell=True)
            res = run_cmd("\"{}\" /i /S".format(redist_path), shell=False)
            # 0 is success, 1638 is "program already installed"
            if res.returncode not in [0, 1638]:
                raise SACError(
                    "LOGIC_ERROR",
                    "Cannot install Microsoft Visual C++ Redistributable",
                    vc_redist_file=redist_path
                )
            if res.returncode == 1638:
                global_logger.info(
                    message="Microsoft Visual C++ Redistributable already "
                    "installed"
                )

    ## Perform install via "setup.exe"
    # @param self Pointer to object.
    def _setup_exe_install(self, setup_folder, required_modules, langs):
        _ = LogFunc(message="Install platform via setup.exe")
        # 0. install vc_redist
        self._setup_vc_redist(setup_folder)
        # 1. Building setup.exe path.
        setup_exe_name = "setup.exe"
        setup_exe_path = os.path.join(self.path, setup_exe_name)
        # 2. Remove download location data from file's ADS.
        run_cmd("echo.>\"{}:Zone.Identifier\"".format(setup_exe_path),
                shell=True)
        # 3. Gather installation data.
        server = 1 if PlatformModules.SERVER in required_modules \
                 or PlatformModules.SERVER64 in required_modules else 0
        web_ext = 1 if PlatformModules.WEB in required_modules \
                 or PlatformModules.WEB64 in required_modules else 0
        client = 1 if PlatformModules.CLIENT in required_modules \
                 or PlatformModules.CLIENT64 in required_modules else 0
        thin_client = 1 if PlatformModules.THIN in required_modules \
                 or PlatformModules.THIN64 in required_modules else 0
        components = "DESIGNERALLCLIENTS={} THINCLIENT={}" \
            " WEBSERVEREXT={} SERVER={} CONFREPOSSERVER=0 CONVERTER77=0 " \
            " SERVERCLIENT={} LANGUAGES={}".format(client, thin_client, web_ext,
                                                   server, server,
                                                   langs.upper())
        # 4. Run installation.
        res = run_cmd("\"{}\" /S INSTALLDIR=\"{}\" {}".format(
            setup_exe_path, setup_folder, components
        ), shell=False)

    ## Check that platform in specified folder contains necessary modules.
    # @param setup_folder Folder where platform installed.
    # @param required_modules List of necessary modules.
    @staticmethod
    def validate_installation(setup_folder, required_modules):

        # define function which check file in specific folder and in bin/
        # subfolder
        def check_files_presence(path, file_list):
            if not os.path.exists(path):
                raise SACError(
                        "INSTALL_ERROR", "Folder not found", file=path
                    )
            for f in file_list:
                if f not in os.listdir(path) and \
                   f not in os.listdir(os.path.join(path, "bin")):
                    raise SACError(
                        "INSTALL_ERROR", "File not found", file=f
                    )

        _ = LogFunc(message="Validating installation")
        test_files = {
            PlatformModules.CLIENT: ["1cv8.exe", "1cv8s.exe"],
            PlatformModules.CLIENT64: ["1cv8.exe", ],
            PlatformModules.SERVER: ["ragent.exe", "rmngr.exe", "rphost.exe",
                                     "ras.exe", "rac.exe"],
            PlatformModules.SERVER64: ["ragent.exe", "rmngr.exe", "rphost.exe",
                                       "ras.exe", "rac.exe"],
            PlatformModules.WEB: ["wsisapi.dll", "wsapch2.dll", "wsap22.dll",
                                  "wsap24.dll"],
            PlatformModules.WEB64: ["wsisapi.dll", "wsapch2.dll", "wsap22.dll",
                                    "wsap24.dll"],
            PlatformModules.THIN: ["1cv8c.exe", ],
            PlatformModules.THIN64: ["1cv8c.exe", ]
        }
        for module in required_modules:
            check_files_presence(setup_folder, test_files[module])

    def __repr__(self):
        return "<1C Enterprise Platform Installer " \
            "(path={},type={},version={},modules={},arch={})>" \
            .format(self.path, self.type, self.version, self.modules, self.arch)

    def __str__(self):
        return self.path


## Class, which handle platform installation.
class Platform1CInstaller:

    ## Constructor.
    # @param self Pointer to object.
    # @param config common::config::Configuration object
    def __init__(self, config):
        self.config = config
        self.installer = Windows1CEnterprisePlatformInstaller \
            .get_installer(
                self.config["distr-folder"],
                self.config["version"],
                self.config["platform-modules"]
            )

    ## Testing installation permissions.
    # @param self Pointer to object.
    @staticmethod
    def test_install_permissions():
        _ = LogFunc(message="install permission test")
        if not check_always_elevated_update() and not test_is_admin():
            raise SACError("WIN_INSTALL_PERM_DENIED")

    ## Test update (ie is packages is correct, can be installed etc).
    # @param self Pointer to object.
    def test_installer(self):
        pass

    def install(self):
        _ = LogFunc(message="Install 1C:Enterprise Platform")
        self.installer.install(self.config["setup-folder"],
                               self.config["platform-modules"],
                               self.config["languages"])


def setup_web(setup_folder):
    iis_not_found = False
    apache_not_found = False
    # determine path to web extensions
    wsisapi_path = find_file_in_1c_installation("wsisapi.dll", setup_folder)
    if wsisapi_path is None:
        raise SACError("LOGIC_ERROR", "Cannot find web extension library")
    # if WinNT lower than 6 detected, show warning and omit IIS setup
    if int(platform.win32_ver()[1].split(".")[0]) < 6:
        global_logger.warning(
            message="Supported version of IIS is 7.0 "
            "or higher, which is available on Windows Vista and higher"
        )
        iis_not_found = True
    else:
        # try to replace in both web-servers. SERVICE_ERROR means web-server
        # not found
        try:
            if not replace_handler_in_all_objects(
                    wsisapi_path, False
            ):
                global_logger.info(
                    message="IIS found, but web extension not " \
                    "mentioned in configuration, so it is not updated"
                )
        except SACError as err:
            if err.str_code != "SERVICE_ERROR":
                raise
            iis_not_found = True
    # setting up apache
    try:
        if not replace_ws_ext_in_all_apaches(os.path.split(wsisapi_path)[0],
                                             False):
            global_logger.info(
                message="Apache found, but web extension not " \
                "mentioned in configuration, so it is not updated"
            )
    except SACError as err:
        if err.str_code != "SERVICE_ERROR":
            raise
        apache_not_found = True
    if iis_not_found and apache_not_found:
        raise SACError("LOGIC_ERROR", "Both Apache and IIS server not found")


## Check, can data in web-servers configuration be updated.
def test_web():
    iis_not_found = False
    apache_not_found = False
    try:
        replace_handler_in_all_objects("", True)
    except SACError as err:
        if err.str_code != "SERVICE_ERROR":
            raise
        iis_not_found = True
    try:
        replace_ws_ext_in_all_apaches("", True)
    except SACError as err:
        if err.str_code != "SERVICE_ERROR":
            raise
        apache_not_found = True
    if iis_not_found and apache_not_found:
        raise SACError("LOGIC_ERROR", "Both Apache and IIS server not found")
