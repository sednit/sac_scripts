import xml.etree.ElementTree as ET


from ..common import bootstrap
from ..common.errors import SACError
from ..common.logger import global_logger
from ..utils.cmd import run_cmd


## Run appcmd.
# @param args Argument list for appcmd utility
# @return CompletedProcess object.
def run_appcmd(args):
    args = ["C:\\Windows\\System32\\inetsrv\\appcmd.exe", ] + args
    try:
        return run_cmd(args)
    except Exception as e:
        if isinstance(e, OSError) or \
           (isinstance(e, SACError) and \
            e.str_code == "OS_ERROR"):
            # if nothing found, raise an exception, which will be caught by
            # outer function
            raise SACError("SERVICE_ERROR", "IIS not found")
        else:
            raise


## Get list of site names from IIS.
# @return List of names.
def get_sites():
    res = run_appcmd(["list", "site", "/xml"])
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
                       args=res.args)
    # parse XML response from string
    root = ET.fromstring(res.stdout)
    # extract names of sites from XML
    return [i.attrib["SITE.NAME"] for i in root]


## Get list of virtual directories names from IIS.
# @return List of names.
def get_vdirs():
    res = run_appcmd(["list", "vdirs", "/xml"])
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
        )
    # parse XML response from string
    root = ET.fromstring(res.stdout)
    # extract names of sites from XML
    return [i.attrib["VDIR.NAME"] for i in root]


## Get list of virtual directories names from IIS.
# @return List of names.
def get_apps():
    res = run_appcmd(["list", "app", "/xml"])
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
                       args=res.args)
    # parse XML response from string
    root = ET.fromstring(res.stdout)
    # extract names of sites from XML
    return [i.attrib["APP.NAME"] for i in root]


## Get list of handler mappings for site.
# @param site Site name. If empty, search in server root config.
# @return List of XML nodes.
def get_handlers(path=""):
    args = ["list", "config", "/section:handlers"]
    if path != "":
        args.append("{}".format(path))
    res = run_appcmd(args)
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
                       args=res.args)
    # parse XML response from string
    root = ET.fromstring(res.stdout)
    return [i for i in root[0]]


## Is handler mapping is for wsisapi.
# @param handler_entry XML node for handler mapping.
# @return True, if handler map wsisapi.dll, False otherwise.
def is_wsisapi(handler_entry):
    return "scriptProcessor" in handler_entry.attrib and "wsisapi.dll" \
        in handler_entry.attrib["scriptProcessor"].lower()


## Get list of all registered ISAPI handlers' restrictions.
# @return List of XML nodes.
def get_list_of_registered_isapi_handlers():
    res = run_appcmd([
        "list", "config",
        "/section:system.webServer/security/isapiCgiRestriction", "/xml"
    ])
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
                       args=res.args)
    # parse XML response from string
    root = ET.fromstring(res.stdout)
    isapi_entries = []
    for i in root.getchildren()[0].getchildren()[0].getchildren():
        try:
            # check that necessary attributes exists and add entry to list then
            i.attrib["path"]
            i.attrib["allowed"]
            isapi_entries.append(i)
        except:
            pass
    return isapi_entries


## Register new ISAPI handler restriction.
# @path Path to ISAPI handler.
def register_new_isapi_restriction(path):
    register_string = "[path='{}',allowed='true']".format(path)
    res = run_appcmd([
        "set", "config",
        "/section:system.webServer/security/isapiCgiRestriction",
        "/+{}".format(register_string), "/commit:apphost"
    ])
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
                       args=res.args)


## Enable existing ISAPI handler restriction.
# @path Path to ISAPI handler.
def enable_isapi_restriction(path):
    register_string = "[path='{}'].allowed:true".format(path)
    res = run_appcmd([
        "set", "config",
        "/section:system.webServer/security/isapiCgiRestriction",
        "/{}".format(register_string), "/commit:apphost"
    ])
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR",
                       returncode=res.returncode,
                       stdout=bootstrap.try_decode(res.stdout),
                       stderr=bootstrap.try_decode(res.stderr),
                       args=res.args)


## Enable ISAPI handler restriction or create it, if doesn't exist.
# @path Path to ISAPI handler.
def enable_or_create_isapi_restriction(path):
    for i in get_list_of_registered_isapi_handlers():
        if i.attrib["path"] == path:
            enable_isapi_restriction(path)
            return
    register_new_isapi_restriction(path)


## Replace handler mapping paths.
# @param old_path Path, which is used as filter.
# @param new_path New path value.
# @param site Site name. If empty, search in server root config.
def replace_1c_handler_path(old_path, new_path, obj_path=""):
    args = ["set", "config", "/section:handlers", "/[scriptProcessor='{}']"
            ".scriptProcessor:{}".format(old_path, new_path)]
    # if obj_path is set, append its name and /commit:apphost to args
    if obj_path != "":
        args += [obj_path, ]# "/commit:apphost"]
    res = run_appcmd(args)
    if res.returncode != 0:
        raise SACError("CMD_RESULT_ERROR", args=args,
                       returncode=res.returncode,
                       stdout=res.stdout, stderr=res.stderr)
    global_logger.info(message="Updated info about 1C:Enterprise web extension",
                       iis_obj_path=obj_path,
                       old_path=old_path,
                       new_path=new_path)


def replace_handler_in_all_objects(new_path, test):
    replaced = 0
    for obj in list(set(
            [i.strip("/") for i in get_sites() + get_vdirs() + get_apps()]
    )):
        for handler in get_handlers(obj):
            if is_wsisapi(handler):
                if not test:
                    enable_or_create_isapi_restriction(new_path)
                    old_path = handler.attrib["scriptProcessor"]
                    replace_1c_handler_path(old_path, new_path, obj)
                replaced += 1
    return replaced


def replace_handler_in_specific_entry(new_path, entry_name, test):
    # 1. select necessary entry
    current_obj = None
    current_obj_index = None
    current_handler = None
    for obj in list(set(
            [i.strip("/") for i in get_sites() + get_vdirs() + get_apps()]
    )):
        for handler in get_handlers(obj):
            if is_wsisapi(handler):
                try:
                    split = obj.strip("/").split("/")
                    if split[-1] == entry_name:
                        entry_index = len(split) - 1
                        if current_obj_index is None \
                           or entry_index < current_obj_index:
                            current_obj = obj
                            current_handler = handler
                            current_obj_index = entry_index
                except:
                    pass
    if current_handler is None:
        raise SACError(
            "LOGIC_ERROR",
            "Cannot find wsisapi.dll handlers with specified app name"
        )
    # 2. replace
    if not test:
        enable_or_create_isapi_restriction(new_path)
        old_path = current_handler.attrib["scriptProcessor"]
        replace_1c_handler_path(old_path, new_path, current_obj)
