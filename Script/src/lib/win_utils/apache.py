import subprocess as sp
import shlex
import re
import os
from functools import reduce


from ..utils.apache import detect_apache_str_param, get_apache_version, \
    replace_path_in_load_module_string
from ..utils.fs import split_path
from ..common.errors import SACError
from ..common.logger import global_logger
from .service import execute_wmic_get_command


## Find all unique apache executables.
# @return List of paths.
def find_all_apache_executables():
    executables = []
    # find apache services
    for entry in execute_wmic_get_command(
            "service", "Name like '%apache%' OR Name like '%httpd%'",
            "Name,PathName", True
    ):
        executables.append(os.path.realpath(os.path.abspath(
            re.search(r"\"?(.*\.exe).*", entry["PathName"]).groups()[0]
        )))
    # if nothing found, raise an exception, which will be caught by
    # outer function
    if len(executables) < 1:
        raise SACError("SERVICE_ERROR",
                                     "Apache not found")
    # remove duplicated entries and convert result back to list
    return list(set(executables))


## Get full path to apache root.
# @param apache_executable path to apache executable.
# @return Path.
def get_apache_full_root(apache_executable):
    exe = apache_executable
    httpd_root = detect_apache_str_param(exe, "HTTPD_ROOT")
    # remove trailing slash, if presented
    httpd_root = httpd_root[1:] if httpd_root[0] in ["/", "\\"] else httpd_root
    # split paths into parts
    httpd_root = split_path(httpd_root)
    spl_exe = split_path(exe)
    # find index of apache root folder in split path. First try to find
    # "HTTPD_ROOT" in split path. If not found, find bin folder in split path
    try:
        index_of_apache_root_folder = spl_exe.index(httpd_root[0]) + 1
    except:
        index_of_apache_root_folder = spl_exe.index("bin")
    return os.path.join(*spl_exe[0:index_of_apache_root_folder])


## Get path to apache main config.
# @param apache_executable path to apache executable.
# @return Path.
def get_apache_config_path(apache_executable):
    httpd_root = get_apache_full_root(apache_executable)
    return os.path.normpath(os.path.join(
        httpd_root,
        detect_apache_str_param(apache_executable, "SERVER_CONFIG_FILE")
    ))


## Find and replace ws extensions paths in apache configurations.
# @param exe Apache executable.
# @param new_path new path values.
# @return True, if something replaced, False otherwise.
def replace_ws_ext_in_apache_config(exe, new_path, test):
    # find and read config
    config = get_apache_config_path(exe)
    cfg = open(config, "r")
    content = cfg.read().split("\n")
    content_copy = content.copy()
    cfg.close()
    # build full_new_path
    names = {
        "20": "wsapch2.dll",
        "22": "wsap22.dll",
        "24": "wsap24.dll",
    }
    new_path = os.path.join(new_path,
                            names["".join(get_apache_version(exe)[0:2])])
    # find and replace
    replaced = False
    for i in range(0, len(content)):
        if "_1cws_module" in content[i] and \
           content[i].strip(" \n\r\t")[0] != "#":
            content[i] = replace_path_in_load_module_string(content[i],
                                                            new_path)
            replaced = True
    # if nothing replaced, return None
    if not replaced:
        return False
    # if test set, return without actual changing in config
    if test:
        return True
    # try to make backup
    try:
        h, t = os.path.split(config)
        cfg_backup = open(os.path.join(h, "backup_" + t), "w")
        cfg_backup.write("\n".join(content_copy))
        cfg_backup.close()
    except:
        raise SACError("INSTALL_ERROR", "Unable to create backup"
                       "of Apache config file", apache_config_path=config)
    # write config back
    cfg = open(config, "w")
    cfg.write("\n".join(content))
    cfg.close()
    global_logger.info(message="Updated info about 1C:Enterprise web extension",
                       apache_config_path=config,
                       apache_version=".".join(get_apache_version(exe)),
                       new_path=new_path)
    return True


## Find all apache installations and replace web-extension data.
# @param new_path New path to web-extension.
# @param test Test mode flag.
def replace_ws_ext_in_all_apaches(new_path, test):
    # OR all results
    return reduce(
        lambda acc, x: acc or replace_ws_ext_in_apache_config(x, new_path,
                                                              test),
        find_all_apache_executables(),
        False
    )
