# coding: utf-8

import time
import shutil
import os


from ..common.config import StrPathExpanded
from ..common.errors import *
from .win_utils import *
from .permissions import *
from .service import *
from ..utils import *


class Platform1CRemover:

    def __init__(self, config):
        self.config = config

    @staticmethod
    def _remove_copy(path):
        global_logger.debug(message="Old version installed via copy",
                                path=path)
        # first: unregister dlls
        try:
            regsvr_unregister_dll(os.path.join(path, "comcntr.dll"))
        except:
             pass
        try:
            regsvr_unregister_dll(os.path.join(path, "radmin.dll"))
        except:
             pass
        # second: remove folder
        try:
            if os.path.exists(path):
                shutil.rmtree(path, ignore_errors=True)
            if not os.path.exists(path):
                os.makedirs(path, exist_ok=True)
        except:
            return False
        return True

    @staticmethod
    def _remove_setup(name):
        global_logger.debug(message="Old version installed via setup",
                                name=name)
        res = run_cmd("wmic product where \"Name='{}'\" call uninstall"
                      .format(name), shell=True)
        if len(res.stderr.strip(b"\r\n ")) == 0 and res.returncode == 0:
            return True
        else:
            return False

    ## Find installations in Program and Features.
    # @param version Version of 1C:Enterprise installation. If empty string
    #  (default), doesn't filter by version.
    # @param setup_folder Folder, where 1C:Enterprise installed. If empty
    #  string, doesn't filter by setup_folder.
    # @return list of ("setup", <name>) tuples. First element of tuple indicates
    #  install type.
    @staticmethod
    def _find_installation_in_paf(version=PlatformVersion(""),
                                  setup_folder=StrPathExpanded("")):
        # retrieving list of installed 1C:Enterprise versions
        result = execute_wmic_get_command(
            "product",
            "(Name like '%1C:%' OR Name like '%1С:%')"
            " AND NOT Name like '%Ring%' AND NOT Name like '%License%'",
            "Name,InstallLocation,Version",
            True
        )
        # convert Version property to PlatformVersion object and InstallLocation
        # to StrPathExpanded
        for i in result:
            i["Version"] = PlatformVersion(i["Version"])
            i["InstallLocation"] = StrPathExpanded(i["InstallLocation"])
        # filter results, if necessary
        if version != PlatformVersion(""):
            result = [i for i in result if i["Version"] == version]
        if setup_folder != StrPathExpanded(""):
            temp = []
            for i in result:
                if i["InstallLocation"] == setup_folder:
                    temp.append(i)
                    continue
                # if InstallLocation doesn't match exactly, but overlaps with
                # setup_folder, raise an error
                if i["InstallLocation"] != setup_folder and \
                   overlaps(i["InstallLocation"], setup_folder):
                    raise SACError(
                        "UNINSTALL_ERROR",
                        "InstallLocation doesn't match exactly, but there is "
                        "version, which installed in parent or child folder",
                        expected=setup_folder,
                        actual=i["InstallLocation"]
                    )
            result = temp
        return [("setup", i["Name"]) for i in result]

    ## Find installation in folder.
    # @param setup_folder Directory, where 1C:Enterprise installed.
    @staticmethod
    def _find_installation_in_folder(setup_folder, version=PlatformVersion("")):
        found_version = None
        files = ["1cv8.exe", "1cv8c.exe", "1cv8s.exe", "ragent.exe"]
        # if setup folder not exist, return nothing
        if not os.path.exists(setup_folder):
            return []
        for f in files:
            if f in [i.lower() for i in os.listdir(setup_folder)]:
                found_version = PlatformVersion(get_exe_version(os.path.join(
                    setup_folder, f
                )))
        # if nothing found, return empty list
        if found_version is None:
            return []
        if version != PlatformVersion(""):
            if found_version != version:
                raise SACError(
                    "UNINSTALL_ERROR",
                    "old version doesn't match one in configuration",
                    actual=found_version,
                    expected=version,
                    folder=setup_folder
                )
        return [("copy", setup_folder), ]

    ## Find installations.
    # @param self Pointer to object.
    # @return List of tuples (installation type (setup or copy),
    #  argument (path or name)).
    def _find_installations(self):
        if self.config["version"] == "" and \
           self.config["setup-folder"] == StrPathExpanded(""):
            raise SACError("ARGS_ERROR",
                                         "Version nor setup-folder specified")
        found_versions = self._find_installation_in_paf(
            self.config["version"],
            self.config["setup-folder"]
        )
        if self.config["setup-folder"] != StrPathExpanded(""):
            temp_found_versions = self._find_installation_in_folder(
                self.config["setup-folder"],
                self.config["version"]
            )
            if len(temp_found_versions) < 1 and \
               os.path.exists(os.path.join(self.config["setup-folder"], "bin")):
                temp_found_versions = self._find_installation_in_folder(
                    os.path.join(self.config["setup-folder"], "bin"),
                    self.config["version"]
                )
            found_versions += temp_found_versions
        if self.config["version"] != "" and len(found_versions) == 0:
            raise SACError(
                "UNINSTALL_ERROR",
                "no 1C:Enterprise installations found but 'version' specified"
            )
        return found_versions

    def uninstall_old(self):
        for itype, arg in self._find_installations():
            if itype == "setup":
                self._remove_setup(arg)
            elif itype == "copy":
                self._remove_copy(arg)
