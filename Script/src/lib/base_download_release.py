# coding: utf-8

import re
import shutil
import os
import cgi
import uuid
import platform
from urllib.parse import urlparse
from robobrowser import RoboBrowser


from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from lib.common.logger import global_logger, LogFunc
from lib.common.config import StrPathExpanded
from lib.utils import replace_similar_symbols, detect_actual_os_type, \
    is_64bit_arch, unpack_archive
from lib.utils.cmd import run_cmd
from lib.utils.fs import copy_file_or_directory


## Return tag content as string.
# @param tag Tag object.
# @return Tag string representation.
def str_tag(tag):
    return "".join([str(i) for i in tag.contents])


## Find tags on HTML page.
# @param browser RoboBrowser object with opened page, where search should be
#  performed.
# @param tag_type Tag type (dv, a, p, etc).
# @param regex_filter String with regex, which will be used as first filter.
# @param func_filter Function func(str) -> bool, which will be used as an
#  additional filter.
# @return List with tag objects.
def find_tags_by_content(browser, tag_type, regex_filter=None,
                         func_filter=None):
    # find all tags with tag_type
    tags = browser.find_all(tag_type)
    # filter tags with regex
    if regex_filter is not None:
        # replace similar symbols in regex
        regex_filter = replace_similar_symbols(regex_filter)
        global_logger.debug(message="Filtering tags by regex",
                            regex=regex_filter)
        tags = [i for i in tags \
                if re.search(regex_filter, str_tag(i)) is not None]
    # filter tags with function
    if func_filter is not None:
        tags = [i for i in tags \
                if func_filter(str_tag(i))]
    return tags


## Find tag with link. If more than one tag are found, the first
#  found only will be returned.
# @param browser RoboBrowser object with opened page, where search should be
#  performed.
# @param tag_type Tag type (dv, a, p, etc).
# @param regex_filter String with regex, which will be used as first filter.
# @param func_filter Function func(str) -> bool, which will be used as an
#  additional filter.
# @return URL.
# @exception SACError("BROWSER_ERROR") Raised if no tag found.
def find_link(browser, tag_type, regex_filter=None, func_filter=None):
    a = find_tags_by_content(browser, tag_type, regex_filter,
                             func_filter)
    if len(a) < 1:
        raise SACError(
            "BROWSER_ERROR", "cannot find tag",
            tag_type=tag_type, regex_filter=regex_filter,
            func_filter=func_filter.__name__ if func_filter else None,
            current_url=browser.url
        )
    global_logger.debug(message="Found link", href=a[0]["href"])
    return a[0]


## Find href inside tag and follow it. If more than one tag are found, the first
#  founded only will be used.
# @param browser RoboBrowser object with opened page where search should be
#  performed.
# @param tag_type Tag type (div, a, p, etc).
# @param regex_filter String with regex, which will be used as the first filter.
# @param func_filter Function func(str) -> bool, which will be used as an
#  additional filter.
# @return URL.
# @exception SACError("BROWSER_ERROR") Raised if no tag found.
def find_link_and_go(browser, tag_type, regex_filter=None, func_filter=None):
    link = find_link(browser, tag_type, regex_filter, func_filter)
    browser.follow_link(link)


## Download file from url and session.
# @param session Session object. This session might be used for download files
#  from site, which require login or some specific cookies.
# @param url Source URL.
# @param dst Destination folder.
# @param extract_filename_from_response If True, file name will be extracted
#  from response header. If extraction fails, file name will be random UUID.
#  If False, dst_filename arg used as file name (and UUID, if dst_filename not
#  set).
# @param dst_filename File name, which will be used if
#  extract_filename_from_response is False.
# @return Path to downloaded file.
def download_file_from_url_session(session, url, dst,
                                   extract_filename_from_response=True,
                                   dst_filename=None):
    l = LogFunc(message="Downloading file", src=url, dst=dst)
    response = session.get(url, stream=True)
    # build dst full path
    # if extract_filename_from_response is True, try to extract file
    # name from response. If fail, set it to random UUID.
    if not response.ok:
        raise SACError(
            "URL_ERROR", "Link returned unsuccessful code", url=url,
            response_code=response.status_code
        )
    if extract_filename_from_response:
        name = get_filename_from_response(response)
        if name is None:
            name = str(uuid.uuid4())
    # if not, get file name from dst_filename argument. If it is None,
    # set file name to random UUID.
    else:
        name = dst_filename if dst_filename is not None else str(uuid.uuid4)
    file_path = os.path.join(dst, name)
    # perform copy
    with open(file_path, "wb") as f:
        shutil.copyfileobj(response.raw, f)
    return file_path


## Extract filename filled from response headers.
# @param response Response object.
# @return String with file name or None, if filename not presented.
def get_filename_from_response(response):
    if "Content-Disposition" in response.headers:
        header = response.headers["Content-Disposition"]
        value, params = cgi.parse_header(header)
        if "filename" in params:
            return params["filename"]
    try:
        return os.path.basename(urlparse(response.url).path)
    except:
        raise SACError(
            "URL_ERROR", "Cannot detect file name from URL", url=response.url
        )


## Proceed to page with product's specific version. If version is string with
#  "current" value, then browser will proceed to page with last version.
# @param browser Browser object.
# @param product_name_regex Regexp, which is used for product search.
# @param version PlatformVersion object or string. Can have specific string
#  value "current", which means proceed to the last version of the product.
def go_to_product_version_page(browser, product_name_regex, version):
    link_tag = find_link(browser, "a", product_name_regex)
    try:
        # if version set to "current", get to last version
        if version == "current":
            row_tag = link_tag.parent.parent
            td = row_tag.find("td", class_="actualVersionColumn")
            a = td.find("a")
            browser.follow_link(a)
            return a.contents[0]
        # otherwise, get to specific version
        else:
            find_link_and_go(browser, "a", product_name_regex)
    except:
        raise SACError(
            "LOGIC_ERROR",
            "Cannot find specified product. Make sure that this product is "
            "available for you.",
            product=product_name_regex.strip("^$").replace("\\.", "."),
            version=str(version),
        )
    try:
        find_link_and_go(browser, "a", "{}".format(version))
        return version
    except:
        raise SACError(
            "LOGIC_ERROR",
            "Cannot find version of product",
            product=product_name_regex.strip("^$").replace("\\.", "."),
            version=str(version),
        )


## Find first subdirectory with real files.
# @param path Path where to find.
# @return Path to subfolder or None, if none found.
def find_subdir_with_files(path):
    for entry in os.listdir(path):
        if os.path.isfile(os.path.join(path, entry)):
            return path
        else:
            res = find_subdir_with_files(os.path.join(path, entry))
            if res is not None:
                return res
    return None


## Save platform distr to file structure.
# @param source Source location.
# @param download_root Destination location.
# @param tmp Temporary location.
# @param version Platform version.
# @param os_type Operating system type.
# @param arch Architecture (64, 32).
# @param distr_type Distr type (full, client, server).
def save_platform(source, download_root, tmp, version, os_type, arch,
                  distr_type):
    dst = os.path.join(download_root, "platform", str(version), os_type,
                       str(arch), distr_type)
    tmp = os.path.join(tmp, "unpacked")
    if os.path.exists(dst):
        shutil.rmtree(dst)
    if os.path.exists(tmp):
        shutil.rmtree(tmp)
    # if source is dir, perform copy
    if os.path.isdir(source):
        copy_file_or_directory(find_subdir_with_files(source), dst, False)
    # otherwise perform unpack
    else:
        unpack_archive(source, tmp)
        copy_file_or_directory(find_subdir_with_files(tmp), dst, False)
        shutil.rmtree(tmp)


## Save PostgreSQL distr to file structure.
# @param source Source location.
# @param download_root Destination location.
# @param tmp Temporary location.
# @param version Platform version.
# @param os_type Operating system type.
# @param arch Architecture (64, 32).
def save_postgres(source, download_root, tmp, version, os_type, arch):
    dst = os.path.join(download_root, "postgres", str(version), os_type,
                       str(arch))
    tmp = os.path.join(tmp, "unpacked")
    if os.path.exists(dst):
        shutil.rmtree(dst)
    if os.path.exists(tmp):
        shutil.rmtree(tmp)
    # if source is dir, perform copy
    if os.path.isdir(source):
        copy_file_or_directory(find_subdir_with_files(source), dst, False)
    # otherwise perform unpack
    else:
        unpack_archive(source, tmp)
        copy_file_or_directory(find_subdir_with_files(tmp), dst, False)
        shutil.rmtree(tmp)


## Save distr directly to destination folder.
# @param source Source location.
# @param dst Destination location.
# @param tmp Temporary location.
def save_accurately(src, dst, tmp):
    tmp = os.path.join(tmp, "unpacked")
    if os.path.exists(dst):
        shutil.rmtree(dst)
    if os.path.exists(tmp):
        shutil.rmtree(tmp)
    # if source is dir, perform copy
    if os.path.isdir(src):
        copy_file_or_directory(find_subdir_with_files(src), dst, False)
    # otherwise perform unpack
    else:
        unpack_archive(src, tmp)
        copy_file_or_directory(find_subdir_with_files(tmp), dst, False)
        shutil.rmtree(tmp)


## Start command, that required GUI. On Linux if failed, try to run via Xvfb.
# @param args Command arguments.
def run_config_unpack(args):
    if detect_actual_os_type() == "Windows":
        res = run_cmd(args)
        if res.returncode != 0:
            raise SACError(
                "CMD_RESULT_ERROR",
                returncode=res.returncode,
                stdout=bootstrap.try_decode(res.stdout),
                stderr=bootstrap.try_decode(res.stderr),
                args=args
            )
    else:
        res = run_cmd(args)
        # if command failed and specific stderr detected, try to start command
        # via xvfb
        if res.returncode != 0:
            try:
                xvfb = XvfbWrapper()
                res_ = run_cmd(args)
                if res_.returncode != 0:
                    raise Exception
                return
            except:
                pass
        if res.returncode != 0:
            raise SACError(
                "CMD_RESULT_ERROR",
                returncode=res.returncode,
                stdout=bootstrap.try_decode(res.stdout),
                stderr=bootstrap.try_decode(res.stderr),
                args=args
            )


## Find and get EFD extractor for Windows - setup.exe.
# @param dir_ Possible location of setup.exe.
# @return Actual path.
def find_and_get_setup_win(dir_):
    setup_exe = os.path.join(dir_, "setup.exe")
    # if setup.exe doesn't exists in specified location,
    # try to return file supplied with scripts
    if not os.path.exists(setup_exe):
        copy_file_or_directory(os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "..", "..",
            "external_utils", "efd_utils", "setup.exe"
        ), dir_)
    return setup_exe


## Find and get EFD extractor for Linux - setup (.x86_64 or .i386).
# @param dir_ Possible location of setup.
# @return Actual path.
def find_and_get_setup_linux(dir_):
    setup_packed = os.path.join(dir_, "setup")
    # if setup.exe doesn't exists in specified location,
    # try to return file supplied with scripts
    if not os.path.exists(setup_packed):
        setup_packed = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "..", "..",
            "external_utils", "efd_utils", "setup"
        )
    # now extract real executables from self-packed archive
    setup_unpack_args = ["bash", "-c", setup_packed +
                         " --noexec --keep --target " + dir_]
    if run_cmd(setup_unpack_args).returncode != 0:
        raise Exception
    # depends on architecture, return path
    return os.path.join(dir_, "setup." + \
                        ("x86_64" if is_64bit_arch() else "i386"))


## Deduce configuration parameters (name, version, type).
# @param path Source location.
# @return Tuple (path, name, version, type)
def deduce_configuration_params(path):
    suppl_dir = os.path.join(path, os.listdir(path)[0])
    conf_latin_name = os.listdir(suppl_dir)[0]
    version = os.listdir(os.path.join(suppl_dir, conf_latin_name))[0]\
                .replace("_", ".")
    distr_type = None
    for file_ in os.listdir(os.path.join(suppl_dir, conf_latin_name,
                                         version.replace(".", "_"))):
        if ".cfu" in file_:
            distr_type = "update"
            break
        elif ".cf" in file_:
            distr_type = "full"
            break
        else:
            pass
    if distr_type is None:
        return None
    return find_subdir_with_files(path), conf_latin_name, version, distr_type


## Unpack and copy configuration distr on Windows.
# @param setup_exe Path to EFD extractor.
# @param dst Destination location.
# @param with_dir_structure Create folders structure.
def unpack_and_copy_win(setup_exe, dst, with_dir_structure):
    # save original tmplts locations
    tmplts_dir = os.path.expandvars("%APPDATA%\\1C\\1cv8")
    tmplts2_dir = os.path.expandvars("%APPDATA%\\1C\\1cv82")
    tmplts_orig = os.path.join(tmplts_dir, "tmplts_orig")
    tmplts = os.path.join(tmplts_dir, "tmplts")
    tmplts2_orig = os.path.join(tmplts2_dir, "tmplts_orig")
    tmplts2 = os.path.join(tmplts2_dir, "tmplts")
    renamed = False
    renamed2 = False
    # try to move tmplts folders
    try:
        os.rename(tmplts, tmplts_orig)
        renamed = True
    except FileNotFoundError:
        pass
    try:
        os.rename(tmplts2, tmplts2_orig)
        renamed2 = True
    except FileNotFoundError:
        pass
    # perform unpack
    run_config_unpack([setup_exe, "/s"])
    if os.path.exists(tmplts):
        created_tmplts = tmplts
    elif os.path.exists(tmplts2):
        created_tmplts = tmplts2
    else:
        raise Exception
    # copy distr to destination
    if with_dir_structure:
        copy_file_or_directory(created_tmplts, dst, False)
    else:
        copy_file_or_directory(find_subdir_with_files(created_tmplts), dst,
                               False)
    shutil.rmtree(created_tmplts)
    # restore tmplts folders
    if renamed:
        os.rename(tmplts_orig, tmplts)
    if renamed2:
        os.rename(tmplts2_orig, tmplts2)


## Unpack and copy configuration distr on Linux.
# @param setup_exe Path to EFD extractor.
# @param dst Destination location.
# @param with_dir_structure Create folders structure.
def unpack_and_copy_linux(setup_exe, dst, with_dir_structure):
    # save original tmplts location
    tmplts_dir = os.path.expanduser(
        os.path.expandvars("~/.1cv8/1C/1cv8")
    )
    tmplts_orig = os.path.join(tmplts_dir, "tmplts_orig")
    tmplts = os.path.join(tmplts_dir, "tmplts")
    renamed = False
    # try to move tmplts folders
    try:
        os.rename(tmplts, tmplts_orig)
        renamed = True
    except FileNotFoundError:
        pass
    # perform unpack
    run_config_unpack([setup_exe, "/s"])
    # copy distr to destination
    if with_dir_structure:
        copy_file_or_directory(tmplts, dst, False)
    else:
        copy_file_or_directory(find_subdir_with_files(tmplts), dst,
                               False)
    shutil.rmtree(tmplts)
    # restore tmplts folders
    if renamed:
        os.rename(tmplts_orig, tmplts)


## Extract configuration to destination location.
# @param src Source location.
# @param dst Destination location.
# @param tmp Temporary location.
# @param with_dir_structure Create folders structure.
def extract_1c_conf(src, dst, tmp, with_dir_structure=False):
    tmp = os.path.join(tmp, "1c_conf_setup")
    unpack_archive(src, tmp)
    try:
        if detect_actual_os_type() == "Windows":
            setup_exe = find_and_get_setup_win(tmp)
            unpack_and_copy_win(setup_exe, dst, with_dir_structure)
        else:
            # if OS is Linux, add execute bit to setup executables
            run_cmd(["chmod", "+x", "-R", tmp])
            setup_exe = find_and_get_setup_linux(tmp)
            unpack_and_copy_linux(setup_exe, dst, with_dir_structure)
        shutil.rmtree(tmp)
    except:
        raise


## Save configuration distr to file structure.
# @param source Source location.
# @param download_root Destination location.
# @param tmp Temporary location.
# @param version Platform version.
# @param distr_type Distr type (full or update).
def save_configuration(source, download_root, tmp, version=None,
                       distr_type=None):
    unpack_tmp = os.path.join(tmp, "unpacked")
    if os.path.exists(unpack_tmp):
        shutil.rmtree(unpack_tmp)
    # extract distr and deduce its parameters
    if os.path.isdir(source):
        params = deduce_configuration_params(source)
    else:
        extract_1c_conf(source, unpack_tmp, tmp, True)
        params = deduce_configuration_params(unpack_tmp)
    if params is None:
        shutil.rmtree(unpack_tmp)
        raise SACError(
            "LOGIC_ERROR", "Cannot determine parameters of the configuration"
        )
    # build destination path and save configuration
    dst = os.path.join(download_root, "1c-confs", params[1],
                       version if version is not None else params[2],
                       distr_type if distr_type is not None else params[3])
    if os.path.exists(dst):
        shutil.rmtree(dst)
    copy_file_or_directory(find_subdir_with_files(params[0]), dst, False)
    if os.path.exists(unpack_tmp):
        shutil.rmtree(unpack_tmp)


## Save configuration distr directly to destination folder.
# @param source Source location.
# @param dst Destination location.
# @param tmp Temporary location.
def save_configuration_accurately(src, dst, tmp):
    if os.path.exists(dst):
        shutil.rmtree(dst)
    if os.path.isdir(src):
        copy_file_or_directory(find_subdir_with_files(src), dst, False)
    # if comes as archive, extract it
    else:
        extract_1c_conf(src, dst, tmp)
        shutil.rmtree(tmp)


## Check that specified URL returned correct code.
# @param browser Browser object.
# @param url URL.
def check_download_url(browser, url):
    # check that this url returns the correct code
    response = browser.session.get(url, stream=True)
    if not response.ok:
        raise SACError(
            "URL_ERROR", "response status code is not good",
            response_code=response.status_code,
            url=response.url
        )


## Base class for all scenarios, which is perform download from releases portal.
class BaseDownloadReleaseScenario(BaseScenario):

    def _validate_additional_data(self):
        pass

    def _validate_specific_data(self):
        validate_data = [
            ["download-folder", StrPathExpanded],
            ["username", str],
            ["password", str],
            ["use-eu-site", bool],
            ["save-accurately", bool],
        ]
        self.config.validate(validate_data)
        self._validate_additional_data()

    def _after_init(self):
        self.logged_in = False
        # setting up RoboBrowser object
        self.browser = RoboBrowser(history=True, parser="html.parser")
        nt_version = platform.win32_ver()[1].split(".")
        if nt_version[0] != "" and int(nt_version[0]) < 6:
            global_logger.warning(message="Detected Windows version XP or lower"
                                  ". SSL verification disabled due to known "
                                  "bug with it in urllib3.")
            self.browser.session.verify = False
            import urllib3
            urllib3.disable_warnings()
        # set releases URL
        self.portal_url = "https://releases.1c.ru" if not \
                          self.config["use-eu-site"] else \
                          "https://releases.1c.eu"
        # make tmp folder
        self.download_tmp_location = os.path.join(self.tmp, "downloaded")
        if os.path.exists(self.download_tmp_location):
            shutil.rmtree(self.download_tmp_location)
        os.makedirs(self.download_tmp_location)

    ## Log into releases.1c.ru portal.
    # @param self Pointer to object.
    def _login_to_portal(self):
        l = LogFunc(message="Logging to portal")
        # open portal
        self.browser.open(self.portal_url + "/total")
        # get login form
        form = self.browser.get_form(id="loginForm")
        if form is None:
            raise SACError(
                "URL_ERROR", "Cannot find login form. Maybe site structure was"\
                " changed, address is wrong or you already logged in. " \
                "Log out and try again. If you keep getting this error, report"\
                " to developer",
                url=self.browser.url
            )
        # fill form
        form["username"] = self.config["username"]
        form["password"] = self.config["password"]
        self.browser.submit_form(form)
        # check, if login is successful
        errors = self.browser.find_all(id="credential.errors")
        if len(errors) > 0:
            raise SACError("URL_ERROR", "Login failed",
                                         error=str_tag(errors[0]))
        self.logged_in = True

    def _get_url(self):
        return ""

    def _process_files(self, src):
        pass

    ## Execute scenario.
    # @param self Pointer to object.
    def _real(self):
        # log in
        if not self.logged_in:
            self._login_to_portal()
        self.downloaded_file = download_file_from_url_session(
            self.browser.session, self._get_url(), self.download_tmp_location
        )
        self._process_files(self.downloaded_file)
