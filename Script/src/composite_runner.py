import sys
import os
import subprocess as sp
import threading
import time
import platform
from datetime import datetime, timedelta
from io import StringIO
from enum import Enum
import copy
import shlex


from lib.common.const import ReturnCode
from lib.common import bootstrap
from lib.common import global_vars as gv
from lib.common.errors import SACError
from lib.common.logger import global_logger
from lib.common.config import ScenarioConfiguration, StrPathExpanded
from lib.utils import read_yaml, detect_actual_os_type, \
    PlatformVersion, try_open_file


TOP_LEVEL_SCENARIO_NAME = None
DICTIONARY = None
INTERPRETERS = None
GLOBAL_INTERRUPT_FLAG = False
TIME_LIMIT_FLAG = False
SLEEP_DELAY = 0.05


## Decorator which captures SACErrors, logs them and returns an
#  appropriate value.
def handle_SAC_errors(func):
    def wrapper(*args, **kwargs):
        res = 1
        try:
            res = func(*args, **kwargs)
        except SACError as err:
            if err.num_code != 0:
                global_logger.error(**err.to_dict())
            res = err.num_code
        except Exception as err:
            err = SACError("UNKNOWN", err)
            global_logger.error(**err.to_dict())
            res = err.num_code
        return res
    return wrapper


## Add debug values from global_vars to destination dictionary copy.
# @param dst Destination dictionary.
# @return Copy of `dst` with debug values.
def add_debug_values(dst):
    dst = dst.copy()
    for key in ["debug", "collapse-traceback", "print-begin",
                "print-uuid", "print-function", "escape-strings"]:
        dst[key] = getattr(gv, key.upper().replace("-", "_"))
    return dst


## Cross-platform version of killing processes tree. This function encapsulate
#  detection of OS and setting proper kill_process_tree function.
# @param pid PID of root process.
def kill_process_tree(pid):
    os_type = detect_actual_os_type()
    # decide which module will be used
    if os_type == "Windows":
        from lib import win_utils as os_module
    else:
        from lib import linux_utils as os_module
    pt = os_module.ProcessTree(pid)
    # if OS is Windows and lower than NT 6.x, terminate processes
    if os_type == "Windows":
        nt_version = platform.win32_ver()[1]
        if nt_version != "" and int(nt_version[0]) < 6:
            pt.kill(True)
            for i in range(0, 20):
                if pt.update() < 1:
                    return True
                time.sleep(0.01)
            return False
    # otherwise try to kill processes softly before forced termination
    pt.kill()
    for i in range(0, 20):
        if pt.update() < 1:
            return True
        time.sleep(0.01)
    # now terminate processes
    pt.kill(True)
    for i in range(0, 20):
        if pt.update() < 1:
            return True
        time.sleep(0.01)
    return False


class Interpreter:

    def __init__(self, rule):
        self.__rule = rule

    def wrap_data(self, script_name, config_name, parameters):
        parameters_strings = ["--{}={}".format(k, v) for k, v in
                              parameters.items()]
        parts = shlex.split(self.__rule)
        updated_parts = []
        for i in range(0, len(parts)):
            if "{script-name}" in parts[i]:
                updated_parts.append(parts[i].replace("{script-name}",
                                                      script_name))
            elif "{config-name}" in parts[i]:
                updated_parts.append(parts[i].replace("{config-name}",
                                                      config_name))
            elif "{cmdline-parameters-as-one}" in parts[i]:
                updated_parts.append(parts[i].replace(
                    "{cmdline-parameters-as-one}",
                    shlex.quote(" ".join(parameters_strings))
                ))
            elif "{cmdline-parameters}" in parts[i]:
                updated_parts += parameters_strings
            else:
                updated_parts.append(parts[i])
        return updated_parts

    def check(self):
        try:
            test_proc = sp.Popen(self.wrap_data("", "", {}), stdout=sp.PIPE,
                                 stderr=sp.PIPE)
            test_proc.terminate()
            test_proc.communicate()
        except FileNotFoundError:
            raise SACError(
                "ARGS_ERROR", "Interpreter doesn't exists",
                command=self.__rule
            )
        except PermissionError:
            raise SACError(
                "ARGS_ERROR", "Cannot access interpreter",
                command=self.__rule
            )

    @staticmethod
    def parse_interpreters_file(path):
        try_open_file(path)
        struct = read_yaml(path)
        result_struct = {}
        for key, interpreter_str in struct.items():
            interpreter = Interpreter(interpreter_str)
            result_struct[key] = interpreter
        return result_struct


## Class, which represent command dictionary entry.
class DictionaryEntry:

    ## Constructor.
    # @param self Pointer to object.
    # @param script Script name.
    # @param config Configuration file name.
    # @param interpreter Interpreter object.
    # @param first_version First version.
    # @param last_version Last version.
    # @param exclude_versions
    # @param script_prefix Prefix to script name (can be path).
    # @param config_prefix Prefix to configuration file name.
    # @param test_files Check, is files, specified in config-name and
    #  script-name, exists.
    def __init__(self, script, config, interpreter, first_version=None,
                 last_version=None, exclude_versions=[], script_prefix="",
                 config_prefix="", test_files=False):
        # if script is None, assume that this command should be executed
        # by composite scenario runner.
        #
        # if path to script is absolute, store it as is. Otherwise append
        # prefixes
        if script is not None and os.path.isabs(script):
            self.script = script
        else:
            self.script = os.path.join(script_prefix, StrPathExpanded(script)) \
                          if script is not None else None
        if config is not None and os.path.isabs(config):
            self.config = config
        else:
            self.config = os.path.join(config_prefix, StrPathExpanded(config)) \
                          if config is not None else None
        if test_files:
            self.check_files()
        self.first_version = PlatformVersion(first_version)
        self.last_version = PlatformVersion(last_version)
        self.exclude_versions = [PlatformVersion(entry)
                                 for entry in exclude_versions]
        self.interpreter = interpreter

    ## Create DictionaryEntry object from dict.
    # @param data Dictionary with data.
    # @param script_prefix Prefix to script name (can be path).
    # @param config_prefix Prefix to configuration file name.
    @staticmethod
    def from_dict(data, script_prefix="", config_prefix=""):
        if "first-version" not in data:
            data["first-version"] = None
        if "last-version" not in data:
            data["last-version"] = None
        if "exclude-versions" not in data:
            data["exclude-versions"] = []
        if "interpreter" not in data:
            data["interpreter"] = None
        if "config-name" not in data:
            data["config-name"] = None
        if "script-name" not in data:
            data["script-name"] = None
        try:
            if data["interpreter"] is None:
                interpreter = data["interpreter"]
            else:
                interpreter = INTERPRETERS[data["interpreter"]]
        except KeyError:
            raise SACError(
                "ARGS_ERROR", "Such interpreter doesn't exists",
                interpreter=data["interpreter"]
            )
        return DictionaryEntry(
            data["script-name"], data["config-name"], interpreter,
            data["first-version"], data["last-version"],
            data["exclude-versions"], script_prefix, config_prefix
        )

    ## String representation of object.
    # @param self Pointer to object.
    def __str__(self):
        s = "Script: {}, Config: {}".format(self.script, self.config)
        if self.first_version is not None:
            s += ", First version: {}".format(self.first_version)
        if self.last_version is not None:
            s += ", Last version: {}".format(self.last_version)
        if self.exclude_versions != []:
            s += ", Exclude versions: {}".format(
                ", ".join([str(i) for i in self.exclude_versions])
            )
        return s

    ## String representation of object.
    # @param self Pointer to object.
    def __repr__(self):
        return self.__str__()

    ## Check files existence and configuration correctness of configuration.
    # @param self Pointer to object.
    def check_files(self):
        if self.script is not None:
            try_open_file(self.script)
        try_open_file(self.config)


## Class, which represent command dictionary.
class CommandDictionary:
    ## Constructor.
    # @param self Pointer to object.
    # @param path If specified, object will be initialized with data from file.
    def __init__(self, path=None):
        self.data = {}
        if path:
            self.add_data_from_file(path)

    ## Add data from file.
    # @param self Pointer to object.
    # @param path Path, where dictionary stored.
    # @param script_prefix Prefix, which will be added to all script fields.
    #  If not specified, will be set to path to composite_runner.py.
    # @param config_prefix Prefix, which will be added to all config fileds.
    #  If not specified, will be set to path to `path`.
    def add_data_from_file(self, path, script_prefix=None, config_prefix=None):
        # read the dictionary file
        try:
            try_open_file(path)
        except:
            raise SACError(
                "ARGS_ERROR", "Cannot find or access dictionary file",
                path=StrPathExpanded(path)
            )
        file_data = read_yaml(path)
        # set prefixes
        script_prefix = os.path.dirname(os.path.realpath(__file__)) \
                        if script_prefix is None else script_prefix
        config_prefix = os.path.dirname(path) \
                        if config_prefix is None else config_prefix
        # iterate over data and build DictionaryEntry objects.
        for key, value in file_data.items():
            try:
                self.data[key] = DictionaryEntry.from_dict(value, script_prefix,
                                                           config_prefix)
            # if cannot build entry, raise and error
            except:
                raise SACError(
                    "ARGS_ERROR", "cannot build dictionary entry from supplied "
                    "data", key=key, value=value
                )

    ## Provide access to entries via key.
    # @param self Pointer to object.
    # @param key Name of the entry.
    def __getitem__(self, key):
        try:
            return self.data[key]
        except KeyError as err:
            raise SACError("ARGS_ERROR",
                                         "Command not found in dictionary",
                                         command=key)

    def __contains__(self, key):
        return key in self.data


## Enumeration with scenario execution modes (test, real, rollback).
class ExecuteMode(Enum):
    TEST = 0
    REAL = 1
    ROLLBACK = 2


## Enumeration with script states.
class ScriptState(Enum):
    NOT_STARTED = "not started"
    STARTED = "started"
    FINISHED = "finished"
    FAILED = "failed"
    INTERRUPTED = "interrupted"
    NOT_SET = "not set"


## Function which return dummy object with some of attributes as Script.
# @return Object with 'state', 'returncode', 'finished', 'started', 'command',
#  'name' properties and 'get_execution_history()' method.
def get_dummy_script():
    return type("DummyScript", (),
                {"state": ScriptState.NOT_SET,
                 "returncode": None,
                 "finished": True,
                 "started": True,
                 "command": None,
                 "name": None,
                 "get_execution_history": lambda self: {}})()


## Representation of python script from SAC. It is responsible for
#  extracting and validating data from config and command dictionary, starting
#  and controlling inner process execution. Also it restart script if necessary.
class Script:

    ## Constructor.
    # @param self Pointer to object.
    # @data Data which defines python script.
    def __init__(self, data):
        self.name = data["name"] if "name" in data else data["command"]
        self.command = data["command"]
        # extract paths to files from command dictionary
        self.script_path = DICTIONARY[data["command"]].script
        if self.script_path is None:
            raise SACError(
                "CONFIG_ERROR",
                "Command can be executed only by composite scenario executor "
                "itself and cannot be part of composite scenario",
                name=self.name, command=self.command
            )
        self.interpreter = DICTIONARY[data["command"]].interpreter
        self.interpreter.check()
        # save cmd_args
        self.cmd_args = data["scenario-data"] if "scenario-data" in data else {}
        self.cmd_args = add_debug_values(self.cmd_args)
        self.config_path = DICTIONARY[data["command"]].config
        try:
            temp_config = ScenarioConfiguration(read_yaml(self.config_path))
            temp_config.add_cmd_args(self.cmd_args, True)
        except:
            temp_config = None
        # set time variables
        if temp_config:
            try:
                self.time_limit = temp_config["time-limit"]
            except:
                self.time_limit = 0
            try:
                self.timeout = temp_config["timeout"]
            except:
                self.timeout = 0
            try:
                self.try_count = temp_config["try-count"]
            except:
                self.try_count = 1
        else:
            self.time_limit = 0
            self.timeout = 0
            self.try_count = 1
        # set inner state flags and variables
        self.__proc = None  # currently executed process
        # list of processes which was executed with script. Processes stored
        # in the order they run
        self.__procs_list = []
        self.__proc_started = False
        self.__proc_finished = False
        # it is currently a process in progress
        self.__in_progress = False
        # number of processes executed for this script
        self.__exec_counter = 0
        # time limit for current process
        self.__current_time_limit = None
        # the time when next process can be started
        self.__next_start_time = None
        # flag which indicate that last process was interrupted cuz time is out
        self.__last_process_interrupted_by_time_limit = False
        # flag which indicate that script shouldn't be restarted even if there
        # is a attempts left
        self.__dont_restart = False
        # flag which indicate that process was killed externally
        self.__killed = False
        # flag which indicate that script can be killed in progress without
        # consequences
        self.__interruptable = "interruptable" in data and data["interruptable"]

    ## Kill script. This kill currently executed process, its children, and
    #  poll currently executed process after exit.
    # @param self Pointer to object.
    def __kill(self):
        # if current proc is None, do nothing
        if self.__proc is not None:
            self.__in_progress = False
            kill_process_tree(self.__proc.pid)
            self.__proc.kill()
            self.__proc.poll()

    ## Create new process for script. Kill script, if already executed.
    # @param self Pointer to object.
    def __create_proc(self):
        # kill currently executed process
        self.__kill()
        temp_cmd_args = self.cmd_args.copy()
        if TOP_LEVEL_SCENARIO_NAME is not None:
            temp_cmd_args["composite-scenario-name"] = TOP_LEVEL_SCENARIO_NAME
        # if config is not None, add it
        args = self.interpreter.wrap_data(
            self.script_path if self.script_path else "",
            self.config_path if self.config_path else "",
            temp_cmd_args
        )
        # increase counter of executed process
        self.__exec_counter += 1
        # mark that process is in progress
        self.__in_progress = True
        # reset flag
        self.__last_process_interrupted_by_time_limit = False
        # set when process should be killed according to the timeout
        if self.time_limit > 0:
            self.__current_time_limit = datetime.now() \
                                        + timedelta(seconds=self.time_limit)
        else:
            self.__current_time_limit = None
        # creates process
        global_logger.debug(message="Starting script process", args=args)
        self.__proc = sp.Popen(args)
        # appends process to the list with processes
        self.__procs_list.append(self.__proc)

    ## Update state of script and its inner process.
    # @param self Pointer to object.
    # @return True if script finished, False otherwise.
    def __update_process_state(self):
        # raise an error if process is not started. Not SACError because
        # this should not normally happen.
        if not self.__proc_started:
            raise RuntimeError("Process not started")
        # poll currently executed process
        self.__proc.poll()
        if self.__proc.returncode is not None:
            # first, kill process and reset inner state to `not in progress`
            self.__kill()
            self.__in_progress = False
            # update next start time
            if self.__next_start_time is None:
                self.__next_start_time = datetime.now() \
                                         + timedelta(seconds=self.timeout)
            # if process has returned 0 and it has not been interrupted,
            # assume that it has finished successfully
            if self.__proc.returncode == 0 \
               and not self.__last_process_interrupted_by_time_limit:
                self.__proc_finished = True
                return True
            else:
                # if less than `try_count` have been executed, check that next `start time`
                # has come and process is allowed to restart and then start new process
                if self.__exec_counter < self.try_count \
                   and not self.__dont_restart:
                    if self.__next_start_time <= datetime.now():
                        self.__next_start_time = None
                        self.__create_proc()
                    return False
                else:
                    self.__proc_finished = True
                    return True
        # if current process has not finished
        else:
            # if time limit has exceeded, kill process
            if self.__current_time_limit is not None \
               and datetime.now() > self.__current_time_limit:
                self.__kill()
                self.__last_process_interrupted_by_time_limit = True
            return False

    ## Start script execution.
    # @param self Pointer to object.
    def start(self):
        global_logger.info(message="Starting Script. Info",
                           script=self.script_path, config=self.config_path,
                           cmd_args=self.cmd_args, name=self.name,
                           command=self.command, try_count=self.try_count,
                           timeout=self.timeout)
        if self.__proc_started:
            raise RuntimeError("Process already started")
        self.__proc_started = True
        self.__create_proc()
        self.__update_process_state()

    ## Kill script.
    # @param self Pointer to object.
    def kill(self):
        if not self.__proc_started:
            raise RuntimeError("Process not started")
        # set killed flag only if script has not finished
        if not self.__proc_finished:
            self.__killed = True
        self.__kill()
        self.__dont_restart = True
        self.__update_process_state()

    ## Kill script gently. This means that the script will end only if it is marked
    #  as interruptable.
    # @param self Pointer to object.
    def gentle_kill(self):
        if not self.__proc_started:
            raise RuntimeError("Process not started")
        if self.__interruptable:
            if not self.__proc_finished:
                self.__killed = True
            self.__kill()
            self.__dont_restart = True
            self.__update_process_state()

    ## Returns the code of the last finished process. If the process has been interrupted or has finished as
    #  the time was out, returns special codes.
    # @param self Pointer to object.
    # @return Code.
    @property
    def returncode(self):
        if self.__proc_finished:
            return self.__procs_list[-1].returncode
        else:
            return None

    @property
    def started(self):
        return self.__proc_started

    @property
    def finished(self):
        return self.__proc_finished

    @property
    def killed(self):
        return self.__killed

    ## Gets list of executed processes' pids and returns codes.
    # @param self Pointer to object.
    # @return List of pairs (pid, returncode).
    def get_execution_history(self):
        return [(proc.pid, proc.returncode) for proc in self.__procs_list]

    ## Gets state of the script.
    # @param self Pointer to object.
    # @return ScriptState value.
    @property
    def state(self):
        if not self.__proc_started:
            return ScriptState.NOT_STARTED
        else:
            if not self.__proc_finished:
                return ScriptState.STARTED
            else:
                if self.__killed:
                    return ScriptState.INTERRUPTED
                else:
                    if self.returncode == ReturnCode.SUCCESS.value:
                        return ScriptState.FINISHED
                    else:
                        return ScriptState.FAILED

    def is_alive(self):
        return self.__proc_started and not self.__proc_finished

    def poll(self):
        return self.__update_process_state()

    ## Wait for the script execution is over. If timeout is set and time is out,
    #  raise a TimeoutError exception. It is safe to catch this
    #  exception and try again to wait.
    # @param self Pointer to object.
    # @param timeout Number of seconds. If None, wait script end forever.
    def join(self, timeout=None):
        if timeout is not None:
            endtime = datetime.now() + timedelta(seconds=timeout)
        while True:
            self.poll()
            if self.is_alive() or self.__proc.returncode is None:
                if timeout is not None and datetime.now() > endtime:
                    raise TimeoutError
            else:
                return

    ## Build tuple with 3 Script objects which is refer to 3 execution
    #  modes of script: test mode, real mode, rollback.
    # @param cls Class.
    # @param data Data, which is define script.
    # @param forward_interruptable Allow forward step be interruptable.
    # @return Tuple (test_script, real_script, rollback_script). rollback_script
    #  can be None, if rollback not specified for this script.
    @classmethod
    def build_triple(cls, data, forward_interruptable=True,
                     restrict_tests=True):
        # fill test script data
        test_data = copy.deepcopy(data)
        if not forward_interruptable and "interruptable" in test_data:
            raise SACError(
                "CONFIG_ERROR", "step block can contain 'interruptable' key "
                "only in parallel block", name=test_data["name"]
            )
        test_data["scenario-data"]["test-mode"] = True
        test_data["scenario-data"]["restrict-tests"] = restrict_tests
        test = cls(test_data)
        # fill real script data
        real_data = copy.deepcopy(data)
        real_data["scenario-data"]["test-mode"] = False
        real_data["scenario-data"]["restrict-tests"] = False
        real = cls(real_data)
        # if no rollback in input data, return None as rollback script
        if "rollback" not in data:
            return test, real, None
        # otherwise fills rollback script data
        rollback_data = copy.deepcopy(data["rollback"])
        if "interruptable" in rollback_data:
            raise SACError(
                "CONFIG_ERROR", "rollback block can't contain 'interruptable' "
                "key", name=test_data["name"]
            )
        rollback_data["name"] = test.name
        rollback_data["scenario-data"]["test-mode"] = False
        rollback_data["scenario-data"]["restrict-tests"] = False
        return test, real, cls(rollback_data)


## Get column from list of lists (element with specified index from each list).
# @param data List of lists.
# @return List with elements.
def get_column(data, index):
    return [i[index] for i in data]


## Get list of scripts for rollback of thread. This function return only started
#  scripts with reversed order.
# @param List of tuples.
# @return List of scripts.
def get_rollback_list(flow_lst):
    return [i[2] for i in flow_lst if i[1].started and i[1].returncode
            not in [ReturnCode.ERROR_STATE_NOT_CHANGED.value, None] and
            i[2] is not None][::-1]


## Extract from all scripts those, which are essential for executing the specified mode.
# @param scripts Structure with scripts for all modes. This is 3-level list
#  (Stages -> Flows -> Steps), where each Step is tuple of 3 Script.
# @param mode ExecuteMode.
# @return 3-level list as 'scripts' argument, where Step is Script object.
def extract_mode_specific_scripts(scripts, mode):
    if mode == ExecuteMode.ROLLBACK:
        return [[get_rollback_list(flow) for flow in stage]
                for stage in scripts[::-1]]
    else:
        return [[get_column(flow, mode.value) for flow in stage]
                for stage in scripts]


## Convert raw composite scenario data to 3-level list
#  (Stages -> Flows -> Steps), where each Step is tuple of 3 Script.
# @param raw_data Raw composite scenario data.
# @return 3-level list (Stages -> Flows -> Steps), where each Step is tuple of 3
#  Script.
def convert_raw_scenario_data_to_scripts(raw_data,
                                         restrict_initial_tests=False):
    root = []
    for i in range(0, len(raw_data)):
        if "command" in raw_data[i]:
            root.append(
                [[Script.build_triple(
                    raw_data[i], False, i != 0 or restrict_initial_tests
                ), ], ]
            )
        else:
            flows = []
            for flow in raw_data[i]["parallel"]:
                if isinstance(flow, list):
                    flows.append(
                        [Script.build_triple(
                            flow[j], True, not (j == 0 and i == 0) or \
                            restrict_initial_tests
                        ) for j in range(0, len(flow))]
                    )
                else:
                    raise TypeError("parallel branch must be a list of steps")
            root.append(flows)
    return root


## Find last finished script in list of Scripts.
# @param List of Scripts.
# @return Index.
def last_finished_script_in_list(lst):
    for i in range(0, len(lst)):
        if lst[i].finished:
            if i == (len(lst) - 1) or not lst[i + 1].finished:
                return i
    return -1


## Poll script flow, which is represented as list of Scripts.
#  This mean find first not finished script in list,  start it, if necessary,
#  and poll it.
# @param lst List of Scripts.
# @return 0 if all scripted finished successful, any other number if any of
#  the scripts are finished with non-zero return code, None in all other situations.
def poll_script_list(lst):
    if len(lst) < 1:
        return 0
    # update each started script
    [i.poll() for i in lst if i.started]
    #
    last_finished_index = last_finished_script_in_list(lst)
    if last_finished_index >= 0:
        # if the last return code is not 0, return immediately
        if lst[last_finished_index].returncode != ReturnCode.SUCCESS.value:
            return lst[last_finished_index].returncode
        else:
            if GLOBAL_INTERRUPT_FLAG:
                return lst[last_finished_index].returncode
            # if last element, return last code
            if last_finished_index == (len(lst) - 1):
                return lst[last_finished_index].returncode
    # if none of conditions hit, start next script, if not started,
    # and return None
    if not lst[last_finished_index + 1].started:
        lst[last_finished_index + 1].start()
    return None


## Kill script flow, which is represented as list of Scripts.
#  This mean find last started script, kill it and poll whole list.
# @param lst List of Scripts.
def kill_script_list(lst):
    last_finished_index = last_finished_script_in_list(lst)
    if last_finished_index != (len(lst) - 1):
        if lst[last_finished_index + 1].started:
            lst[last_finished_index + 1].kill()
    poll_script_list(lst)


## Gently kill script flow, which is represented as list of Scripts.
#  This mean find last started script, gently kill it and poll whole list.
# @param lst List of Scripts.
def gentle_kill_script_list(lst):
    last_finished_index = last_finished_script_in_list(lst)
    if last_finished_index != (len(lst) - 1):
        if lst[last_finished_index + 1].started:
            lst[last_finished_index + 1].gentle_kill()
    poll_script_list(lst)


## Wait end of script flow, which is represented as list of Scripts.
#  This mean find last started script, kill it and poll whole list.
# @param lst List of Scripts.
# @param timeout Number of seconds. If None, wait forever.
def join_script_list(lst, timeout=None):
    if isinstance(timeout, int):
        endtime = datetime.now() + timedelta(seconds=timeout)
    else:
        endtime = None
    while poll_script_list(lst) is None:
        if endtime is not None and datetime.now() > endtime:
            raise sp.TimeoutError
    return poll_script_list(lst)


## Poll the list of threads.
# @param threads List of threads.
# @return 0 if all threads finished successful, any other number if any of
#  threads finished with non-zero return code, None in all other situations.
def poll_flows(flows):
    not_finished_found = False
    for flow in flows:
        scripts_poll_result = poll_script_list(flow)
        if scripts_poll_result is None:
            not_finished_found = True
        elif scripts_poll_result != 0:
            return scripts_poll_result
        else:
            pass
    return None if not_finished_found else 0


## Wait end of all threads.
# @param threads List of threads.
def join_flows(flows):
    results = []
    poll_flows(flows)
    for flow in flows:
        results.append(join_script_list(flow))
    return results


## Execute scenario stage.
# @param stage List of threads.
# @param gentle_kill The flag which allows to use gentle_kill if one of the threads has
#  failed.
# @return 0 if all threads have finished successfully, non-zero otherwise.
def execute_stage(stage, gentle_kill=False):
    inner_fail_flag = False
    error_code = None
    kill_func = gentle_kill_script_list if gentle_kill else kill_script_list
    flows = stage[:]
    # while none of the interrupted flags are set, unfinished threads are left
    while not inner_fail_flag and not GLOBAL_INTERRUPT_FLAG and len(flows) > 0:
        indexes_to_remove = []
        for i in range(0, len(flows)):
            poll_result = poll_script_list(flows[i])
            # if the thread has not finished, just skip it
            if poll_result is None:
                continue
            else:
                # if the thread has not finished, remove it from currently executed threads
                indexes_to_remove.append(i)
                # if return code is not 0, also set the flag and error_code
                if poll_result != 0:
                    inner_fail_flag = True
                    error_code = poll_result
        # now remove all finished threads from the list
        for i in indexes_to_remove[::-1]:
            del flows[i]
        time.sleep(SLEEP_DELAY)
    # successful execution
    if len(flows) == 0 and not GLOBAL_INTERRUPT_FLAG and not inner_fail_flag:
        return 0
    # if global interrupt flag is set, set error code and kill all remaining threads
    if GLOBAL_INTERRUPT_FLAG:
        error_code = SACError("INTERRUPTED").num_code
        for flow in flows:
            kill_script_list(flow)
    # if inner fail flag is set, kill all of the remaining threads in a gentle way or not,
    # depends on gentle_kill flag
    elif inner_fail_flag:
        for flow in stage:
            kill_func(flow)
    join_flows(stage)
    return error_code


## Schedule setting global interrupt and time limit flags.
# @param time_limit Number of seconds.
# @return threading.Timer object, which can be used for cancel timer.
def set_time_limit_alarm(time_limit):

    def set_flag():
        global GLOBAL_INTERRUPT_FLAG, TIME_LIMIT_FLAG
        GLOBAL_INTERRUPT_FLAG = True
        TIME_LIMIT_FLAG = True

    t = threading.Timer(time_limit, set_flag)
    t.start()
    return t


## Execute scenario in mode.
# @param scripts 3-level list (Stages -> Flows -> Steps), where each Step is
#  tuple of 3 Script.
# @param mode Scripts execute mode.
# @param time_limit Time limit for scripts execution.
# @return True, if successful, False otherwise.
def execute_scenario_in_mode(scripts, mode):

    @handle_SAC_errors
    def execute_scenario(scripts, mode):
        specific_scripts = extract_mode_specific_scripts(scripts, mode)
        for stage in specific_scripts:
            stage_result = execute_stage(stage, True)
            if stage_result != ReturnCode.SUCCESS.value:
                return stage_result
        return ReturnCode.SUCCESS.value

    strings = {
        ExecuteMode.TEST: "Test run",
        ExecuteMode.REAL: "Real run",
        ExecuteMode.ROLLBACK: "Rollback",
    }
    global_logger.info(
        message="****** Starting {} ******".format(strings[mode])
    )
    result = execute_scenario(scripts, mode)
    global_logger.info(message="{} result".format(strings[mode]),
                       mode_result_code=result)
    # if test run fails, return error result
    if result != ReturnCode.SUCCESS.value:
        global_logger.info(
            message="****** {} failed! ******".format(strings[mode]),
            mode_result_code=result
        )
        return False
    return True


## Extract data from STATUSES dictionary and transform it to table.
# @param scripts 3-level list (Stages -> Flows -> Steps), where each Step is
#  tuple of 3 Script.
# @param rollback_started Flag which indicate that rollback was started.
# @return List of lists with data, suitable for printing in table.
def get_scripts_statuses_data(scripts, max_mode):

    ## Convert one stage statuses to string representation.
    # @param st Tuple of 3 Scripts.
    # @return String with status representation.
    def get_result(st):
        strings = {
            -2: None,
            -1: "INTERNAL_ERROR",
            0: "TEST NOT STARTED",
            1: "TEST FAILED",
            2: "TEST INTERRUPTED",
            3: "TEST FINISHED",
            4: "NOT STARTED",
            5: "FINISHED",
            6: "FAILED",
            7: "INTERRUPTED",
            8: "ERROR",
            9: "ROLLBACKED",
            10: "ROLLBACK NOT SET",
            11: "ROLLBACK FAILED",
            12: "ROLLBACK INTERRUPTED"
        }
        # set forward code
        forward_code = -1
        if st[0].state is ScriptState.NOT_STARTED:
            forward_code = 0
        elif st[0].state is ScriptState.FAILED:
            forward_code = 1
        elif st[0].state is ScriptState.INTERRUPTED:
            forward_code = 2
        elif st[0].state is ScriptState.FINISHED:
            if max_mode is ExecuteMode.TEST:
                forward_code = 3
            else:
                if st[1].state is ScriptState.NOT_STARTED:
                    forward_code = 4
                elif st[1].state is ScriptState.FAILED:
                    forward_code = 6
                elif st[1].state is ScriptState.INTERRUPTED:
                    forward_code = 7
                elif st[1].state is ScriptState.FINISHED:
                    forward_code = 5
        # set backward code
        rollback_code = -1
        if st[1] is not ScriptState.NOT_STARTED and \
           max_mode is ExecuteMode.ROLLBACK:
            if st[2].state is ScriptState.NOT_STARTED:
                rollback_code = -2
            elif st[2].state is ScriptState.FAILED:
                rollback_code = 6
            elif st[2].state is ScriptState.INTERRUPTED:
                rollback_code = 7
            elif st[2].state is ScriptState.FINISHED:
                rollback_code = 5
            elif st[2].state is ScriptState.NOT_SET:
                rollback_code = 10
        else:
            rollback_code = -2
        return strings[forward_code], strings[rollback_code]

    def generate_step_data_from_script_triple(input_triple):
        triple = list(input_triple[:])  # make copy of input triple
        # if `rollback scripts` is None (ie not set), assign to temporary
        # triple dummy object with necessary attributes and methods
        if triple[2] is None:
            triple[2] = get_dummy_script()
        name = triple[0].name
        result, rollback_result = get_result(triple)
        test_returncode = triple[0].returncode
        real_returncode = triple[1].returncode
        rollback_returncode = triple[2].returncode
        forward_command = triple[0].command
        rollback_command = triple[2].command
        test_pids = ",".join(
            [str(i[0]) for i in triple[0].get_execution_history()]
        )
        if test_pids == "":
            test_pids = None
        real_pids = ",".join(
            [str(i[0]) for i in triple[1].get_execution_history()]
        )
        if real_pids == "":
            real_pids = None
        rollback_pids = ",".join(
            [str(i[0]) for i in triple[2].get_execution_history()]
        )
        if rollback_pids == "":
            rollback_pids = None
        return [name, result, rollback_result, test_returncode,
                real_returncode, rollback_returncode, forward_command,
                rollback_command, test_pids, real_pids, rollback_pids]

    data = []
    for stage in scripts:
        for flow in stage:
            for step in flow:
                data.append(generate_step_data_from_script_triple(step))
    return data


## Print table to global_logger.
# @param scripts 3-level list (Stages -> Flows -> Steps), where each Step is
#  tuple of 3 Script.
# @param rollback_started Flag which indicate that rollback was started.
def print_scripts_results(scripts, max_mode):
    global_logger.print_raw_text(create_string_table(
        ["Name", "State", "Rollback state", "Test code", "Real code",
         "Rollback code", "Command", "Rollback command", "Test PIDs",
         "Real PIDs", "Rollback PIDs"],
        get_scripts_statuses_data(scripts, max_mode), "Results"))


## Create string with table from specified header, data and title.
# @param header List of column names.
# @param data List of lists with data.
# @param title Table name. Can be omitted, then it will not be printed.
# @return String, which can be printed.
def create_string_table(header, data, title=None):
    s = StringIO("\n")
    table = []
    table.append([str(i).replace("_", " ") for i in header])
    table += [[str(j) if j is not None else "-" for j in list(i)] for i in data]
    max_lengths = []
    for i in range(0, len(table[0])):
        elements = [len(j[i]) for j in table]
        max_lengths.append(max(elements))
    for i in range(0, len(table)):
        for j in range(0, len(table[i])):
            table[i][j] = table[i][j].ljust(max_lengths[j], " ")
    header = " | ".join(table[0])
    header_separator = "-" * len(header)
    if title is not None:
        s.write(round((len(header) - len(title)) / 2) * " " + title + "\n")
    s.write(header + "\n")
    s.write(header_separator + "\n")
    for i in table[1:]:
        s.write(" | ".join(i) + "\n")
    return s.getvalue()


## Function, which is main() for composite scenarios execution.
# @param config ScenarioConfiguration object.
# @param disable_rollback Flag, which indicate that rollback shouldn't be run.
# @return Integer representation of result.
def execute_composite_scenario(time_limit, max_mode, data, is_composite,
                               restrict_initial_tests=False):
    # set time limit alarm
    global GLOBAL_INTERRUPT_FLAG, TIME_LIMIT_FLAG
    GLOBAL_INTERRUPT_FLAG = False
    TIME_LIMIT_FLAG = False
    if time_limit > 0:
        time_limit_alarm = set_time_limit_alarm(time_limit)
    else:
        time_limit_alarm = None
    # main logic
    modes_states = [None, None, None]
    scripts_root = convert_raw_scenario_data_to_scripts(data,
                                                        restrict_initial_tests)
    # execute test run
    modes_states[0] = execute_scenario_in_mode(scripts_root,
                                               ExecuteMode.TEST)
    if max_mode == ExecuteMode.TEST or not modes_states[0] or \
       GLOBAL_INTERRUPT_FLAG:
        if time_limit_alarm is not None:
            time_limit_alarm.cancel()
        print_scripts_results(scripts_root, ExecuteMode.TEST)
        return modes_states
    # execute real run
    gv.TEST_MODE = False
    bootstrap.SYSTEM_STATE_CHANGED = True
    modes_states[1] = execute_scenario_in_mode(scripts_root, ExecuteMode.REAL)
    # if is executed in real mode and `successful` OR `rollback disabled`, return
    if time_limit_alarm is not None:
        time_limit_alarm.cancel()
    if modes_states[1] or max_mode == ExecuteMode.REAL or \
       (GLOBAL_INTERRUPT_FLAG and not TIME_LIMIT_FLAG):
        print_scripts_results(scripts_root, ExecuteMode.REAL)
        return modes_states
    # execute rollback
    GLOBAL_INTERRUPT_FLAG = False
    TIME_LIMIT_FLAG = False
    modes_states[2] = execute_scenario_in_mode(scripts_root,
                                               ExecuteMode.ROLLBACK)
    print_scripts_results(scripts_root, ExecuteMode.ROLLBACK)
    return modes_states


## Load and parse main config of scenario.
# @param cmd_args Parsed command line arguments. Tuple where first element is a
#  list with positional arguments and second element is dictionary with pairs
#  key: value.
# @return ScenarioConfiguration object.
def parse_main_config(cmd_args):
    test_mode = "test-mode" not in cmd_args[1] or cmd_args[1]["test-mode"]
    try:
        config = ScenarioConfiguration(
            read_yaml(DICTIONARY[cmd_args[0][0]].config)
        )
        config.add_cmd_args(cmd_args[1])
    # if failed to parse main_config, return default values
    except:
        composite_data = [{"name": cmd_args[0][0],
                           "command": cmd_args[0][0],
                           "scenario-data": cmd_args[1]}]
        return test_mode, 0, False, composite_data
    # now extract values from config
    # if not composite scenario data
    if not config.composite_scenario_data:
        real_composite_scenario = False
        composite_data = [{"name": cmd_args[0][0],
                           "command": cmd_args[0][0],
                           "scenario-data": cmd_args[1]}]
        if config.rollback_scenario:
            composite_data[0]["rollback"] = config.rollback_scenario
    else:
        real_composite_scenario = True
        composite_data = config.composite_scenario_data
    try:
        time_limit = int(config["time-limit"]) \
                     if int(config["time-limit"]) >= 0 else 0
    except:
        time_limit = 0
    # now when we assume that we working with out configuration file,
    # check dangling references
    placeholders_left = config.placeholders
    if len(placeholders_left) > 0:
        raise SACError(
            "CONFIG_ERROR", "Dangling references left in configuration",
            config=DICTIONARY[cmd_args[0][0]].config,
            placeholders_left=placeholders_left
        )
    return test_mode, time_limit, real_composite_scenario, composite_data


## Detect maximum execution mode which is executed last.
# @param config ScenarioConfiguration object.
# @param cmd_args Parsed command line arguments. Tuple, where first element is a
#  list with positional arguments and second element is dictionary with pairs
#  key: value.
# @return ExecuteMode enum value.
def get_max_execution_mode(is_test_mode, cmd_args):
    if is_test_mode:
        return ExecuteMode.TEST
    elif "disable-rollback" in cmd_args[1] and cmd_args[1]["disable-rollback"]:
        return ExecuteMode.REAL
    else:
        return ExecuteMode.ROLLBACK


## Function which is responsible for reading command dictionary,
#  command line parsing and starting execution of the necessary scenario type.
# @return Integer representation of the result.
def composite_runner_main():
    global_logger.info(message="****** Starting composite scenario ******")
    try:
        # parsing cmd args
        cmd_args = bootstrap.parse_cmd_args(sys.argv[1:])
        # replace the dictionary path, if the second positional argument is provided
        if len(cmd_args[0]) > 1:
            dictionary_path = StrPathExpanded(cmd_args[0][1])
        else:
            dictionary_path = os.path.join(sys.path[0], "..", "..", "configs",
                                           "dictionary.yaml")
        interpreters_path = os.path.join(
            os.path.dirname(dictionary_path), "interpreters.yml"
        )
        global_logger.info(message="Parsed command line arguments",
                           command=cmd_args[0][0], dictionary=dictionary_path,
                           arguments=cmd_args[1])
        # building dictionary
        global DICTIONARY, INTERPRETERS
        INTERPRETERS = Interpreter.parse_interpreters_file(interpreters_path)
        DICTIONARY = CommandDictionary(dictionary_path)
        bootstrap.set_debug_values(cmd_args[1])
        # make config and detect max execution mode
        test_mode, time_limit, real_composite_scenario, \
            composite_data = parse_main_config(cmd_args)
        max_mode = get_max_execution_mode(test_mode, cmd_args)
    # if any error occurred, return 9and don't print table
    except SACError as err:
        global_logger.error(**err.to_dict())
        return ([None, None, None], ExecuteMode.TEST)
    except Exception as err:
        global_logger.error(**SACError("UNKNOWN", err).to_dict())
        return ([None, None, None], ExecuteMode.TEST)
    # building data and setting necessary global variables
    global TOP_LEVEL_SCENARIO_NAME
    if real_composite_scenario:
        # set global top-level scenario name
        TOP_LEVEL_SCENARIO_NAME = cmd_args[0][0]
    else:
        TOP_LEVEL_SCENARIO_NAME = None
    # now really execute scenario
    res = execute_composite_scenario(
        time_limit,
        max_mode,
        composite_data,
        real_composite_scenario,
        "restrict-initial-tests" in cmd_args[1] and \
        cmd_args[1]["restrict-initial-tests"],
    )
    # if global flags is set, log appropriate error
    if TIME_LIMIT_FLAG:
        try:
            raise SACError("TIMEOUT_ERROR")
        except Exception as err:
            global_logger.error(**err.to_dict())
    elif GLOBAL_INTERRUPT_FLAG:
        try:
            raise SACError("INTERRUPTED")
        except Exception as err:
            global_logger.error(**err.to_dict())
    return res, max_mode


## Convert the list of 3 elements [test_result, real_result, rollback_result],
#  returned by execute_scenario_in_mode() to return code and hint.
# @param modes_states List of 3 elements [test_result, real_result,
#  rollback_result], returned by execute_scenario_in_mode().
# @param max_mode Last possibly executed mode.
# @return Tuple (code, hint)
def convert_modes_states_and_get_hint(modes_states, max_mode):
    if modes_states[0] is None:
        return ReturnCode.ERROR_STATE_NOT_CHANGED, "HINT: preparation failed"
    else:
        if not modes_states[0]:
            return ReturnCode.ERROR_STATE_NOT_CHANGED, "HINT: test mode failed"
        else:
            if max_mode == ExecuteMode.TEST:
                return ReturnCode.SUCCESS, "HINT: test mode successful"
            if modes_states[1]:
                return ReturnCode.SUCCESS, \
                    "HINT: successful both test and real mode"
            else:
                if max_mode == ExecuteMode.REAL:
                    return ReturnCode.ERROR_STATE_CHANGED, \
                        "HINT: real mode failed, rollback disabled"
                if modes_states[2]:
                    return ReturnCode.ROLLBACKED, \
                        "HINT: real mode failed, rollback successful"
                else:
                    return ReturnCode.ERROR_STATE_CHANGED, \
                        "HINT: real mode failed, rollback failed"


## Main function.
def main():
    # creating pid file
    pid = os.getpid()
    pid_filename = os.path.join(gv.PID_PATH,
                                "SAC_{}.pid".format(pid))
    pid_file = open(pid_filename, "w")
    pid_file.write(str(pid))
    pid_file.close()
    # creating agent log
    log_folder = "script_logs"
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)
    global_logger.add_file_handler(os.path.join(
        gv.PID_PATH, log_folder, "composite_runner" + "_" + str(
            datetime.now().strftime("%y%m%d_%H%M%S")) +
        "_" + str(os.getpid()) + ".log"
    ))
    global_logger.add_stream_handler(sys.stdout)
    # execute agent main function
    modes_states, max_mode = composite_runner_main()
    # remove pid file
    try:
        os.remove(pid_filename)
    except:
        global_logger.warning(message="Couldn't remove pid file",
                              pid_filename=pid_filename)
    # convert statuses, set exit code and exit
    code, hint = convert_modes_states_and_get_hint(modes_states, max_mode)
    global_logger.info(message=hint)
    global_logger.info(message="Result code",
                       result_code=code.value)
    sys.exit(code.value)


if __name__ == "__main__":
    main()
