# coding: utf-8

import requests
import json
import cgi
import base64
import uuid
import os
import hashlib
import shutil


from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError
from lib.common.logger import global_logger, LogFunc
from lib.utils import splitext_archive, unpack_zip, unpack_archive, \
    KNOWN_ARCHIVE_EXTENSIONS
from lib.utils.fs import split_path
from lib.common.config import StrPathExpanded


BASE_PATH = "https://update-api.1c.ru/update-platform/programs/update/"


## Make {basePath}/info request.
# @param config_name Name of configuration in update-api service.
# @param config_version Current version of configuration.
# @param additional_parameters Dictionary with additional_parameters.
# @return requests.Response object.
def info_request(config_name, config_version):
    l = LogFunc(message="Making Info request", config_name=config_name,
                current_version=config_version)
    # fill JSON data
    json_data = {
        "programName": config_name,
        "versionNumber": config_version,
        "platformVersion": "",
        "programNewName": "",
        "redactionNumber": "",
        "updateType": "NewConfigurationAndOrPlatform",
        "additionalParameters": []
    }
    # make request
    response = requests.post(
        BASE_PATH + "info/", data=json.dumps(json_data),
        headers={
            "User-Agent": "1C+Enterprise/8.3",
            "Content-Type": "application/json",
            "Accept": "*/*"
        }
    )
    global_logger.debug(message="Info response", http_code=response.status_code,
                        headers=response.headers, content=response.content)
    # if response contain JSON data, return it, raise exception otherwise
    if response.ok and "application/json" in \
       cgi.parse_header(response.headers["Content-Type"]):
        return response
    else:
        raise SACError("NOT_JSON", content=response.content)


## Make {BasePath}/ request.
# @param info_response requests.Response object, which returned by info_request().
# @param username Name of user on portal.1c.ru.
# @param password Password of user on portal.1c.ru.
# @param additional_parameters Dictionary with additional_parameters.
# @return requests.Response object.
def get_files_request(info_response, username, password):
    # extract upgrade sequence
    try:
        upgrade_sequence = info_response.json()["configurationUpdateResponse"] \
                   ["upgradeSequence"]
        if len(upgrade_sequence) < 1:
            raise SACError("ARGS_ERROR",
                                         "upgrade UUIDS not found",
                                         value=info_response.content)
    except:
        raise SACError("ARGS_ERROR",
                                     "upgrade UUIDS not found",
                                     value=info_response.content)
    # extract current uuid
    try:
        current_uuid = info_response.json()["configurationUpdateResponse"] \
                   ["programVersionUin"]
    except:
        raise SACError("ARGS_ERROR",
                                     "current version UUID not found",
                                     value=info_response.content)
    l = LogFunc(message="Making GetFiles request",
                upgrade_sequence=upgrade_sequence,
                current_uuid=current_uuid,)
    # fill JSON data
    json_data = {
        "upgradeSequence": upgrade_sequence,
        "programVersionUin": current_uuid,
        "login": username,
        "password": password,
        "platformDistributionUin": None,
        "additionalParameters": []
    }
    # make request
    response = requests.post(BASE_PATH, data=json.dumps(json_data),
        headers={"User-Agent": "1C+Enterprise/8.3",
                 "Content-Type": "application/json",
                 "Accept": "*/*"}
    )
    global_logger.debug(message="Get files response",
                        http_code=response.status_code,
                        headers=response.headers, content=response.content)
    # if response contain JSON data, return it, raise exception otherwise
    if response.ok and "application/json" in \
       cgi.parse_header(response.headers["Content-Type"]):
        return response
    else:
        raise SACError("NOT_JSON", content=response.content)


def download_and_save_file(configuration_update_entry, username, password,
                           tmp_folder):
    # make temp filename
    temp_filename = str(uuid.uuid4()) + "." \
                    + configuration_update_entry["updateFileFormat"]
    # get file from URL
    url = configuration_update_entry["updateFileUrl"]
    response = requests.get(
        url, stream=True, auth=(username, password),
        headers={
            "User-Agent": "1C+Enterprise/8.3",
        }
    )
    global_logger.debug(message="Download file response",
                        http_code=response.status_code,
                        headers=response.headers)
    temp_file = os.path.join(tmp_folder, temp_filename)
    with open(temp_file, "wb") as f:
        shutil.copyfileobj(response.raw, f)
    return temp_file


## Download and extract update file. Also performs comparison of hash sums.
# @param configuration_update_entry Element of
#  GetFilesResponse["configurationUpdateDataList"].
# @param store_location Path, where updates will be stored.
# @param username Name of user on portal.1c.ru.
# @param password Password of user on portal.1c.ru.
# @param tmp_folder Path to temporary folder. If None (by default),
#  it will be created automatically.
def download_and_extract_update(configuration_update_entry, store_location,
                                username, password, tmp):
    l = LogFunc(message="Downloading file",
                configuration_update_entry=configuration_update_entry,
                store_location=store_location)
    conf_name, conf_version = split_path(
        configuration_update_entry["templatePath"].replace("\\", "/")
    )[1:3]
    conf_version = conf_version.replace("_", ".")
    conf_file_type = {
        ".cf": "full", ".cfu": "update"
    }[splitext_archive(configuration_update_entry["updateFileName"])[1]]
    dst = os.path.join(store_location, "1c-confs", conf_name, conf_version,
                       conf_file_type)
    if not os.path.exists(dst):
        os.makedirs(dst)
    downloaded_file = download_and_save_file(configuration_update_entry,
                                             username, password, tmp)
    # try to unpack, if necessary
    if configuration_update_entry["updateFileFormat"].lower() == "zip":
        unpack_zip(downloaded_file, dst, pattern="", invert=False, replace=True,
                   try_encode="cp866")
    elif splitext_archive(downloaded_file)[1] \
         in KNOWN_ARCHIVE_EXTENSIONS:
        unpack_archive(downloaded_file, dst, pattern="", invert=False,
                       replace=True)
    else:
        shutil.copy(downloaded_file, dst)
    # check hash. If error occurred (file not found or hashes doesn't match),
    # log warning and proceed
    try:
        # build file path
        update_file = os.path.join(dst,
                                   configuration_update_entry["updateFileName"])
        if not os.path.exists(update_file):
            raise Exception
        # calculate actual hash
        hash_md5 = hashlib.md5()
        with open(update_file, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        actual_hash = hash_md5.hexdigest()
        # convert hashSum to string representation of number in base 16
        remote_hash = format(int.from_bytes(
            base64.b64decode(configuration_update_entry["hashSum"]),
            byteorder="big",
            signed=False
        ), "x")
        if actual_hash != remote_hash and "0" + remote_hash != actual_hash:
            global_logger.warning(
                message="Actual hash of .cfu file doesn't match the one "
                "returned by update-api", hash=remote_hash,
                actual_hash=actual_hash
            )
    except:
        global_logger.warning(message="Can't find .cfu file, proceeding without"
                              " hash checking")


class DownloadFromUpdateApiScenario(BaseScenario):

    def _validate_specific_data(self):
        validate_data = [
            ["configuration-name", str],
            ["current-version", str],
            ["download-folder", StrPathExpanded],
            ["username", str],
            ["password", str],
        ]
        self.config.validate(validate_data)

    def __set_upgrade_sequence(self):
        # Info request, which should return list of updates (at least one),
        # otherwise consider it as error
        info_response = info_request(
            self.config["configuration-name"],
            self.config["current-version"]
        )
        # get links to files
        files_response = get_files_request(
            info_response,
            self.config["username"], self.config["password"]
        )
        self.upgrade_sequence = files_response \
            .json()["configurationUpdateDataList"]

    def _check_folders(self):
        pass

    def _check_upgrade_sequence(self):
        self.__set_upgrade_sequence()
        for entry in self.upgrade_sequence:
            response = requests.get(
                url=entry["updateFileUrl"], stream=True,
                headers={"User-Agent": "1C+Enterprise/8.3"},
                auth=(self.config["username"], self.config["password"])
            )
            if not response.ok:
                raise SACError(
                    "URL_ERROR", "response status code is not good",
                    response_code=response.status_code,
                    url=response.url
                )

    def _get_available_tests(self):
        return [
            ("check-folders", self._check_folders, False),
            ("check-upgrade-sequence", self._check_upgrade_sequence, False),
        ]

    ## Execute scenario.
    # @param self Pointer to object.
    def _real(self):
        l = LogFunc(message="Downloading configuration update from update-api",
                    config_name=self.config["configuration-name"],
                    current_version=self.config["current-version"],
                    dst=self.config["download-folder"])
        if not hasattr(self, "upgrade_sequence"):
            self.__set_upgrade_sequence()
        # download each file
        for entry in self.upgrade_sequence:
            download_and_extract_update(
                entry, self.config["download-folder"],
                self.config["username"], self.config["password"], self.tmp
            )


if __name__ == "__main__":
    DownloadFromUpdateApiScenario.main()
