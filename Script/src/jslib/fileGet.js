const path = require("path");
const node_modules_path = path.resolve('../../../node/node_modules');
const libPath = path.resolve();

const fs = require('fs');
const WebSocket = require(node_modules_path + '/ws');

const _createMessage = async (srcPath) => {
  const message = {
    action: 'get_file',
    path: srcPath,
  };
  return JSON.stringify(message);
}


const _getName = (srcPath) => {
  const name = path.basename(srcPath);
  return name;
}

const fileGet = async (options = {}) => {
  const host = options.host || 'portal.itprogress.ru';
  const port = options.port || 978;
  const dstPath = options.dstPath; 
  const srcPath = options.srcPath;
  const name = options.name || _getName(srcPath);

  return new Promise((resolve, reject) => {
  	const ws = new WebSocket(`ws://${host}:${port}`);
    ws.on('open', async () => {
      const message = await _createMessage(srcPath);
      console.log(message);
      ws.send(message);
    });
   
    ws.on('message', function (data) {
      console.log(data);
      const fullDstPath = path.resolve(dstPath, name);
      fs.writeFile(fullDstPath, data, (err) => {
        if (err) {
          console.log(err);
          return reject(err)
        };
        console.log('asdasdasd');
        return resolve();
      });
    });
  });
}

module.exports = fileGet ;
