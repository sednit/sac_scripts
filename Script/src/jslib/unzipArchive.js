const path = require("path");
const node_modules_path = path.resolve('../../../node/node_modules');
const libPath = path.resolve();

const fs = require('fs');

const yauzl = require(node_modules_path + '/yauzl');
const mkdirp = require(node_modules_path + '/mkdirp');
const { mkdirpAsync, directoryNotExists } = require(libPath + '/utilsFs');

const unzipArchive = async (options = {}) => {
  const srcPath = options.srcPath;
  const dstPath = options.dstPath || path.dirname(srcPath);
  const name = options.name || path.basename(srcPath, '.zip');

  console.log(srcPath);
  if (await directoryNotExists(dstPath)) {
    await mkdirpAsync(dstPath);
  }
  return new Promise((resolve, reject) => {
    yauzl.open(srcPath, {lazyEntries: true}, (err, zipfile) => {
      if (err) throw err;
      zipfile.on("end", function(entry) {
 		    return resolve();
      })
      zipfile.on("entry", function(entry) {
        if (/\/$/.test(entry.fileName)) {
        	const dirPath = path.resolve(dstPath, entry.fileName) + '\\';
        	mkdirp(dirPath, function(err) {
            if (err) throw err;
            zipfile.readEntry();
          });
        } else {
          zipfile.openReadStream(entry, function(err, readStream) {
            if (err) throw err;
            const filePath = path.resolve(dstPath, entry.fileName);
            const dirPath = path.dirname(filePath);
            mkdirp(dirPath, function(err) {
              readStream.on("end", function() {
                zipfile.readEntry();
              });
              if (err) throw err;
              readStream.pipe(fs.createWriteStream(filePath));
            });
          });
        }
      });
      zipfile.readEntry();
    })
  })
}

module.exports = unzipArchive ;
