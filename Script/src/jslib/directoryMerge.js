const path = require("path");
const node_modules_path = path.resolve('../../../node/node_modules');
const libPath = path.resolve();

const fs = require('fs');
const ncp = require(node_modules_path + '/ncp').ncp;

const directoryMerge = async (options = {}) => {
  const from = options.from;
  const to = options.to;
  return new Promise((resolve) => {
  	ncp(from, to, { stopOnErr: true } ,(err) => {
      if (err) throw err;
      return resolve();
    });
  })
}

module.exports = directoryMerge ;
