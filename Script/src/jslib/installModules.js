const path = require("path");
const node_modules_path = path.resolve('../../../node/node_modules');
const libPath = path.resolve();
const nodePath = path.resolve('../../../node');

const fs = require('fs');

const modules = require('../../../configs/modules');
const npm = require(node_modules_path + '/npm');

const installModules = async (options = {}) => {
  npm.load({
    loaded: false
  }, function (err) {
    npm.commands.install(nodePath, modules, function (err, data) {});
  })
}

module.exports = installModules;
