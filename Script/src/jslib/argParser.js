const result = {};

const parseCmdArgs = (argsRaw) => {
  for(const arg of argsRaw) {
    const regExp = /,?--([^=]+)=(.+)/;
    const match = regExp.exec(arg);
    result[match[1]] = match[2];
  }
}

const parseArgv = (argv) => {
  result.configPath = argv[2];
  parseCmdArgs(argv.slice(3));
  return result;
}

module.exports = parseArgv;

