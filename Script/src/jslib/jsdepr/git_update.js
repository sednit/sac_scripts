const path = require("path");
const node_modules_path = path.resolve('../../node/node_modules');
const libPath = path.resolve('jslib');

const NodeGit = require(node_modules_path + '/nodegit');
const fs = require('fs');
const args = require(libPath + '/argParser')(process.argv);

if (args['test-mode'] === 'True') return; 

const repositoryIsExists = async (pathToRepository) => {
  return new Promise((resolve) => {
  	fs.access(pathToRepository + '/.git', (err) => {
  	  if(err) {
  	  	resolve(false);
  	  } else {
  	  	resolve(true);
  	  }
    })
  })
}

const cloneRepository = async (pathToRepository, url) => {
  return NodeGit.Clone(url, pathToRepository).then( () => {
    return true;
  }).catch( (err) => {
  	return false;
  });
}

const initRepository = async (pathToRepository, url) => {
  return NodeGit.Repository.init(pathToRepository, 0).then( (repository) => {
  	fs.writeFile("test.txt", 'dfdfgd', function(err) {});
  	return NodeGit.Remote.create(repository, 'rep', url).then(() => {

  	}).catch((err) => {
   	  fs.writeFile("test.txt", err, function(err) {});  		
  	})
    return true;
  }).catch( (err) => {
  	fs.writeFile("test.txt", err, function(err) {}); 
  	return false;
  });
}

const pullRepository = async (pathToRepository, remote) => {
  let repo;
  return NodeGit.Repository.open(pathToRepository).then((repository) => {
    repo = repository;
    return true;
  }).then(() => {
  	//return NodeGit.Reset.reset(repo, pathToRepository, Reset.TYPE.HARD);
    return repo.fetchAll();
  }).then(() => {
    return repo.mergeBranches('master', 'origin/master');
  }).catch((err) => {
  	fs.writeFile("test.txt", err, function(err) {});
  })
  //NodeGit.Repository.open(pathToRepository).then(function(repository) {
  //  repo = repository;
  //  return repo.fetch('origin');
  //}).then(function() {
  //  return repo.getBranchCommit('origin/HEAD');
  //}).then(function(originHeadCommit) {
  //  return Reset.reset(repo, originHeadCommit, Reset.TYPE.HARD);
  //})
}

const main = async () => {
  const url = 'https://sednit@bitbucket.org/sednit/sac_scripts.git';
  const pathToRepository = path.resolve('../../test');
  if(await repositoryIsExists()) {
    await pullRepository(pathToRepository, url);
  } else {
    await cloneRepository(pathToRepository, url)
  }
}

main();
