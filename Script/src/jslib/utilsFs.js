const path = require("path");
const node_modules_path = path.resolve('../../../node/node_modules');
const mkdirp = require(node_modules_path + '/mkdirp');
const fs = require('fs');

const directoryExists = async (directoryPath) => {
  return new Promise( (resolve) => {
   	fs.stat(directoryPath, function (err, stats) {
      if (err || stats === undefined) {
        return resolve(false);
      }
      if (stats.isDirectory()) {
        return resolve(true);
      }
      return resolve(false);
    });
  })
}

const directoryNotExists = async (directoryPath) => {
  return !directoryExists(directoryPath);
}

const mkdirpAsync = async (directoryPath) => {
  return new Promise((resolve) => {
    mkdirp(directoryPath, function(err) {
      if (err) throw err;
    })
    return resolve(true);
  })
}

module.exports = { directoryExists, directoryNotExists, mkdirpAsync }
