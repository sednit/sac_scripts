const path = require("path");
//const fileGet = require('./fileGet');
//const unzipArchive = require('./unzipArchive');
//const directoryMerge = require('./directoryMerge');
const installModules = require('./installModules');

//fileGet({
//  host: '192.168.0.240',
//  port: 978,
//  dstPath: 'C:/projects/python/sac scripts/test',
//  srcPath: '/platform/8.3/8.3.12.1616/windows64full/0x040e.ini',
//  name: undefined,
//}).then( (result) => {
//  console.log('OK');
//});

//const test = async () => {
//  const rootDir = path.resolve('../../..');
//  const version = 'debug';
//  const tmpDir = path.resolve('../../../tmp');
//  const ftpPath = path.posix.join('sac', version + '.zip');
//  console.log(ftpPath);
//  await fileGet({
//    host: '192.168.0.240',
//    port: 978,
//    dstPath: tmpDir,
//    srcPath: ftpPath,
//    name: undefined,
//  });
//  const archivePath = path.resolve(tmpDir, version + '.zip')
//  await unzipArchive({ srcPath: archivePath });
//  await directoryMerge({
//  	from: path.resolve(rootDir, 'tmp/debug'),
//  	to: path.resolve(rootDir, 'tmp/s'),
//  });
//}
//test();

installModules();
