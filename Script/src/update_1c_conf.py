#!/usr/bin/python3
# coding: utf-8
import sys
import os
import subprocess as sp
import datetime


from lib.common import bootstrap
from lib.base_1c_conf_action import *
from lib.common.config import *
from lib.utils import *
from lib.utils.cmd import *


if __name__ == "__main__":
    Base1CConfActionScenario.main("update-cfg")
