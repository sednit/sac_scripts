import os
from lib.common import bootstrap
from lib.common.base_scenario import BaseScenario
from lib.common.errors import SACError

class UpdateDBData(BaseScenario):

    def _validate_specific_data(self):
        pass

    def _real(self):
    	if not os.path.exists(self.config["db-path"] + "\\1Cv8.1CD"):
  			raise SACError(
        	    "RUN_ERROR",
        	    "1CD file does not exists",
        	    "error"
        	)

if __name__ == "__main__":
  UpdateDBData.main()