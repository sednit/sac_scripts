# coding: utf-8

import re
import shutil
import os
import cgi
import uuid
import platform
from robobrowser import RoboBrowser


from lib.common import bootstrap
from lib.common.base_scenario import *
from lib.common.errors import *
from lib.common.logger import *
from lib.utils import *
from lib.utils.cmd import run_cmd
from lib.utils.fs import *
from lib.common import global_vars as gv
from lib.common.config import *
from lib.base_download_release import *


class DownloadConfigurationScenario(BaseDownloadReleaseScenario):

    def _validate_additional_data(self):
        validate_data = [
            ["name", str],
            ["version", str],
            ["distr-type", str, str, ["full", "update"]],
        ]
        self.config.validate(validate_data)

    def _get_url(self):
        # reset browser opened page
        self.browser.open(self.portal_url+"/total")
        # get to configuration versions page
        # get to specific version page
        self.real_version = go_to_product_version_page(
            self.browser, "^{}$".format(escape_special(self.config["name"])),
            self.config["version"]
        )
        try:
            find_link_and_go(
                self.browser, "a",
                "Дистрибутив обновления" if self.config["distr-type"] == "update" \
                else "Полный"
            )
        except SACError as err:
            if err.str_code == "BROWSER_ERROR":
                raise SACError(
                    "LOGIC_ERROR",
                    "Can't find product distribution with specified parameters",
                    name=self.config["name"],
                    distr_type=self.config["distr-type"],
                )
        # get download link
        return self.browser.find(class_="downloadDist").find("a")["href"]

    def _get_available_tests(self):
        return [
            ["try-login", self._login_to_portal, False],
            ["check-url", self._get_url, False],
        ]

    def _process_files(self, src):
        if self.config["save-accurately"]:
            save_configuration_accurately(src, self.config["download-folder"],
                                          self.tmp)
        else:
            save_configuration(src, self.config["download-folder"],
                               self.tmp, self.real_version,
                               self.config["distr-type"])


if __name__ == "__main__":
    DownloadConfigurationScenario.main()
