const path = require("path");
const node_modules_path = path.resolve('../../node/node_modules');
const libPath = path.resolve('jslib');

const fs = require('fs');

const mkdirp = require(node_modules_path + '/mkdirp');
const ncp = require(node_modules_path + '/ncp').ncp;

const args = require(libPath + '/argParser')(process.argv);

if (args['test-mode'] === 'True') return; 

const main = async () => {
  ncp(args.from, args.to, { stopOnErr: true } ,(err) => {
    if (err) throw err;
  });
}

main();
